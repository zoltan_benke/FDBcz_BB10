#ifndef ACTORITEM_H
#define ACTORITEM_H

#include <QObject>
#include <QVariantMap>
#include <QStringList>
#include <bb/cascades/CustomControl>

using namespace bb::cascades;
/*

  FILM OBJECT
      {
            "id": "96166",
            "title": "Expendables: Postradatelní 3",
            "orig_title": "The Expendables 3",
            "image": "996bc61794a3d6505cc7932a7a1d.jpg",
            "year": "2014",
            "rating": "0",
            "job": [
                {
                    "title": "hraje",
                    "nick": "Max Drummer"
                }
            ],
            "jobfdb": "hraje ... Max Drummer"
      }

  */

class ActorItem : public bb::cascades::CustomControl
{
    Q_OBJECT
    Q_PROPERTY(QVariant id READ id NOTIFY dataChanged)
    Q_PROPERTY(QVariant url READ url NOTIFY dataChanged)
    Q_PROPERTY(QVariant name READ name NOTIFY dataChanged)
    Q_PROPERTY(QVariant lastname READ lastname NOTIFY dataChanged)
    Q_PROPERTY(QVariant nick READ nick NOTIFY dataChanged)
    Q_PROPERTY(QVariant profession READ profession NOTIFY dataChanged)
    Q_PROPERTY(QVariant nationality READ nationality NOTIFY dataChanged)
    Q_PROPERTY(QVariant age READ age NOTIFY dataChanged)
    Q_PROPERTY(QVariant birthday READ birthday NOTIFY dataChanged)
    Q_PROPERTY(QVariant death READ death NOTIFY dataChanged)
    Q_PROPERTY(QVariant image READ image NOTIFY dataChanged)
    Q_PROPERTY(QVariant info READ info NOTIFY dataChanged)
    Q_PROPERTY(QVariantList films READ films NOTIFY dataChanged)
public:
    ActorItem();
    ActorItem(QVariantMap &item);


    Q_INVOKABLE inline QVariant id() const { return _item.value("id", "-1"); }
    Q_INVOKABLE inline QVariant url() const { return _item.value("url"); }
    Q_INVOKABLE inline QVariant name() const { return _item.value("name"); }
    Q_INVOKABLE inline QVariant lastname() const { return _item.value("lastname"); }
    Q_INVOKABLE inline QVariant nick() const { return _item.value("nick"); }
    Q_INVOKABLE inline QVariant profession() const { return _item.value("profession"); }
    Q_INVOKABLE inline QVariant nationality() const { return _item.value("nationality"); }
    Q_INVOKABLE inline QVariant age() const { return _item.value("age"); }
    Q_INVOKABLE inline QVariant birthday() const { return _item.value("birthday"); }
    Q_INVOKABLE inline QVariant death() const { return _item.value("death"); }
    Q_INVOKABLE inline QVariant image() const { return _item.value("image"); }
    Q_INVOKABLE inline QVariant info() const { return _item.value("info"); }
    Q_INVOKABLE inline QVariantList films() const { return _item.value("film").toList(); }

signals:
    void dataChanged();

private:
    QVariantMap _item;

};

#endif // ACTORITEM_H
