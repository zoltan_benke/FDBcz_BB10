/*
 * settings.cpp
 *
 *  Created on: 24.2.2014
 *      Author: Benecore
 */

#include "settings.h"
#include "../custom/sizehelper.h"

Settings *Settings::_instance = 0;

Settings::Settings()
{
	settings = new QSettings("DevPDA Inc.", "FDB.cz");


	// General settings
	_language = settings->value("language", "cs").toString();
	_showLogo = settings->value("showLogo", true).toBool();
	_showStationName = settings->value("showStationName", false).toBool();
	_showFirstProgram = settings->value("showFirstProgram", true).toBool();
	_showSecondProgram = settings->value("showSecondProgram", false).toBool();
	_showActive = settings->value("showActiveStations", true).toBool();
	_imageQuality = settings->value("imageQuality", 1).toInt();
	_startView = settings->value("startView", 1).toInt();
	// Tv tipy filtering
	_filterTipy = settings->value("filterTipy", false).toBool();
	// Font
	_fontFamilyTitle = settings->value("fontFamilyTitle", "Sans-serif").toString();
	_fontFamilySubtitle = settings->value("fontFamilySubtitle", "Sans-serif").toString();
	_fontSizeTitle = settings->value("fontSizeTitle", SizeHelper::instance()->nType() ? 8 : 9).toFloat();
	_fontSizeSubtitle = settings->value("fontSizeSubtitle", SizeHelper::instance()->nType() ? 5 : 6).toFloat();
	// Colors
	_activeColor = settings->value("activeColor", "0098f0").toString();
	// Calendar
	_manualNotify = settings->value("manualNotify", false).toBool();
	_reminder = settings->value("reminder", 15).toInt();
	_kinoReminder = settings->value("kinoReminder", -1).toInt();
	_unitedTime = settings->value("unitedTime", false).toBool();
	// Searching
	_searchLimit = settings->value("searchLimit", 10).toInt();
	_searchLimitProgram = settings->value("searchLimitProgram", 10).toInt();
	// Caache
	_cacheLimit = settings->value("cacheLimit", 5242880).toLongLong();
}

Settings::~Settings()
{
	delete settings;
}


Settings *Settings::instance()
{
	if (!_instance)
		_instance = new Settings;
	return _instance;
}
