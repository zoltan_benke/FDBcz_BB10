/*
 * database.cpp
 *
 *  Created on: 7.3.2014
 *      Author: benecore
 */

#include "database.h"

Database *Database::_instance = 0;

Database::Database()
{
	db = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE"));
	db->setDatabaseName("data/database.db");
	bool ok = db->open();
	if(ok){
		//qDebug() << "DatabaseManager: db opened.";
	}else{
		qDebug() << "DatabaseManager: db open error.";
	}
	createTables();
}

Database::~Database()
{
	delete db;
}


Database *Database::instance()
{
	if (!_instance)
		_instance = new Database;
	return _instance;
}


void Database::destroy()
{
	if (_instance){
		delete _instance;
		_instance = 0;
	}
}


void Database::reopen()
{
	emit favoriteChanged(getFavorites(), favoriteCount());
	emit cinemasChanged();
}


void Database::close()
{
	if (db->isOpen())
		db->close();
}



void Database::createTables()
{
	db->exec("CREATE TABLE IF NOT EXISTS favorite(id TEXT UNIQUE)");
	db->exec("CREATE TABLE IF NOT EXISTS cinemas(id TEXT UNIQUE, cinema TEXT)");
	db->exec("CREATE TABLE IF NOT EXISTS login(email TEXT UNIQUE, password TEXT)");
}


bool Database::addFavorite(const QString& id)
{
	if (!db->isOpen())
		db->open();
	QSqlQuery query;
	query.prepare("INSERT INTO favorite VALUES(?)");
	query.addBindValue(id);

	bool exec = query.exec();

	if (exec){
		//qDebug() << "ID added successfully";
		emit favoriteChanged(getFavorites(), favoriteCount());
	}else{
		qWarning() << "Unable to add ID" << db->lastError();
	}
	return exec;
}

bool Database::removeFavorite(const QString& id)
{
	if (!db->isOpen())
		db->open();
	QSqlQuery query;
	query.prepare("DELETE FROM favorite WHERE id = ?");
	query.addBindValue(id);

	bool exec = query.exec();

	if (exec){
		//qDebug() << "ID removed successfully";
		emit favoriteChanged(getFavorites(), favoriteCount());
	}else{
		qWarning() << "Unable to remove ID" << db->lastError();
	}
	return exec;
}

bool Database::isFavorite(const QString &id)
{
	QString value;
	if (!db->isOpen())
		db->open();
	QSqlQuery query;
	query.prepare("SELECT id FROM favorite WHERE id = ?");
	query.addBindValue(id);

	bool exec = query.exec();

	while (query.next()){
		value = query.value(0).toString();
	}

	if (exec){
		//qDebug() << "ID favorited successfully";
	}else{
		qWarning() << "Unable to get favorite ID" << db->lastError();
	}
	if (value.isEmpty())
		return false;
	return true;
}

QVariantList Database::getFavorites()
{
	QVariantList list;
	if (!db->isOpen())
		db->open();
	QSqlQuery query;
	query.prepare("SELECT * FROM favorite");

	bool exec = query.exec();

	while (query.next()){
		list.append(query.value(0));
	}
	if (exec){
		//qDebug() << "GET FAVORITES successfully";
	}else{
		qWarning() << "Unable to get FAVORITES" << db->lastError();
	}
	return list;
}


int Database::favoriteCount()
{
	int count;
	if (!db->isOpen())
		db->open();
	QSqlQuery query;
	query.prepare("SELECT COUNT(*) FROM favorite");

	bool exec = query.exec();

	while(query.next()){
		count = query.value(0).toInt();
	}
	if (exec){
		//qDebug() << "GET COUNT successfully" << count;
	}else{
		qWarning() << "Unable to get COUNT" << db->lastError();
	}
	return count;
}

bool Database::addCinema(const QVariant &id, const QVariant &kino)
{
	if (!db->isOpen())
		db->open();
	QSqlQuery query;
	query.prepare("INSERT INTO cinemas VALUES(?, ?)");
	query.addBindValue(id);
	query.addBindValue(kino);

	bool exec = query.exec();

	if (exec){
		//qDebug() << "ID added successfully";
		emit cinemasChanged();
	}else{
		qWarning() << "Unable to add Cinema" << db->lastError();
	}
	return exec;
}

bool Database::removeCinema(const QVariant& id)
{
	if (!db->isOpen())
		db->open();
	QSqlQuery query;
	query.prepare("DELETE FROM cinemas WHERE id = ?");
	query.addBindValue(id);

	bool exec = query.exec();

	if (exec){
		//qDebug() << "ID removed successfully";
		emit cinemasChanged();
	}else{
		qWarning() << "Unable to remove Cinema" << db->lastError();
	}
	return exec;
}

bool Database::isFavoriteCinema(const QVariant& id)
{
	QString value;
	if (!db->isOpen())
		db->open();
	QSqlQuery query;
	query.prepare("SELECT id FROM cinemas WHERE id = ?");
	query.addBindValue(id);

	bool exec = query.exec();

	while (query.next()){
		value = query.value(0).toString();
	}

	if (exec){
		//qDebug() << "ID favorited successfully";
	}else{
		qWarning() << "Unable to get favorite cinema" << db->lastError();
	}
	if (value.isEmpty())
		return false;
	return true;
}

int Database::cinemasCount()
{
	int count;
	if (!db->isOpen())
		db->open();
	QSqlQuery query;
	query.prepare("SELECT COUNT(*) FROM cinemas");

	bool exec = query.exec();

	while(query.next()){
		count = query.value(0).toInt();
	}
	if (exec){
		//qDebug() << "GET COUNT successfully" << count;
	}else{
		qWarning() << "Unable to get count of cinemas" << db->lastError();
	}
	return count;
}

QVariantList Database::cinemas()
{
	QVariantList list;
	if (!db->isOpen())
		db->open();
	QSqlQuery query;
	query.prepare("SELECT * FROM cinemas");

	bool exec = query.exec();

	while (query.next()){
		list.append(query.value(1));
	}
	if (exec){
		//qDebug() << "GET FAVORITES successfully";
	}else{
		qWarning() << "Unable to get list of cinemas" << db->lastError();
	}
	return list;
}


QString Database::cinemasId()
{
	QString result;
	if (!db->isOpen())
		db->open();
	QSqlQuery query;
	query.prepare("SELECT id FROM cinemas");

	bool exec = query.exec();

	while (query.next()){
		result += query.value(0).toString() + ",";
	}
	result.chop(1);
	if (exec){
		//qDebug() << "GET FAVORITES successfully";
	}else{
		qWarning() << "Unable to get list of cinemas" << db->lastError();
	}
	qDebug() << "CINEMAS ID:" << result;
	return result;
}

bool Database::clearCinemas()
{
	if (!db->isOpen())
		db->open();
	QSqlQuery query;
	query.prepare("DELETE FROM cinemas");

	bool exec = query.exec();

	if (exec){
		//qDebug() << "GET FAVORITES successfully";
		emit cinemasChanged();
	}else{
		qWarning() << "Unable to delete cinemas" << db->lastError();
	}
	return exec;
}


bool Database::setLogin(const QString &email, const QString &password)
{
	if (!db->isOpen())
		db->open();
	QSqlQuery query;
	query.prepare("INSERT INTO login VALUES(?, ?)");
	query.addBindValue(email);
	query.addBindValue(password);

	bool exec = query.exec();

	if (exec){
		qDebug() << "Login saved";
		emit loginChanged();
	}else{
		qWarning() << "Unable to save login details" << db->lastError();
	}
	return exec;
}


QVariantMap Database::getLogin()
{
	QVariantMap map;
	if (!db->isOpen())
		db->open();
	QSqlQuery query;
	query.prepare("SELECT * FROM login");

	bool exec = query.exec();

	while (query.next()){
		qDebug() << query.value(0);
		map.insert("email", query.value(0));
		map.insert("password", query.value(1));
	}
	if (exec){
		qDebug() << "Getlogin success";
	}else{
		qWarning() << "Unable to get login details" << db->lastError();
	}
	return map;
}


bool Database::clearLogin()
{
	if (!db->isOpen())
		db->open();
	QSqlQuery query;
	query.prepare("DELETE FROM login");

	bool exec = query.exec();

	if (exec){
		qDebug() << "LOGIN DELETED";
		emit loginChanged();
	}else{
		qWarning() << "Unable to delete login details" << db->lastError();
	}
	return exec;
}
