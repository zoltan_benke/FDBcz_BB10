/*
 * settings.h
 *
 *  Created on: 24.2.2014
 *      Author: Benecore
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <QObject>
#include <QSettings>
#include <QDebug>
#include <bb/cascades/CustomControl>

using namespace bb::cascades;

class Settings: public bb::cascades::CustomControl
{
	Q_OBJECT
	// General settings
	Q_PROPERTY(QString language READ language WRITE setLanguage NOTIFY settingsChanged)
	Q_PROPERTY(bool showLogo READ showLogo WRITE setShowLogo NOTIFY settingsChanged)
	Q_PROPERTY(bool showStationName READ showStationName WRITE setShowStationName NOTIFY settingsChanged)
	Q_PROPERTY(bool showFirstProgram READ showFirstProgram WRITE setShowFirstProgram NOTIFY settingsChanged)
	Q_PROPERTY(bool showSecondProgram READ showSecondProgram WRITE setShowSecondProgram NOTIFY settingsChanged)
	Q_PROPERTY(bool showActive READ showActive WRITE setShowActive NOTIFY settingsChanged)
	Q_PROPERTY(QString imageUrl READ imageUrl CONSTANT)
	Q_PROPERTY(int imageQuality READ imageQuality WRITE setImageQuality NOTIFY settingsChanged)
	Q_PROPERTY(int startView READ startView WRITE setStartView NOTIFY settingsChanged)
	// Tv Tipy filtering
	Q_PROPERTY(bool filterTipy READ filterTipy WRITE setFilterTipy NOTIFY settingsChanged)
	// Font
	Q_PROPERTY(QString fontFamilyTitle READ fontFamilyTitle WRITE setFontFamilyTitle NOTIFY settingsChanged)
	Q_PROPERTY(QString fontFamilySubtitle READ fontFamilySubtitle WRITE setFontFamilySubtitle NOTIFY settingsChanged)
	Q_PROPERTY(float fontSizeTitle READ fontSizeTitle WRITE setFontSizeTitle NOTIFY settingsChanged)
	Q_PROPERTY(float fontSizeSubtitle READ fontSizeSubtitle WRITE setFontSizeSubtitle NOTIFY settingsChanged)
	// Colors
	Q_PROPERTY(QString activeColor READ activeColor WRITE setActiveColor NOTIFY settingsChanged)
	// Calendar
	Q_PROPERTY(bool manualNotify READ manualNotify WRITE setManualNotify NOTIFY settingsChanged)
	Q_PROPERTY(int reminder READ reminder WRITE setReminder NOTIFY settingsChanged)
	Q_PROPERTY(int kinoReminder READ kinoReminder WRITE setKinoReminder NOTIFY settingsChanged)
	Q_PROPERTY(bool unitedTime READ unitedTime WRITE setUnitedTime NOTIFY settingsChanged)
	// Searching
	Q_PROPERTY(int searchLimit READ searchLimit WRITE setSearchLimit NOTIFY settingsChanged)
	Q_PROPERTY(int searchLimitProgram READ searchLimitProgram WRITE setSearchLimitProgram NOTIFY settingsChanged)
	// Cache setting
	Q_PROPERTY(qint64 cacheLimit READ cacheLimit WRITE setCacheLimit NOTIFY settingsChanged)
public:
	Settings();
	virtual ~Settings();


	static Settings *instance();


	inline QString language() const { return _language; }
	void setLanguage(const QString &value){
		if (_language != value){
			_language = value;
			settings->setValue("language", _language);
			emit settingsChanged();
		}
	}
	inline bool showLogo() const { return _showLogo; }
	void setShowLogo(const bool &value){
		if (_showLogo != value){
			_showLogo = value;
			settings->setValue("showLogo", _showLogo);
			emit settingsChanged();
		}
	}
	inline bool showStationName() const { return _showStationName; }
	void setShowStationName(const bool &value){
		if (_showStationName != value){
			_showStationName = value;
			settings->setValue("showStationName", _showStationName);
			emit settingsChanged();
		}
	}
	inline bool showFirstProgram() const { return _showFirstProgram; }
	void setShowFirstProgram(const bool &value){
		if (_showFirstProgram != value){
			qDebug() << "showFirstProgramChanged" << value;
			_showFirstProgram = value;
			settings->setValue("showFirstProgram", _showFirstProgram);
			emit settingsChanged();
		}
	}
	inline bool showSecondProgram() const { return _showSecondProgram; }
	void setShowSecondProgram(const bool &value){
		if (_showSecondProgram != value){
			_showSecondProgram = value;
			settings->setValue("showSecondProgram", _showSecondProgram);
			emit settingsChanged();
		}
	}
	inline bool showActive() const { return _showActive; }
	void setShowActive(const bool &value){
		if (_showActive != value){
			_showActive = value;
			settings->setValue("showActiveStations", _showActive);
			emit settingsChanged();
		}
	}
	inline const QString imageUrl() const {
		switch (_imageQuality){
		case 0:
			return QString("http://img.fdb.cz/obrazky_mi/"); // Low quality
		case 1:
			return QString("http://img.fdb.cz/obrazky_ms/"); // Medium quality
		case 2:
			return QString("http://img.fdb.cz/obrazky_mn/"); // High quality
		default:
			return QString();
		}
	}
	inline int imageQuality() const { return _imageQuality; }
	inline void setImageQuality(const int &value){
		if (_imageQuality != value){
			_imageQuality = value;
			settings->setValue("imageQuality", _imageQuality);
			emit settingsChanged();
		}
	}
	inline int startView() const { return _startView; }
	inline void setStartView(const int &value){
		if (_startView != value){
			_startView = value;
			settings->setValue("startView", _startView);
			emit settingsChanged();
		}
	}
	inline bool filterTipy() const { return _filterTipy; }
	inline void setFilterTipy(const bool &value){
		if (_filterTipy != value){
			_filterTipy = value;
			settings->setValue("filterTipy", _filterTipy);
			emit settingsChanged();
		}
	}
	inline QString fontFamilyTitle() const { return _fontFamilyTitle; }
	inline void setFontFamilyTitle(const QString &value){
		if (_fontFamilyTitle != value){
			_fontFamilyTitle = value;
			settings->setValue("fontFamilyTitle", _fontFamilyTitle);
			emit settingsChanged();
		}
	}
	// Font
	inline QString fontFamilySubtitle() const { return _fontFamilySubtitle; }
	inline void setFontFamilySubtitle(const QString &value){
		if (_fontFamilySubtitle != value){
			_fontFamilySubtitle = value;
			settings->setValue("fontFamilySubtitle", _fontFamilySubtitle);
			emit settingsChanged();
		}
	}
	inline float fontSizeTitle() const { return _fontSizeTitle; }
	inline void setFontSizeTitle(const float &value){
		if (_fontSizeTitle != value){
			_fontSizeTitle = value;
			settings->setValue("fontSizeTitle", _fontSizeTitle);
			emit settingsChanged();
		}
	}
	inline float fontSizeSubtitle() const { return _fontSizeSubtitle; }
	inline void setFontSizeSubtitle(const float &value){
		if (_fontSizeSubtitle != value){
			_fontSizeSubtitle = value;
			settings->setValue("fontSizeSubtitle", _fontSizeSubtitle);
			emit settingsChanged();
		}
	}
	inline QString activeColor() const { return _activeColor; }
	inline void setActiveColor(const QString &value){
		if (_activeColor != value){
			_activeColor = value;
			settings->setValue("activeColor", _activeColor);
			emit settingsChanged();
		}
	}
	// Calendar
	inline bool manualNotify() const { return _manualNotify; }
	inline void setManualNotify(const bool &value){
		if (_manualNotify != value){
			_manualNotify = value;
			settings->setValue("manualNotify", _manualNotify);
			emit settingsChanged();
		}
	}
	inline int reminder() const { return _reminder; }
	inline void setReminder(const int &value){
		if (_reminder != value){
			_reminder = value;
			settings->setValue("reminder", _reminder);
			emit settingsChanged();
		}
	}
	inline int kinoReminder() const { return _kinoReminder; }
	inline void setKinoReminder(const int &value){
		if (_kinoReminder != value){
			_kinoReminder = value;
			settings->setValue("kinoReminder", _kinoReminder);
			emit settingsChanged();
		}
	}
	inline bool unitedTime() const { return _unitedTime; }
	inline void setUnitedTime(const bool &value){
		if (_unitedTime != value){
			_unitedTime = value;
			settings->setValue("unitedTime", _unitedTime);
			emit settingsChanged();
		}
	}
	inline int searchLimit() const { return _searchLimit; }
	inline void setSearchLimit(const int &value){
		if (_searchLimit != value){
			_searchLimit = value;
			settings->setValue("searchLimit", _searchLimit);
			emit settingsChanged();
		}
	}
	inline int searchLimitProgram() const { return _searchLimitProgram; }
	inline void setSearchLimitProgram(const int &value){
		if (_searchLimitProgram != value){
			_searchLimitProgram = value;
			settings->setValue("searchLimitProgram", _searchLimitProgram);
			emit settingsChanged();
		}
	}
	// Cache settings
	inline qint64 cacheLimit() const { return _cacheLimit; }
	inline void setCacheLimit(const qint64 &value){
		if (_cacheLimit != value){
			_cacheLimit = value;
			settings->setValue("cacheLimit", _cacheLimit);
			emit settingsChanged();
		}
	}

signals:
	void settingsChanged();

protected:
	QSettings *settings;


	private:
	Q_DISABLE_COPY(Settings)
	static Settings *_instance;
	// Prave bezi
	QString _language;
	bool _showLogo;
	bool _showStationName;
	bool _showFirstProgram;
	bool _showSecondProgram;
	bool _showActive;
	QString _fontFamilyTitle;
	QString _fontFamilySubtitle;
	float _fontSizeTitle;
	float _fontSizeSubtitle;
	QString _activeColor;
	int _imageQuality;
	int _startView;
	bool _filterTipy;
	bool _manualNotify;
	int _reminder;
	qint64 _cacheLimit;
	int _kinoReminder;
	bool _unitedTime;
	int _searchLimit;
	int _searchLimitProgram;
};

#endif /* SETTINGS_H_ */
