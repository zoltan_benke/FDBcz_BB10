/*
 * database.h
 *
 *  Created on: 7.3.2014
 *      Author: benecore
 */

#ifndef DATABASE_H_
#define DATABASE_H_

#include <QObject>
#include <QtSql>
#include <QList>
#include <QDebug>
#include <bb/cascades/CustomControl>

using namespace bb::cascades;

class Database : public bb::cascades::CustomControl
{
	Q_OBJECT
	Q_PROPERTY(QVariantList favoriteList READ getFavorites NOTIFY favoriteChanged)
	Q_PROPERTY(int favoriteCount READ favoriteCount NOTIFY favoriteChanged)
	Q_PROPERTY(QVariantList cinemasList READ cinemas NOTIFY cinemasChanged)
	Q_PROPERTY(int cinemasCount READ cinemasCount NOTIFY cinemasChanged)
	Q_PROPERTY(bool isLogged READ isLogged NOTIFY loginChanged)
public:
	Database();
	virtual ~Database();

	static Database *instance();
	static void destroy();

	// Favorite stations
	Q_INVOKABLE
	bool addFavorite(const QString &id);
	Q_INVOKABLE
	bool removeFavorite(const QString &id);
	Q_INVOKABLE
	bool isFavorite(const QString &id);
	Q_INVOKABLE
	QVariantList getFavorites();
	Q_INVOKABLE
	int favoriteCount();
	// Favorite cinemas
	Q_INVOKABLE
	bool addCinema(const QVariant &id, const QVariant &kino);
	Q_INVOKABLE
	bool removeCinema(const QVariant &id);
	Q_INVOKABLE
	bool isFavoriteCinema(const QVariant &id);
	Q_INVOKABLE
	int cinemasCount();
	Q_INVOKABLE
	QVariantList cinemas();
	Q_INVOKABLE
	QString cinemasId();
	Q_INVOKABLE
	bool clearCinemas();

	Q_INVOKABLE
	bool setLogin(const QString &email, const QString &password);
	Q_INVOKABLE
	QVariantMap getLogin();
	Q_INVOKABLE
	bool clearLogin();


public slots:
	void reopen();
	void close();

private slots:
	inline bool isLogged() { return !getLogin().value("email").isNull(); }

signals:
	void favoriteChanged(const QVariantList list, const int count);
	void cinemasChanged();
	void loginChanged();

protected:
	void createTables();


	private:
	Q_DISABLE_COPY(Database)
	static Database *_instance;
	QSqlDatabase *db;
};

#endif /* DATABASE_H_ */
