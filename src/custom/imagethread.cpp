/*
 * imagethread.cpp
 *
 *  Created on: 28.2.2014
 *      Author: Benecore
 */

#include "imagethread.h"
#include <QDebug>


static void checkDir()
{
	QDir dir("data/thumbnails/");
	if (!dir.exists()){
		qDebug() << "Create dir";
		bool check = dir.mkpath(dir.path());
		Q_ASSERT(check);
		Q_UNUSED(check);
	}
}


ImageThread::ImageThread(QString hash, QByteArray data, QObject *parent) :
		QObject(parent),
		_hash(hash),
		_data(data)
{

}

ImageThread::~ImageThread() {
}

void ImageThread::process()
{
	checkDir();
	if (this->_data.isEmpty()){
		getImage();
	}else{
		saveImage();
	}
}

void ImageThread::getImage()
{
	QByteArray data;
	QFile image(QString("data/thumbnails/%1").arg(_hash));
	if (!image.open(QIODevice::ReadOnly)){
		data = QByteArray();
	}else{
		data = image.readAll();
		image.close();
	}
	emit imageData(data);
	emit finished();
}

void ImageThread::saveImage()
{
	QFile image(QString("data/thumbnails/%1").arg(_hash));
	if (!image.open(QIODevice::WriteOnly)){
		qWarning() << Q_FUNC_INFO << "Unable to open file" << image.errorString();
		emit finished();
		return;
	}
	image.write(this->_data);
	image.close();
	emit finished();
}
