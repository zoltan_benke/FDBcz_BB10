/*
 * calendarhandler.h
 *
 *  Created on: 13.3.2014
 *      Author: benecore
 */

#ifndef CALENDARHANDLER_H_
#define CALENDARHANDLER_H_

#include <QObject>
#include <bb/system/SystemListDialog>
#include <bb/pim/calendar/CalendarFolder>
#include <bb/pim/calendar/CalendarEvent>
#include <bb/pim/calendar/CalendarService>
#include <bb/pim/calendar/Result>
#include <bb/pim/calendar/EventSearchParameters>
#include <bb/system/SystemToast>
#include <QVariantMap>
#include <bb/cascades/CustomControl>
#include "helper.h"
#include "invoker.h"

using namespace bb::system;
using namespace bb::pim::calendar;
using namespace bb::cascades;

class CalendarHandler: public bb::cascades::CustomControl {
	Q_OBJECT
public:
	CalendarHandler();
	virtual ~CalendarHandler();

	static CalendarHandler *instance();
	static void destroy();

	Q_INVOKABLE
	void addEvent(const int& timestamp,
			const int &duration,
			const QVariantMap &item);
	Q_INVOKABLE
	void addCinemaEvent(const int& timestamp,
			const int &duration,
			const QVariantMap &item);

private slots:
	void finished(bb::system::SystemUiResult::Type value);
	void listFinished(bb::system::SystemUiResult::Type value);

	void process(const int &reminder = 0);


private:
	Q_DISABLE_COPY(CalendarHandler)
	static CalendarHandler *_instance;
	Invoker *invoker;
	CalendarService *calendarService;
	SystemToast *toast;
	SystemListDialog *listDialog;
	QVariantMap _item;
	int _timestamp;
	int _duration;
	QList<QString> _dayNotify;
	QList<QString> _cinemaNotify;
	bool _isKino;
	int _reminder;
};

#endif /* CALENDARHANDLER_H_ */
