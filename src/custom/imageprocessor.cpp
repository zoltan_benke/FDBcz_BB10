/* Copyright (c) 2012, 2013  BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "imageprocessor.hpp"

//! [0]
ImageProcessor::ImageProcessor(const QString &fileName, QObject *parent)
: QObject(parent)
, m_fileName(fileName)
{
}


ImageProcessor::ImageProcessor(const QString &fileName, const QByteArray &imageData, QObject *parent)
: QObject(parent)
, m_fileName(fileName)
, m_data(imageData)
{
}
//! [0]

//! [1]
QImage ImageProcessor::start()
{
	QImage image;

	if (m_data.isEmpty()){
		m_data = getImage();
	}else{
		saveImage();
	}

	image.loadFromData(m_data);

	//image = image.scaled(768, 500, Qt::KeepAspectRatioByExpanding);

	// Image processing goes here, example could be adding water mark to the downloaded image

	return image;
}
//! [1]

QByteArray ImageProcessor::getImage()
{
	QByteArray data;
	QFile image(QString("data/thumbnails/%1").arg(m_fileName));
	if (!image.open(QIODevice::ReadOnly)){
		data = QByteArray();
	}else{
		data = image.readAll();
		image.close();
	}
	return data;
}



void ImageProcessor::saveImage()
{
	QFile image(QString("data/thumbnails/%1").arg(m_fileName));
	if (!image.open(QIODevice::WriteOnly)){
		qWarning() << Q_FUNC_INFO << "Unable to open file" << image.errorString();
		return;
	}
	image.write(m_data);
	image.close();
}
