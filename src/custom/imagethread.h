/*
 * imagethread.h
 *
 *  Created on: 28.2.2014
 *      Author: Benecore
 */

#ifndef IMAGETHREAD_H_
#define IMAGETHREAD_H_

#include <QObject>
#include <QFile>
#include  <bb/cascades/ImageView>
#include "helper.h"

using namespace bb::cascades;

class ImageThread: public QObject {
	Q_OBJECT
public:
	ImageThread(QString hash, QByteArray data = "", QObject *parent = 0); // save
	virtual ~ImageThread();


signals:
	void finished();
	void imageData(QByteArray data);

public slots:
	void process();
	void getImage();
	void saveImage();


private:
	QString _hash;
	QByteArray _data;
};

#endif /* IMAGETHREAD_H_ */
