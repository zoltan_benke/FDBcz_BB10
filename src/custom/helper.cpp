#include "helper.h"
#include <time.h>

Helper *Helper::_instance = 0;


Helper::Helper()
{
	invoker = new Invoker;
}


Helper::~Helper()
{
	delete invoker;
}


Helper *Helper::instance()
{
	if (!_instance)
		_instance = new Helper;
	return _instance;
}

void Helper::destroy()
{
	if (_instance){
		delete _instance;
		_instance = 0;
	}
}


QString Helper::getMonthName(const int& month)
{
	QSettings settings("DevPDA Inc.", "FDB.cz");
	QString lang = settings.value("language", "cs").toString();
	QString monthString = QLocale((lang == "cs") ? QLocale::Czech : QLocale::Slovak).standaloneMonthName(QDate::currentDate().addMonths(month).month());
	monthString = monthString.left(1).toUpper()+monthString.mid(1);
	return monthString;
}

// Default input "yyyy-MM-dd HH:mm"
int Helper::getTimestamp(const QString& dateTime, const QString& inputFormat)
{
	return QDateTime::fromString(dateTime, inputFormat).toUTC().toTime_t();
}


QString Helper::getDate(const int &day, const QVariant &format) {
	if (format.isNull()){
		return QDate::currentDate().addDays(day).toString(Qt::ISODate);
	}else{
		return QDate::currentDate().addDays(day).toString(format.toString());
	}
}


int Helper::getDuration(const QString& startTime, const QString& endTime)
{
	QDateTime start = QDateTime::fromString(startTime, "hh:mm:ss");
	QDateTime end = QDateTime::fromString(endTime, "hh:mm:ss");

	qint64 span = end.toMSecsSinceEpoch() - start.toMSecsSinceEpoch();

	int result = (span/1000)/60;

	qDebug() << "SPAN TIME:" << span;

	return result;
}


QString Helper::increaseTime(const QString &dateTime, const int& seconds, const QString &outputFormat,  const QString &inputFormat)
{
	// Output/Input default yyyy-MM-dd HH:mm::ss
	// 2014-3-20 20:15:00
	return QDateTime::fromString(dateTime, inputFormat).addSecs(seconds).toString(outputFormat);
}


QVariant Helper::increaseDate(const QVariant &date, const int &minute)
{
	return QDateTime::fromString(date.toString(), "dd.MM.yyyyTHH:mm").addSecs((minute*60)).toString("dd.MM.yyyyTHH:mm");
}


QString Helper::getTime(const int &sec) {
	return QTime::currentTime().addSecs(sec).toString("HH:mm");
}


float Helper::getPercentage(QVariant start, QVariant end)
{
	uint startTimestamp = QDateTime::fromString(start.toString(), "hh:mm").toTime_t();
	uint currentTimestamp = QDateTime::currentDateTime().toUTC().toTime_t();
	uint endTimestamp = QDateTime::fromString(end.toString(), "hh:mm").toTime_t();
	qDebug() << "START: " << startTimestamp << " END: " << endTimestamp << " CURRENT: " << currentTimestamp;

	int total = endTimestamp - startTimestamp;
	int elapsed = currentTimestamp - startTimestamp;

	float percentage = (elapsed/total)*100;
	qDebug() << percentage;
	return percentage;
}

qint64 Helper::getThumbnailSize()
{
	QDir dir("data/thumbnails");
	long int sizex = 0;
	QFileInfoList list = dir.entryInfoList(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);
	foreach(const QFileInfo &file, list){
		if (!file.isDir()){
			sizex += file.size();
		}
	}
	return sizex;
}


int Helper::thumbnailCount()
{
	QDir dir("data/thumbnails");
	return dir.entryInfoList(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks).count();
}

bool Helper::clearThumbnails(const QString & dirName)
{
	bool result = true;
	QDir dir(dirName);
	QFileInfoList list = dir.entryInfoList(QDir::Dirs | QDir::AllDirs | QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);
	qDebug() << "LIST COUNT:" << list.count();
	foreach(const QFileInfo &info, list) {
		if (info.isDir()) {
			result = clearThumbnails(info.absoluteFilePath());
		}
		else {
			//qDebug() << "Removing file" << info.absoluteFilePath();
			result = QFile::remove(info.absoluteFilePath());
		}

		if (!result) {
			return result;
		}
	}
	emit thumbnailsChanged();
	return result;
}



QString Helper::convSize(const qint64 &size)
{
	QString temp;
	if (size < 1025){
		temp.append(QString("%1 B").arg(size));
	}
	else if (1024 < size && size < 1048577){
		temp.append(QString("%1 kB").arg(QString::number(size/1024.0, 'f', 1)));
	}
	else if (1048576 < size && size < 1073741825){
		temp.append(QString("%1 MB").arg(QString::number(size/1048576.0, 'f', 2)));
	}
	else{
		temp.append(QString("%1 GB").arg(QString::number(size/1073741824.0, 'f', 2)));
	}
	return temp;
}

void Helper::openInBrowser(const QString& id)
{
	qDebug() << Q_FUNC_INFO;
	invoker->setTarget("sys.browser");
	invoker->setAction("bb.action.OPEN");
	invoker->setUri(QString("http://www.fdb.cz/film.php?id=%1").arg(id));
	invoker->invoke();
}


void Helper::invokeBrowser(const QString& url)
{
	qDebug() << Q_FUNC_INFO;
	invoker->setTarget("sys.browser");
	invoker->setAction("bb.action.OPEN");
	invoker->setUri(url);
	invoker->invoke();
}

bool Helper::saveFile(const QString& path)
{
	qDebug() << "Saving to" << path;
	if (QFile::exists(path)){
		Q_ASSERT(QFile::remove(path));
	}
	bool check = QFile::copy("data/database.db", path);
	Q_ASSERT(check);
	return check;
}

bool Helper::restoreFile(const QString& path)
{
	Database::instance()->close();
	bool remove = QFile::remove("data/database.db");
	Q_ASSERT(remove);
	bool check = QFile::copy(path, "data/database.db");
	Q_ASSERT(check);
	if (check){

	}
	return check;
}
