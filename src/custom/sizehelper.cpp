/*
 * sizehelper.cpp
 *
 *  Created on: 22.12.2013
 *      Author: Benecore
 */

#include "sizehelper.h"
#include <QDebug>


SizeHelper *SizeHelper::_instance = 0;

SizeHelper::SizeHelper()
{
    // TODO Auto-generated constructor stub
    _orientationSupport = OrientationSupport::instance();
    switch(_orientationSupport->orientation()){
        case UIOrientation::Portrait:
            _currentOrientation = PORTRAIT;
            break;
        case UIOrientation::Landscape:
            _currentOrientation = LANDSCAPE;
            break;
        default:
            break;
    }
    connect(_orientationSupport, SIGNAL(orientationAboutToChange(bb::cascades::UIOrientation::Type)),
            this, SLOT(orientationAboutToChange(bb::cascades::UIOrientation::Type)));
}

SizeHelper::~SizeHelper() {
    // TODO Auto-generated destructor stub
    delete _orientationSupport;
}


SizeHelper *SizeHelper::instance()
{
    if (!_instance)
        _instance = new SizeHelper;
    return _instance;
}

QString SizeHelper::orientationString() const
{
    switch(_currentOrientation){
        case PORTRAIT:
            return QString("portrait");
        case LANDSCAPE:
            return QString("landscape");
        default:
            return QString();
    }
}

void SizeHelper::orientationAboutToChange(bb::cascades::UIOrientation::Type orientation)
{
    switch(orientation){
        case UIOrientation::Portrait:
            _currentOrientation = PORTRAIT;
            qDebug() << "Portrait orientation";
            break;
        case UIOrientation::Landscape:
            _currentOrientation = LANDSCAPE;
            qDebug() << "Landscape orientation";
            break;
        default:
            qDebug() << "Unknown orientation";
            break;
    }
    emit orientationStringChanged(orientationString());
    emit headerHeightChanged(headerHeight());
    emit maxWidthChanged(maxWidth());
    emit maxHeightChanged(maxHeight());
}


int SizeHelper::maxWidth() const
{
    switch(_currentOrientation){
        case PORTRAIT:
            return _displayInfo.pixelSize().width();
        case LANDSCAPE:
            return _displayInfo.pixelSize().height();
        default:
            return 0;
    }
}

int SizeHelper::maxHeight() const
{
    switch(_currentOrientation){
        case PORTRAIT:
            return _displayInfo.pixelSize().height();
        case LANDSCAPE:
            return _displayInfo.pixelSize().width();
        default:
            return 0;
    }
}

int SizeHelper::headerHeight() const
{
    switch(_currentOrientation){
        case PORTRAIT:
            return maxWidth() == 720 ? 100 : 120;
        case LANDSCAPE:
            return maxWidth() == 720 ? 80 : 100;
        default:
            return 0;
    }

}
