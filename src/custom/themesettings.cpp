/*
 * simplesettings.cpp
 *
 *  Created on: 4.1.2014
 *      Author: Benecore
 */

#include "themesettings.h"
#include <QDebug>

ThemeSettings *ThemeSettings::_instance = 0;

ThemeSettings::ThemeSettings() :
		settings("themeSettings")
{
	qDebug() << "Physical keyboard: " << info.isPhysicalKeyboardDevice();
	_themeString = getThemeString("theme", info.isPhysicalKeyboardDevice() ? "dark" : "bright");
}

ThemeSettings::~ThemeSettings() {
	// TODO Auto-generated destructor stub
}


ThemeSettings *ThemeSettings::instance()
{
	if (!_instance)
		_instance = new ThemeSettings;
	return _instance;
}


void ThemeSettings::destroy()
{
	if (_instance){
		delete _instance;
		_instance = 0;
	}
}

QString ThemeSettings::getThemeString(const QString &objectName, const QString &defaultValue) {

        // If no value has been saved, return the default value.
        if (settings.value(objectName).isNull()) {
        		if (defaultValue.isEmpty()){
        			return info.isPhysicalKeyboardDevice() ? "dark" : "bright";
        		}
                return defaultValue;
        }

        // Otherwise, return the value stored in the settings object.
        return settings.value(objectName).toString();
}

void ThemeSettings::setThemeString(const QString &objectName, const QString &inputValue) {
        // A new value is saved to the application settings object.

		if (_themeString != inputValue){
			_themeString = inputValue;
			settings.setValue(objectName, QVariant(inputValue));
			emit themeSaved();
		}
}
