/*
 * calendarhandler.cpp
 *
 *  Created on: 13.3.2014
 *      Author: benecore
 */

#include "calendarhandler.h"
#include <QDebug>

CalendarHandler *CalendarHandler::_instance = 0;

CalendarHandler::CalendarHandler()
{
	calendarService = new CalendarService;
	invoker = new Invoker;
	invoker->setTarget("sys.pim.calendar.viewer.eventcreate");
	invoker->setAction("bb.action.CREATE");
	invoker->setMimeType("text/calendar");
	toast = new SystemToast;
	toast->setPosition(SystemUiPosition::MiddleCenter);
	toast->setAutoUpdateEnabled(true);
	toast->button()->setLabel("");
	connect(toast, SIGNAL(finished(bb::system::SystemUiResult::Type)),
			this,
			SLOT(finished(bb::system::SystemUiResult::Type)));
	listDialog = new SystemListDialog;
	listDialog->setTitle(trUtf8("Warn before program"));
	listDialog->setSelectionMode(ListSelectionMode::Single);
	listDialog->cancelButton()->setLabel(tr("Cancel"));
	connect(listDialog, SIGNAL(finished(bb::system::SystemUiResult::Type)), this,
			SLOT(listFinished(bb::system::SystemUiResult::Type)));

	_dayNotify << trUtf8("0 min") << trUtf8("5 min") << trUtf8("10 min") << trUtf8("20 min") << trUtf8("30 min") << trUtf8("1 hour");
	_cinemaNotify << trUtf8("1 day") << trUtf8("2 days") << trUtf8("3 days") << trUtf8("4 days") << trUtf8("5 days");
}

CalendarHandler::~CalendarHandler()
{
}


CalendarHandler *CalendarHandler::instance()
{
	if (!_instance)
		_instance = new CalendarHandler;
	return _instance;
}


void CalendarHandler::destroy()
{
	if (_instance){
		delete _instance;
		_instance = 0;
	}
}


void CalendarHandler::addEvent(const int& timestamp,
		const int &duration,
		const QVariantMap &item)
{
	_isKino = false;
	_timestamp = timestamp;
	_duration = duration;
	_item = item;
	QSettings settings("DevPDA Inc.", "FDB.cz");

	int currentTimestamp = QDateTime::currentDateTime().toTime_t();

	if (currentTimestamp > timestamp){
		toast->setBody(trUtf8("Program is no longer possible to warn"));
		toast->show();
		return;
	}

	if (!settings.value("unitedTime", false).toBool() && !settings.value("manualNotify", false).toBool()){
		listDialog->clearList();
		listDialog->appendItems(_dayNotify);
		listDialog->show();
	}else{
		process();
	}
}


void CalendarHandler::addCinemaEvent(const int& timestamp,
		const int &duration,
		const QVariantMap &item)
{
	_isKino = true;
	_timestamp = timestamp;
	_duration = duration;
	_item = item;
	QSettings settings("DevPDA Inc.", "FDB.cz");

	int currentTimestamp = QDateTime::currentDateTime().toTime_t();

	if (currentTimestamp > timestamp){
		toast->setBody(trUtf8("Program is no longer possible to warn"));
		toast->show();
		return;
	}

	if (!settings.value("unitedTime", false).toBool() && !settings.value("manualNotify", false).toBool()){
		listDialog->clearList();
		listDialog->appendItems(_cinemaNotify);
		listDialog->show();
	}else{
		process();
	}
}


void CalendarHandler::finished(bb::system::SystemUiResult::Type value)
{
	SystemToast *toast = qobject_cast<SystemToast*>(sender());
	Q_UNUSED(toast);
	if (value == SystemUiResult::ButtonSelection){
		qDebug() << "Button clicked";
	}
}

void CalendarHandler::listFinished(bb::system::SystemUiResult::Type value)
{
	if (value == SystemUiResult::ConfirmButtonSelection){
		qDebug() << "Selected index:" << listDialog->selectedIndices().at(0);
		if (_isKino){
			switch(listDialog->selectedIndices().at(0)){
			case 0:
				_reminder = 1440; break;
			case 1:
				_reminder = 2880; break;
			case 2:
				_reminder = 4320; break;
			case 3:
				_reminder = 5760; break;
			case 4:
				_reminder = 7200; break;
			}
		}else{
			switch(listDialog->selectedIndices().at(0)){
			case 0:
				_reminder = 0; break;
			case 1:
				_reminder = 5; break;
			case 2:
				_reminder = 10; break;
			case 3:
				_reminder = 20; break;
			case 4:
				_reminder = 30; break;
			case 5:
				_reminder = 60; break;
			}
		}
		process(_reminder);
	}
	// _dayNotify << trUtf8("0 minút") << trUtf8("5 minút") << trUtf8("10 minút") << trUtf8("20 minút") << trUtf8("30 minút");
	// _cinemaNotify << trUtf8("1 deň") << trUtf8("2 dni") << trUtf8("3 dni") << trUtf8("4 dni");
}

void CalendarHandler::process(const int &reminder)
{
	QSettings settings("DevPDA Inc.", "FDB.cz");



	qDebug() << "Title:" << _item.value("title").toString() << endl \
			<< "Body:" << _item.value("body").toString() << endl \
			<< "Station:" << _item.value("station").toString() << endl \
			<< "Guid:" << _item.value("guid").toString();

	QDateTime dt;
	dt.setTime_t(_timestamp);

	const QDateTime start = dt;
	const QDateTime end = dt.addSecs((_duration*60));

	QString label = QString("%1 %2").arg(_item.value("title").toString()).arg(!_item.value("station").toString().isEmpty() ? QString("(%1)").arg(_item.value("station").toString()) : "");

	QString body = _item.value("body").toString();

	if (settings.value("unitedTime").toBool()){
		if (_isKino){
			qDebug() << "IS KINO";
			switch(settings.value("kinoReminder", -1).toInt()){
			case -1:
				_reminder = 1440; break;
			case -2:
				_reminder = 2880; break;
			case -3:
				_reminder = 4320; break;
			case -4:
				_reminder = 5760; break;
			case -5:
				_reminder = 7200; break;
			}
		}else{
			_reminder = settings.value("reminder", 15).toInt();
		}
	}else{
		_reminder = reminder;
	}

	QPair<AccountId, FolderId> defaultCalendar = calendarService->defaultCalendarFolder();

	// Checking existing Event
	bool exists = false;
	EventSearchParameters params;
	params.setStart(start);
	params.setEnd(end);
	QList<CalendarEvent> events = calendarService->events(params);
	qDebug() << "COUNT OF EVENTS" << events.count();
	foreach(const CalendarEvent &event, events){
		qDebug() << event.subject() << endl << "GUID" << event.guid();
		if (event.guid() == _item.value("guid").toString()){
			exists = true;
			break;
		}
	}
	// end of Checking Event
	if (exists){
		toast->setBody(trUtf8("Is already on the calendar"));
		toast->show();
		return;
	}

	if (settings.value("manualNotify").toBool()){
		QVariantMap map;
		map.insert("accountId", defaultCalendar.first);
		map.insert("folderId", defaultCalendar.second);
		map.insert("startTime", start.toString("yyyy-MM-dd HH:mm:ss"));
		map.insert("duration", _duration);
		map.insert("subject", label);
		map.insert("location", _item.value("location").toString());
		map.insert("body", body);
		invoker->setData(Helper::instance()->encodeObject(map));
		invoker->invoke();
		return;
	}


	CalendarEvent event;
	event.setStartTime(start);
	event.setEndTime(end);
	event.setSensitivity(Sensitivity::Personal);
	event.setSubject(label);
	event.setAccountId(defaultCalendar.first);
	event.setFolderId(defaultCalendar.second);
	if (!_item.value("location").toString().isEmpty()){
		event.setLocation(_item.value("location").toString());
	}
	if (!body.isEmpty()){
		event.setBody(body);
	}
	event.setReminder(_reminder);
	if (!_item.value("guid").toString().isEmpty()){
		event.setGuid(_item.value("guid").toString());
	}

	Result::Type result = calendarService->createEvent(event);
	if (result == Result::Success){
		toast->setBody(trUtf8("Notify was added"));
	}else{
		toast->setBody(trUtf8("Unable to add notify"));
	}
	toast->show();
}
