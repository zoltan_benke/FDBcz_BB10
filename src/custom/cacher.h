/*
 * cacher.h
 *
 *  Created on: 6.3.2014
 *      Author: benecore
 */

#ifndef CACHER_H_
#define CACHER_H_

#include <QObject>
#include <QFile>
#include <QDir>
#include <bb/cascades/CustomControl>

using namespace bb::cascades;


class Cacher : public bb::cascades::CustomControl
{
	Q_OBJECT
public:
	Cacher();
	virtual ~Cacher();

	static Cacher *instance();
	static void destroy();



public slots:
	void storeCache(const QByteArray &data);
	QByteArray getCache();


private:
	Q_DISABLE_COPY(Cacher)
	static Cacher *_instance;
	QFile *_cache;

};

#endif /* CACHER_H_ */
