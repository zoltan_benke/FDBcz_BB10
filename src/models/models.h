/*
 * models.h
 *
 *  Created on: 6.3.2014
 *      Author: benecore
 */

#ifndef MODELS_H_
#define MODELS_H_

#include <QObject>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/CustomControl>

using namespace bb::cascades;

class Models : public bb::cascades::CustomControl
{
	Q_OBJECT
	Q_PROPERTY(bb::cascades::GroupDataModel *baseModel READ baseModel CONSTANT)
	Q_PROPERTY(int baseModelSize READ baseModelSize NOTIFY modelSizeChanged)
	Q_PROPERTY(bb::cascades::GroupDataModel *favoriteModel READ favoriteModel CONSTANT)
	Q_PROPERTY(int favoriteModelSize READ favoriteModelSize NOTIFY modelSizeChanged)
	Q_PROPERTY(bb::cascades::GroupDataModel *vysielanieModel READ vysielanieModel CONSTANT)
	Q_PROPERTY(int vysielanieModelSize READ vysielanieModelSize NOTIFY modelSizeChanged)
public:
	Models();
	virtual ~Models();

	static Models *instance();
	static void destroy();

signals:
	void modelSizeChanged();


public slots:
	inline GroupDataModel *baseModel() const { return _baseModel; };
	inline GroupDataModel *favoriteModel() const { return _favoriteModel; };
	inline GroupDataModel *vysielanieModel() const { return _vysielanieModel; };

	inline int baseModelSize() const { return _baseModel->size(); }
	inline int favoriteModelSize() const { return _baseModel->size(); }
	inline int vysielanieModelSize() const { return _vysielanieModel->size(); }


private:
	Q_DISABLE_COPY(Models)
	static Models *_instance;
	GroupDataModel *_baseModel;
	GroupDataModel *_favoriteModel;
	GroupDataModel *_vysielanieModel;
};

#endif /* MODELS_H_ */
