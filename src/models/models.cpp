/*
 * models.cpp
 *
 *  Created on: 6.3.2014
 *      Author: benecore
 */

#include "models.h"

Models *Models::_instance = 0;

Models::Models()
{
	_baseModel = new GroupDataModel();
	_baseModel->setGrouping(ItemGrouping::None);
	_baseModel->setSortingKeys(QStringList() << "name");
	_favoriteModel = new GroupDataModel();
	_favoriteModel->setGrouping(ItemGrouping::None);
	_favoriteModel->setSortingKeys(QStringList() << "name");
	_vysielanieModel = new GroupDataModel();
	_vysielanieModel->setGrouping(ItemGrouping::None);
	_vysielanieModel->setSortingKeys(QStringList() << "alarm");
}

Models::~Models()
{
	delete _baseModel;
	delete _favoriteModel;
	delete _vysielanieModel;
}


Models *Models::instance()
{
	if (!_instance)
		_instance = new Models;
	return _instance;
}


void Models::destroy()
{
	if (_instance){
		delete _instance;
		_instance = 0;
	}
}
