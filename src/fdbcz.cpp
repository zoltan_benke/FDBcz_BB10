/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "fdbcz.hpp"
#include <QTextCodec>

#include <bb/cascades/maps/MapView>
#include <bb/cascades/maps/MapData>
#include <bb/cascades/maps/DataProvider>
#include <bb/platform/geo/Point>
#include <bb/platform/geo/GeoLocation>
#include <bb/platform/geo/Marker>
#include <bb/UIToolkitSupport>

using namespace bb;
using namespace bb::cascades::maps;
using namespace bb::platform::geo;
using namespace bb::cascades;
using namespace bb::data;

FDBcz::FDBcz(bb::cascades::Application *app) :
						QObject(app), _loading(false), _baseModel(0), _favoriteModel(0)
{
	//connect(app, SIGNAL(thumbnail()), this, SLOT(onThumbnail())); // presun do thumbnailu
	//connect(app, SIGNAL(awake()), this, SLOT(onAwake())); // prebudenie z thumbnailu

	QTextCodec::setCodecForTr(QTextCodec::codecForName("utf8"));
	QTextCodec::setCodecForCStrings(QTextCodec::codecForName("utf8"));
	QTextCodec::setCodecForLocale(QTextCodec::codecForName("utf8"));

	const QString uuid(QLatin1String("f9594a6e-1d89-41e1-b5ca-92c35c98b352"));

	_registrationHandler = new RegistrationHandler(uuid);

	_inviteDownload = new InviteToDownload(_registrationHandler->context());

	api = new FDBLib(this);
	api->setSynchronous(false);
	connect(api, SIGNAL(requestReady(const QByteArray, const QNetworkReply*)), this, SLOT(requestReady(const QByteArray, const QNetworkReply*)));
	connect(api, SIGNAL(error(int, QString)), this, SLOT(error(int, QString)));


	QVariantMap loginDetails = Database::instance()->getLogin();
	if (!loginDetails.value("email").isNull() || !loginDetails.value("password").isNull()){
		api->setLoginDetails(loginDetails.value("email").toString(),
				loginDetails.value("password").toString());
	}


	settings = Settings::instance();
	sizeHelper = SizeHelper::instance();

	// prepare the localization
	m_pTranslator = new QTranslator(this);
	m_pLocaleHandler = new LocaleHandler(this);

	//bool res = QObject::connect(m_pLocaleHandler, SIGNAL(systemLanguageChanged()), this, SLOT(onSystemLanguageChanged()));
	// This is only available in Debug builds
	//Q_ASSERT(res);
	// Since the variable is not used in the app, this is added to avoid a
	// compiler warning
	//Q_UNUSED(res);

	// initial load
	//onSystemLanguageChanged();
	changeLanguage(settings->language());

	// Register types
	qmlRegisterType<WebImageView>("com.devpda.tools", 1, 2, "RemoteImage");
	qmlRegisterType<Timer>("com.devpda.tools", 1, 2, "Timer");
	qmlRegisterType<Invoker>("com.devpda.tools", 1, 2, "Invoker");
	qmlRegisterType<MovieItem>("com.devpda.items", 1, 2, "MovieItem");
	qmlRegisterType<ActorItem>("com.devpda.items", 1, 2, "ActorItem");
	// Create scene document from main.qml asset, the parent is set
	// to ensure the document gets destroyed properly at shut down.
	QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);

	QDeclarativePropertyMap *dbFilename = new QDeclarativePropertyMap;
	dbFilename->insert("dbFile", QVariant(QString(
			QDir::homePath() + "/database.db")));
	qDebug() << "DB PATH:" << QDir::homePath() + "/database.db";
	qml->setContextProperty("filenames", dbFilename);

	qml->setContextProperty("App", this);
	qml->setContextProperty("Api", api);
	qml->setContextProperty("Settings", settings);
	qml->setContextProperty("SizeHelper", sizeHelper);
	qml->setContextProperty("ThemeSettings", ThemeSettings::instance());
	qml->setContextProperty("Models", Models::instance());
	qml->setContextProperty("Database", Database::instance());
	qml->setContextProperty("Cacher", Cacher::instance());
	qml->setContextProperty("Helper", Helper::instance());
	qml->setContextProperty("BBM", _registrationHandler);
	qml->setContextProperty("Invite", _inviteDownload);
	qml->setContextProperty("Calendar", CalendarHandler::instance());


	qDebug() << "CURRENT PATH" << QDir::currentPath();
	// Create root object for the UI
	AbstractPane *root = qml->createRootObject<AbstractPane>();

	_baseModel = root->findChild<GroupDataModel*>("baseModel");
	_favoriteModel = root->findChild<GroupDataModel*>("favoriteModel");
	if (!_baseModel || !_favoriteModel){
		qWarning() << "Can't find models";
	}
	// Set the cover
	app->setCover(new Frame());
	// Set created root object as the application scene
	app->setScene(root);

	new QmlBeam(this);
}



FDBcz::~FDBcz()
{
	delete api;
	delete settings;
	delete sizeHelper;
	ThemeSettings::destroy();
	Models::destroy();
	Database::destroy();
	Helper::destroy();
	delete _registrationHandler;
	delete _inviteDownload;
	CalendarHandler::destroy();
}


void FDBcz::onSystemLanguageChanged()
{
	QCoreApplication::instance()->removeTranslator(m_pTranslator);
	// Initiate, load and install the application translation files.
	QString locale_string = QLocale().name();
	QString file_name = QString("FDBcz_%1").arg(locale_string);
	if (m_pTranslator->load(file_name, "app/native/qm")) {
		QCoreApplication::instance()->installTranslator(m_pTranslator);
	}
}


void FDBcz::changeLanguage(const QString& language)
{
	QCoreApplication::instance()->removeTranslator(m_pTranslator);
	// Initiate, load and install the application translation files.
	QString file_name = QString("FDBcz_%1").arg(language);
	if (m_pTranslator->load(file_name, "app/native/qm")) {
		QCoreApplication::instance()->installTranslator(m_pTranslator);
	}
}


void FDBcz::onThumbnail()
{
	qDebug() << Q_FUNC_INFO;
}

void FDBcz::onAwake()
{
	bb::cascades::Application::instance()->cover();
	qDebug() << Q_FUNC_INFO;
}


void FDBcz::loadPraveBezi()
{
	setLoading(true);
	QByteArray data = Cacher::instance()->getCache();
	if (data.isEmpty()){
		api->praveBezi();
	}else{
		JsonDataAccess jda;
		QVariantList list = jda.loadFromBuffer(data).toMap().value("data").toList();
		Models::instance()->baseModel()->insertList(list);
	}
	setLoading(false);
}


void FDBcz::addToFavorite(const QVariantList& selectedItems)
{
	qDebug() << Q_FUNC_INFO << " SELECTED ITEMS COUNT:" << selectedItems.count();
	for (int i = 0; i < selectedItems.count(); i++){
		const QVariantList &indexPath = selectedItems.at(i).toList();
		QVariantMap item = _baseModel->data(indexPath).toMap();
		qDebug() << Q_FUNC_INFO << "FAVORITE BEFORE:" << item.value("favorite").toBool() << endl;
		Q_ASSERT(_baseModel->remove(item));
		item.insert("favorite", true);
		qDebug() << Q_FUNC_INFO << "FAVORITE AFTER:" << item.value("favorite").toBool() << endl;
		_favoriteModel->insert(item);
		Q_ASSERT(Database::instance()->addFavorite(item.value("id").toString()));
	}
}

void FDBcz::removeFromFavorite(const QVariantList& selectedItems)
{
	/*var list = listView.selectionList()
	                            listView.clearSelection()
	                            for (var i = 0; i < list.length; i++){
	                                var program = listView.dataModel.data(list[i])

	                                baseModel.remove(program)
	                                program.favorite = true
	                                favoriteModel.insert(program)
	                                Database.addFavorite(program.id)
	                            }*/
	qDebug() << Q_FUNC_INFO << " SELECTED ITEMS COUNT:" << selectedItems.count();
}


void FDBcz::requestReady(const QByteArray networkReply, const QNetworkReply *reply)
{
	/*
	  PRAVE_BEZI,
        BEZI_OD,
        VYSIELANIE,
        TIPY,
        PROGRAM,
        DETAIL_FILMU,
        DETAIL_HERCA,
        HLADAT_FILM,
        HLADAT_HERCA,
        GALERIA_FILMU,

        KINO_MESTA,
        KINO_ZOZNAM_KIN,
        KINO_VYSIELANIE,
        KINO_PROGRAM,
        KINO_PREMIERY,
        KINO_VYSIELANIE_OBLUBENE
        // API 9 NOVE
        HLADAT_PROGRAM,
        // Uzivatelske skripty
        REGISTER,
        LOGIN,
        MOJ_FILM,
        HODNOTENIE,
        VIDEL_SOM,
        CHCEM_VIDIET,
        FILMOTEKA,
        MOJE_HODNOTENIA,
        MOJE_CHCEM_VIDIET,
        MOJE_VIDEL_SOM,
        MOJE_KOMENTARE,
        MOJE_VIDEL_HODNOTIL
	 */
	Q_UNUSED(reply);
	const FDBLib::FDBRequest &request = api->request();
	switch (request){
	case FDBLib::BEZI_OD:
		parseBeziOd(networkReply);
		break;
	case FDBLib::PRAVE_BEZI:
		parsePraveBezi(networkReply);
		break;
	case FDBLib::VYSIELANIE:
		parseVysielanie(networkReply);
		break;
	case FDBLib::PROGRAM:
		parseProgram(networkReply);
		break;
	case FDBLib::TIPY:
		parseTvTipy(networkReply);
		break;
	case FDBLib::DETAIL_FILMU:
		parseDetailFilmu(networkReply);
		break;
	case FDBLib::DETAIL_HERCA:
		parseDetailHerca(networkReply);
		break;
	case FDBLib::GALERIA_FILMU:
		parseGaleriaFilmu(networkReply);
		break;
	case FDBLib::HLADAT_FILM:
		parseHladatFilm(networkReply);
		break;
	case FDBLib::HLADAT_HERCA:
		parseHladatHerca(networkReply);
		break;
	case FDBLib::KINO_PREMIERY:
		parseKinoPremiery(networkReply);
		break;
	case FDBLib::KINO_MESTA:
		parseKinoMesta(networkReply);
		break;
	case FDBLib::KINO_ZOZNAM_KIN:
		parseKinoZoznam(networkReply);
		break;
	case FDBLib::KINO_PROGRAM:
		parseKinoProgram(networkReply);
		break;
	case FDBLib::KINO_VYSIELANIE_OBLUBENE:
		parseKinoProgramOblubene(networkReply);
		break;
	case FDBLib::HLADAT_PROGRAM:
		parseHladatProgram(networkReply);
		break;
	case FDBLib::REGISTER:
		parseRegister(networkReply);
		break;
	case FDBLib::LOGIN:
		parseLogin(networkReply);
		break;
	case FDBLib::MOJE_CHCEM_VIDIET:
		parseMojeChcemVidiet(networkReply);
		break;
	case FDBLib::MOJE_VIDEL_SOM:
		parseMojeVidelSom(networkReply);
		break;
	case FDBLib::MOJ_FILM:
		parseMojFilm(networkReply);
		break;
	case FDBLib::CHCEM_VIDIET:
		parseChcemVidiet(networkReply);
		break;
	case FDBLib::VIDEL_SOM:
		parseVidelSom(networkReply);
		break;
	case FDBLib::HODNOTENIE:
		parseHodnotenie(networkReply);
		break;
	case FDBLib::MOJE_HODNOTENIA:
		parseMojeHodnotenia(networkReply);
		break;
	case FDBLib::MOJE_KOMENTARE:
		parseMojeKomentare(networkReply);
		break;
	case FDBLib::MOJE_VIDEL_HODNOTIL:
		parseMojeVidelHodnotil(networkReply);
		break;
	default:
		break;
	}
}

void FDBcz::error(int errorCode, QString errorString)
{
	qDebug() << "ERROR CODE:" << errorCode << endl << \
			"ERROR STRING:" << errorString;
	setLoading(false);
}


void FDBcz::parseBeziOd(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response;
	emit clearModel();
	JsonDataAccess jda;
	QVariantList list = jda.loadFromBuffer(response).toMap().value("data").toList();
	QVariantList dbList = Database::instance()->getFavorites();
	foreach(const QVariant &item, list){
		QVariantMap itemMap = item.toMap();
		if (dbList.contains(item.toMap().value("tv_id"))){
			itemMap.insert("favorite", true);
			emit favoriteItemDone(itemMap);
		}else{
			itemMap.insert("favorite", false);
			emit baseItemDone(itemMap);
		}
	}
	emit reloadModel();
	setLoading(false);
}



void FDBcz::parsePraveBezi(const QByteArray &response)
{
	//Cacher::instance()->storeCache(response);
	emit clearModel();
	JsonDataAccess jda;
	setGenTime(jda.loadFromBuffer(response).toMap().value("gentime").toString());
	QVariantList list = jda.loadFromBuffer(response).toMap().value("data").toList();
	QVariantList dbList = Database::instance()->getFavorites();
	foreach(const QVariant &item, list){
		QVariantMap itemMap = item.toMap();
		if (dbList.contains(item.toMap().value("id"))){
			itemMap.insert("favorite", true);
			emit favoriteItemDone(itemMap);
			//Models::instance()->favoriteModel()->insert(itemMap);
		}else{
			itemMap.insert("favorite", false);
			emit baseItemDone(itemMap);
			//Models::instance()->baseModel()->insert(itemMap);
		}
	}
	//Models::instance()->baseModel()->insertList(list);
	setLoading(false);
}


void FDBcz::parseVysielanie(const QByteArray &response)
{
	qDebug() << "RESPONSE" << response << endl;
	emit vysielanieDone(response);
	/*
	qDebug() << Q_FUNC_INFO << response << endl;
	JsonDataAccess jda;
	setGenTime(jda.loadFromBuffer(response).toMap().value("gentime").toString());
	QVariantList list = jda.loadFromBuffer(response).toMap().value("data").toList();
	Models::instance()->vysielanieModel()->insertList(list);
	setLoading(false);
	 */
}

void FDBcz::parseProgram(const QByteArray &response)
{
	qDebug() << "RESPONSE" << response << endl;
	emit programDone(response);
	/*
	qDebug() << Q_FUNC_INFO << response << endl;
	JsonDataAccess jda;
	setGenTime(jda.loadFromBuffer(response).toMap().value("gentime").toString());
	QVariantList list = jda.loadFromBuffer(response).toMap().value("data").toList();
	Models::instance()->vysielanieModel()->insertList(list);
	setLoading(false);
	 */
}

void FDBcz::parseTvTipy(const QByteArray &response)
{
	qDebug() << "RESPONSE" << response << endl;
	emit tvTipyDone(response);
}

void FDBcz::parseDetailFilmu(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response << endl;
	JsonDataAccess jda;
	QVariantMap movie = jda.loadFromBuffer(response).toMap();
	emit detailFilmuDone(new MovieItem(movie));
}


void FDBcz::parseDetailHerca(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response << endl;
	JsonDataAccess jda;
	QVariantMap movie = jda.loadFromBuffer(response).toMap();
	emit detailHercaDone(new ActorItem(movie));
}


void FDBcz::parseGaleriaFilmu(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response << endl;
	emit galeriaDone(response);
}

void FDBcz::parseHladatFilm(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response;
	emit hladatDone(response);
}

void FDBcz::parseHladatHerca(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response;
	emit hladatDone(response);
}

void FDBcz::parseHladatProgram(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response;
	emit hladatProgramDone(response);
}

void FDBcz::parseKinoPremiery(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response;
	emit kinoPremieryDone(response);
}

void FDBcz::parseKinoMesta(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response;
	emit kinoMestaDone(response);
}

void FDBcz::parseKinoZoznam(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response;
	emit kinoZoznamDone(response);
}

void FDBcz::parseKinoProgram(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response;
	emit kinoProgramDone(response);
}

void FDBcz::parseKinoProgramOblubene(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response;
	JsonDataAccess jda;
	QVariantList list = jda.loadFromBuffer(response).toList();
	for (int i = 0; i < list.count(); ++i){
		QVariantMap item = list.at(i).toMap();
		QVariantList filmList = item["film"].toList();
		for (int a = 0; a < filmList.count(); ++a){
			QVariantMap filmItem = filmList.at(a).toMap();
			filmItem.insert("kinoId", item["id"]);
			filmItem.insert("kinoName", item["name"]);
			filmItem.insert("kinoCity", item["city"]);
			filmItem.insert("kinoAdress", item["adress"]);
			filmItem.insert("kinoWeb", item["web"]);
			emit kinoProgramOblubeneDone(filmItem);
		}
	}
	setLoading(false);
}

void FDBcz::parseRegister(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response;
	emit registerDone(response);
}

void FDBcz::parseLogin(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response;
	emit loginDone(response);
}

void FDBcz::parseMojeChcemVidiet(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response;
	emit mojeChcemVidietDone(response);
}

void FDBcz::parseMojeVidelSom(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response;
	emit mojeVidelSomDone(response);
}

void FDBcz::parseChcemVidiet(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response;
	emit chcemVidietDone(response);
}

void FDBcz::parseVidelSom(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response;
	emit videlSomDone(response);
}

void FDBcz::parseMojFilm(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response;
	emit mojFilmDone(response);
}

void FDBcz::parseHodnotenie(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response;
	emit hodnotenieDone(response);
}

void FDBcz::parseMojeHodnotenia(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response;
	emit mojeHodnoteniaDone(response);
}

void FDBcz::parseMojeKomentare(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response;
	emit mojeKomentareDone(response);
}

void FDBcz::parseMojeVidelHodnotil(const QByteArray& response)
{
	qDebug() << "RESPONSE:" << response;
	emit mojeVidelHodnotilDone(response);
}


// LOCATION MAP
void FDBcz::addPinToMap(QObject* mapObject, const QString& title,
		const QString& subtitle)
{
	MapView* mapView = qobject_cast<MapView*>(mapObject);
	DataProvider* deviceLocDataProv = new DataProvider("device-location-data-provider");
	mapView->mapData()->addProvider(deviceLocDataProv);
	qDebug() << Q_FUNC_INFO << mapView;
	GeoLocation* newDrop = new GeoLocation();
	newDrop->setLatitude(mapView->latitude());
	newDrop->setLongitude(mapView->longitude());
	//QString desc = QString("Coordinates: %1, %2").arg(mapView->latitude(),
	//		0, 'f', 3).arg(mapView->longitude(), 0, 'f', 3);
	newDrop->setName(title);
	newDrop->setDescription(subtitle);

	// use the marker in the assets, as opposed to the default marker
	Marker flag;
	flag.setIconUri(UIToolkitSupport::absolutePathFromUrl(
			QUrl("asset:///images/map/on_map_pin.png")));
	flag.setIconSize(QSize(60, 60));
	flag.setLocationCoordinate(QPoint(20, 59));
	flag.setCaptionTailCoordinate(QPoint(20, 1));
	newDrop->setMarker(flag);

	mapView->mapData()->add(newDrop);
}

void FDBcz::updateMarkers(QObject* mapObject, QObject* containerObject) const
{
	MapView* mapview = qobject_cast<MapView*>(mapObject);
	Container* container = qobject_cast<Container*>(containerObject);

	for (int i = 0; i < container->count(); i++) {
		const QPoint xy = worldToPixel(mapview,
				container->at(i)->property("lat").value<double>(),
				container->at(i)->property("lon").value<double>());
		container->at(i)->setProperty("x", xy.x());
		container->at(i)->setProperty("y", xy.y());
	}
}

QPoint FDBcz::worldToPixel(QObject* mapObject, double latitude,
		double longitude) const
{
	MapView* mapview = qobject_cast<MapView*>(mapObject);
	const Point worldCoordinates = Point(latitude, longitude);

	return mapview->worldToWindow(worldCoordinates);
}
