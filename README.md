## FDB.cz pre BlackBerry 10 ##

**FDB.cz** native application for the BlackBerry 10 smartphones and for all movie fans
Channels are mostly for Czech's nad Slovak's Tv

More info at [http://fdb.devpda.net](http://fdb.devpda.net)

Release notes:
-
### v1.1.6.0 (`28.05.2014`)
- [NEW] Slovak cinemas (Cinemax's network)
- [NEW] App icon + splash screens
- [FIXED] Cover image
- minor fixes 
### v1.1.5.9 (`29.04.2014`)
- [NEW] Quick switcher for filtering tv tips
- [FIXED] User buttons in movie details
- [FIXED] Show text if is no data
- minor fixes 
### v1.1.5.8 (`27.04.2014`)
- [NEW] Czech/Slovak language support
- [NEW] FDB.cz account support
- [NEW] Searching Tv programs
- [NEW] Cinemas premieres by month selection
- [NEW] Ability to show cinema on the map with navigation
- [FIXED] Showing rating in films which hasn't any
- [FIXED] Movie details indicator in cinemas
- [FIXED] Sorting cinemas program by date/time
- [FIXED] and more...
### v1.1.3.6 (`30.03.2014`)
- Improved application UI
- Searching in FDB.cz database
- Detailed info about movies/actors
- Cinemas (Czech only)
- Cinema program
- Cinema premieres
- Improved program notifications (Calendar)
- Backup/Restore favorite stations and cinemas
- and more
### v1.0.2.6 (`28.10.2013`)
- Fixed DST (Daylight saving time)
### v1.0.2.5 (`27.10.2013`)
- Added display the next program and progress indicator on 'Now in Tv'
- Fixed parsing - Station settings
### v1.0.2.4 (`27.10.2013`)
- New style of Title header
- Fixed DST (Daylight saving time)
- Fixed add notification from Tv tips when overlap two days
- Minor fixes
### v1.0.2.3 (`05.10.2013`)
- Migrate to 10.2 API
- UI improvements
- The ability change font (size, style), background, color theme
- Station logo On/Off
- Hide stations which currently `Not broadcast`
- and more 
### v1.0.0.1 (`15.07.2013`)
- Initial version