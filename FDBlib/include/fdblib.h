/*
    FDBLib (FDBLib.h)
    
    Copyright (c) 2014 Zoltán Benke (Benecore) benecore@devpda.net
                                               http://devpda.net
                                               
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
 */

#ifndef FDBLIB_H
#define FDBLIB_H

#include "fdblib_export.h"
#include <QObject>
#include <QDateTime>
#include <QPair>

class QNetworkReply;

class FDBLibPrivate;
class FDBLIBSHARED_EXPORT FDBLib : public QObject {
    Q_OBJECT
public:
    /*!
     \brief

     \param parent
    */
    FDBLib(QObject *parent = 0);
    /*!
         \brief

        */
    virtual ~FDBLib();
    /*!
         \brief

        */
    enum FDBRequest{
        PRAVE_BEZI,
        BEZI_OD,
        VYSIELANIE,
        CASOVA_OS,
        TIPY,
        PROGRAM,
        DETAIL_FILMU,
        DETAIL_HERCA,
        HLADAT_FILM,
        HLADAT_HERCA,
        GALERIA_FILMU,
        /* KINA */
        KINO_MESTA,
        KINO_ZOZNAM_KIN,
        KINO_VYSIELANIE,
        KINO_PROGRAM,
        KINO_PREMIERY,
        KINO_VYSIELANIE_OBLUBENE,
        /* API 9 NOVE */
        HLADAT_PROGRAM,
        /* Uzivatelske skripty */
        REGISTER,
        LOGIN,
        MOJ_FILM,
        HODNOTENIE,
        VIDEL_SOM,
        CHCEM_VIDIET,
        FILMOTEKA,
        MOJE_HODNOTENIA,
        MOJE_CHCEM_VIDIET,
        MOJE_VIDEL_SOM,
        MOJE_KOMENTARE,
        MOJE_VIDEL_HODNOTIL
    };

    /*!
         \brief

        */
    enum FDBError{
        NoError = 0,
        Error = 1
    };


    /*!
         \brief

         \return FDBRequest
        */
    FDBRequest request() const;

    /*!
         \brief

         \return FDBError
        */
    FDBError error() const;

    /*!
         \brief

         \return int
        */
    int errorCode() const;

    /*!
         \brief

         \return QByteArray
        */
    QString errorString() const;

    /*!
         \brief

         \return bool
        */
    bool debugEnabled() const;
    /*!
         \brief

         \param debugEnabled
        */
    void setDebugEnabled(bool &debugEnabled);
    /*!
         \brief

         \return bool
        */
    bool synchronous() const;
    /*!
         \brief

         \param value
        */
    void setSynchronous(const bool &value);

    QString apiUrl() const;

    void setApiUrl(const QString &value);
    Q_INVOKABLE
    bool isLogged() const;
    Q_INVOKABLE
    void setLoginDetails(const QString &mail, const QString &password);
    Q_INVOKABLE
    QString password() const;
    Q_INVOKABLE
    QString email() const;


Q_SIGNALS:
    void requestReady(const QByteArray networkReply, const QNetworkReply *reply = 0);
    void error(int errorCode, QString errorString);


public Q_SLOTS:
    /*!
         \brief Vrati aktualne vysielanie vsetkych/vybranej stanice

         \param id id stanice, predvolene id je 0 co znamena vsetky stanice
        */
    void praveBezi(const QString &id = "0");
    /*!
         \brief Vrati dva programy od urciteho casu

         \param od 16,18,20,22,0, predvolena hodnota je 20 (tj. 20hod)
        */
    void beziOd(const QString &od = "20");
    /*!
         \brief

         \param id id stanice
         \param date datum tvar rok-mesiac-den
        */
    void vysielanie(const QString &id,
                    const QString &date = QDate::currentDate().toString(Qt::ISODate));
    /*!
         \brief

         \param typ
        */
    void tipy(const QString &typ = "0" /*0 - dnes, 1 - zajtra, 2 - tyzden ak su data*/);
    /*!
         \brief

        */
    void program();
    /*!
         \brief Vrati detaily filmu

         \param id filmu
         \param typ 0 - vsetko, 1 - zaklad, 2 - obsah, 3 - obsadenie, 4 - komentare
         \param kina id kin oddelenych ciarkov, ak sa film hra v kinach tak k nim vrati detaily
        */
    void detailFilmu(const QString &id, const QString &typ = "0", const QString &kina = "");
    /*!
         \brief

         \param id id herca
         \param typ 0 - vsetko, 1 - zaklad, 2 - zivotopis, 3 - filmografia
        */
    void detailHerca(const QString &id, const QString &typ = "0");

    /*!
         \brief

         \param nazovFilmu hladany film
         \param limit limit vysledkov predvolena hodnota 10
        */
    void hladatFilm(const QString &nazovFilmu,
                    const bool &baseEncoded = true,
                    const QString &limit = "10");

    /*!
         \brief

         \param menoHerca meno herca
         \param limit limit vysledkov predvoleny limit je 10
        */
    void hladatHerca(const QString &menoHerca,
                     const bool &baseEncoded = true,
                     const QString &limit = "10");

    /*!
         \brief Vrati galeriu filmu podla id

         \param idFilmu id filmu
        */
    void galeriaFilmu(const QString &idFilmu);

    /*!
         \brief Vrati zoznam miest podla kraja

         \param kraj ve tvaru url:
                Karlovarský - karlovarsky
                Ústecký - ustecky
                Liberecký - liberecky
                Kálovehradecký - kralovehradecky
                Pardubický - pardubicky
                Olomoucký - olomoucky
                Moravskoslezský - moravskoslezsky
                Zlínský - zlinsky
                Jihomoravský - jihomoravsky
                Vysočina - vysocina
                Jihočeský - jihocesky
                Plzeňský - plzensky
                Hlavní město Praha - praha
                Středočeský - stredocesky
        */
    void kinoMesta(const QString &kraj);

    /*!
         \brief Vrati zoznam kin podla mesta

         \param mesto url mesta podla akcie 15
        */
    void kinoZoznamKin(const QString &mesto);

    /*!
         \brief Vrati aktualny program kina

         \param urlMesta url adresa mesta podla akcie 15
         \param idKina id kina
         \param urlKina url kina podla akcie 16
         \param datum datum v tvare rok-mesiac-den
        */
    void kinoVysielanie(const QString &urlMesta,
                        const QString &idKina,
                        const QString &urlKina,
                        const QString &datum = QDate::currentDate().toString(Qt::ISODate));

    /*!
         \brief Vrati program kina

         \param idKina id kina podla akcie 16
        */
    void kinoProgram(const QString &idKina);

    /*!
         \brief Vrati premiery v kinach

         \param typ 0 - aktualny mesiac, 1 - minuly mesiac, 2 - tento mesiac, 3 - nasledujuci mesiac
        */
    void kinoPremiery(const QString &typ = "0");

    /*!
         \brief Vrati co a kedy sa v oblubenych kinach hra

         \param idKin id kin oddelen ciarkov. Priklad: 15,17,6,5
         \param datum tvar rok-mesiac-den
        */
    void kinoVysielanieOblubene(const QString &idKin,
                                const QString &datum = QDate::currentDate().toString(Qt::ISODate));
    /*!
         \brief Vyhladavanie v Tv programe az kym su data
                len pre API 9 +

         \param hladat hladany vyraz
         \param baseEncoded use base64 encoded string
         \param limit limit default 10
        */
    void hladatProgram(const QString &hladat, const bool &baseEncoded = true, const QString &limit = "10");

    void casovaOs(const QString &idStanic, const QString &datum = QDate::currentDate().toString(Qt::ISODate));

    // User skripty
    void registrovat(const QString &userName, const QString &userMail);

    void login(const QString &userMail, const QString &password);

    void mojFilm(const QString &idFilmu /* id filmu */);

    void hodnotit(const QString &idFilmu, const int &rating /* 0 az 10 */);

    void videlSom(const QString &idFilmu, const int &videl /* 0 = nie, 1 = ano*/);

    void chcemVidiet(const QString &idFilmu, const int &chcemVidiet /* 0 = nie, 1 = ano*/);

    void filmoteka(const QString &hladat = "", const int &od = 0, const int &limit = 10);

    void mojeHodnotenia(const QString &hladat = "", const int &od = 0, const int &limit = 10);

    void mojeChcemVidiet(const QString &hladat = "", const QString &kina = "", const int &od = 0, const int &limit = 10);

    void mojeVidelSom(const QString &hladat = "", const int &od = 0, const int &limit = 10);

    void mojeKomentare(const QString &hladat = "", const int &od = 0, const int &limit = 10);

    void videlHodnotil(const QString &hladat = "", const int &od = 0, const int &limit = 10);

    void clearLogin();


private Q_SLOTS:
    /*!
         \brief

         \param reply
        */
    void requestFinished(QNetworkReply *reply);

private:
    FDBLibPrivate *const d_ptr;
    Q_DECLARE_PRIVATE(FDBLib)
    Q_DISABLE_COPY(FDBLib)
};

#endif // FDBLIB_H
