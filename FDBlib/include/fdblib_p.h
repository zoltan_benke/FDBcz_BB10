/*
     (fdblib_p.h)
     
    Copyright (c) 2014 Zoltán Benke (Benecore) <benecore@devpda.net>
                                               http://devpda.net
                                               
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
 */

#ifndef FDBLIB_P_H
#define FDBLIB_P_H

#include "fdblib.h"
#include <QCryptographicHash>

class QNetworkAccessManager;
class QNetworkRequest;
class QUrl;
class QEventLoop;

class FDBLIBSHARED_EXPORT FDBLibPrivate
{
public:
    FDBLibPrivate(FDBLib *parent = 0);
    virtual ~FDBLibPrivate();


    void executeRequest(const QUrl &requestUrl);
    void setRequest(const FDBLib::FDBRequest request);
    QByteArray getSignatureHash(const QByteArray &user = "symbian");
    QByteArray getLoginHash(const QByteArray &mail, const QByteArray &password);
    QByteArray generateDateHeader();


private:
    FDBLib::FDBRequest _request;
    FDBLib::FDBError _error;
    int _errorCode;
    QString _errorString;
    bool _debugEnabled;
    QByteArray _timestamp;
    QList<QString> _days;
    QList<QString> _months;
    QNetworkAccessManager *_manager;
    QEventLoop *_loop;
    bool _synchronous;
    QString _apiUrl;
    QString _userMail;
    QString _userPassword;

protected:
    FDBLib *const q_ptr;
    Q_DECLARE_PUBLIC(FDBLib)
};


#endif // FDBLIB_P_H
