/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.2
import com.devpda.tools 1.2
import bb.system 1.2

TabbedPane {
    id: window
    
    
    showTabsOnActionBar: false
    
    Menu.definition: MenuDefinition {
        settingsAction: SettingsActionItem {
            id: settingsAction
            title: qsTr("Settings") + Retranslate.onLocaleOrLanguageChanged
            onTriggered: {
                activePane.push(settingsPage.createObject())
            }
            attachedObjects: [
                ComponentDefinition {
                    id: settingsPage
                    source: "pages/Settings.qml"
                }
            ]
        }
        actions: [
            ActionItem {
                id: aboutAction
              	title: qsTr("About") + Retranslate.onLocaleOrLanguageChanged
                imageSource: "asset:///images/actions/about.png"
                onTriggered: {
                    activePane.push(aboutPage.createObject())
                }
                attachedObjects: [
                    ComponentDefinition {
                        id: aboutPage
                        source: "pages/About.qml"
                    }
                ]
            },
            ActionItem {
                title: qsTr("Rate") + Retranslate.onLocaleOrLanguageChanged
                imageSource: "asset:///images/actions/rate.png"
                onTriggered: {
                    invoker.action = "bb.action.OPEN"
                    invoker.target = "sys.appworld"
                    invoker.uri = "appworld://content/31925890"
                    invoker.invoke()
                }
            },
            ActionItem {
                title: qsTr("Tell a Friend") + Retranslate.onLocaleOrLanguageChanged
                imageSource: "asset:///images/actions/invite.png"
                onTriggered: {
                    Invite.sendInvite();
                }
            }
        ]
    }
    
    attachedObjects: [
        SystemToast {
            id: toast
            button.label: ""
        },
        Invoker {
            id: invoker
            onInvocationFailed: {
                console.log("Invocation failed")
            }
        }
    ]
    
    
    tabs: [
        Tab {
            id: mojeFdb
            
            title: qsTr("My FDB") + Retranslate.onLocaleOrLanguageChanged
            description: qsTr("FDB account") + Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///images/tabs/moje_fdb.png"
            delegate: Delegate {
                id: mojeFdbDelegate
                source: "tabs/MojeFDB.qml"  
            }
            delegateActivationPolicy: TabDelegateActivationPolicy.Default
        },
        Tab {
            id: hladat
            
            title: qsTr("Search") + Retranslate.onLocaleOrLanguageChanged
            description: qsTr("Find movie or personality") + Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///images/tabs/hladat2.png"
            delegate: Delegate {
                id: hladatDelegate
                source: "tabs/Hladat.qml"  
            }
            delegateActivationPolicy: TabDelegateActivationPolicy.Default
        },
        Tab {
            id: praveBezi
            
            title: qsTr("Now in tv") + Retranslate.onLocaleOrLanguageChanged
            description: qsTr("Currently broadcast on tv") + Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///images/tabs/pravebezi2.png"
            delegate: Delegate {
                id: praveBeziDelegate
                source: "tabs/PraveBezi.qml"  
            }
            delegateActivationPolicy: TabDelegateActivationPolicy.ActivateImmediately
        
        },
        Tab {
            id: tvTipy
            
            title: qsTr("Tv tips") + Retranslate.onLocaleOrLanguageChanged
            description: qsTr("What you should not miss") + Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///images/tabs/tv_tipy2.png"
            delegate: Delegate {
                id: tvTipyDelegate
                source: "tabs/TvTipy.qml"   
            }
            delegateActivationPolicy: TabDelegateActivationPolicy.ActivateWhenSelected
        },
        Tab {
            id: mojeKina
            
            title: qsTr("Favorite cinemas") + Retranslate.onLocaleOrLanguageChanged
            description: qsTr("Your favorite cinemas") + Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///images/tabs/moje_kina2.png"
            delegate: Delegate {
                id: mojeKinaDelegate
                source: "tabs/MojeKina.qml"   
            }
            delegateActivationPolicy: TabDelegateActivationPolicy.ActivateWhenSelected
        },
        Tab {
            id: hrajuKina
            
            title: qsTr("Play in cinemas") + Retranslate.onLocaleOrLanguageChanged
            description: qsTr("Playing in your favorite cinemas") + Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///images/tabs/kina2.png"
            delegate: Delegate {
                id: hrajuKinaDelegate
                source: "tabs/HrajuKina.qml"   
            }
            delegateActivationPolicy: TabDelegateActivationPolicy.ActivateWhenSelected
        },
        Tab {
            id: kinoPremiery
            
            title: qsTr("Cinema premieres") + Retranslate.onLocaleOrLanguageChanged
            description: qsTr("Premieres, or what is currently running in theaters") + Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///images/tabs/kino_premiery2.png"
            delegate: Delegate {
                id: kinoPremieryDelegate
                source: "tabs/KinoPremiery.qml"   
            }
            delegateActivationPolicy: TabDelegateActivationPolicy.ActivateWhenSelected
        }
    ]

	onCreationCompleted: {
	    activeTab = praveBezi
        if (!BBM.allowed)
            BBM.registerApplication();
     	Qt.App = App
     	Qt.Settings = Settings
     	Qt.SizeHelper = SizeHelper
     	Qt.ThemeSettings = ThemeSettings
     	Qt.Models = Models
     	Qt.Database = Database
     	Qt.Cacher = Cacher
     	Qt.Helper = Helper
     	Qt.Calendar = Calendar
     	console.log("onCreationCompleted, whiteTheme, ".concat(App.whiteTheme))
     	console.log("onCreationCompleted, showFirstProgram, ".concat(Settings.showFirstProgram))
   }
}