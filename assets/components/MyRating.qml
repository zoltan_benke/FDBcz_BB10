import bb.cascades 1.2

Container {
    property variant rating: 0
    property string numRatings: ''
    property string off: 'asset:///images/ii_stars_sm_inactive.png'
    property string half: 'asset:///images/ii_stars_sm_active_half.png'
    property string on: 'asset:///images/ii_stars_sm_active.png'

    layout: StackLayout {
        orientation: LayoutOrientation.LeftToRight
    }
    
    ImageView {
        id: s1
        imageSource: star(1)
    }
    ImageView {
        id: s2
        imageSource: star(2)
    }
    ImageView {
        id: s3
        imageSource: star(3)
    }
    ImageView {
        id: s4
        imageSource: star(4)
    }
    ImageView {
        id: s5
        imageSource: star(5)
    }
    ImageView {
        id: s6
        imageSource: star(6)
    }
    ImageView {
        id: s7
        imageSource: star(7)
    }
    ImageView {
        id: s8
        imageSource: star(8)
    }
    ImageView {
        id: s9
        imageSource: star(9)
    }
    ImageView {
        id: s10
        imageSource: star(10)
    }
    Label {
        visible: numRatings.length > 0
        text: numRatings + ''
        leftMargin: 10
        textStyle {
            base: SystemDefaults.TextStyles.SmallText
            color: Color.create('#cccccc')
        }
    }
    function star(level){
        if(rating >= level ) return on;
        //if(rating >= level - 0.5) return half
        return off;
    }
}