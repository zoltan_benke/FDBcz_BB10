import bb.cascades 1.2



Container {
    id: root
    
    
    
    function label(){
        var text = new String;            
        switch (rating.length){
            case 4:
                text = rating.slice(0, 3);
                break;
            case 3:
                text = rating.slice(0, 2).concat(",").concat(rating.charAt(2))
                break;
            default:
                text = rating;
                break;
        }
        return text.concat("%")
    }
    
    function setImage(){
        var rate = parseInt(rating)
        if (rate <= 250){
            imagePaint.imageSource = "asset:///images/rating/0-25.png"
        }else if (rate > 250 && rate <= 500){
            imagePaint.imageSource = "asset:///images/rating/25-50.png"
        }else if (rate > 500 && rate <= 750){
            imagePaint.imageSource = "asset:///images/rating/50-75.png"
        }else if (rate > 750 && rate <= 1000){
            imagePaint.imageSource = "asset:///images/rating/75-100.png"
        }
    }
    
    function setBackGround(){
        var rate = parseInt(rating)
        if (rate <= 250){
            background = Color.create("#8d0505");
        }else if (rate > 250 && rate <= 500){
            background = Color.create("#894e00");
        }else if (rate > 500 && rate <= 750){
            background = Color.create("#0f5577");
        }else if (rate > 750 && rate <= 1000){
            background = Color.create("#37760d");
        }
    }
    
    visible: (parseInt(rating) != 0)
    layout: DockLayout{}
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    property string rating
    //background: imagePaint.imagePaint
    onRatingChanged: {
        console.log("RATING: ".concat(rating))
        if (rating){
            ratingLabel.text = label()
            //setImage()
            setBackGround() 
        }
    }
    property string size: "big" // small, normal, big
    onSizeChanged: {
        switch (size){
            case "small":
                minHeight = 50; break;
            case "normal":
                minHeight = 65; break;
            case "big":
                minHeight = 80; break;
        }
    }
    minHeight: 80
    minWidth: minHeight + 35
    maxHeight: 80
    maxWidth: maxHeight + 35
    
    attachedObjects: [
        ImagePaintDefinition {
            id: imagePaint
            repeatPattern: RepeatPattern.X
        }
    ]
    
    Label {
        id: ratingLabel
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center
        textStyle{
            color: Color.White
            base: (size == "big") || (size == "normal") ? SystemDefaults.TextStyles.smallText : SystemDefaults.TextStyles.SubtitleText
        }
    }
}