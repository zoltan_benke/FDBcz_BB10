import bb.cascades 1.2



Container {
    
    layout: StackLayout {}
    
    signal refreshTriggered();
    
    property alias lastRefresh: lastRefreshLabel.text 
    
    Label {
        id: refreshLabel
        horizontalAlignment: HorizontalAlignment.Fill
        textStyle{
            base: SystemDefaults.TextStyles.TitleText
        }
        topMargin: 25
    }
    
    Label {
        id: lastRefreshLabel
        horizontalAlignment: HorizontalAlignment.Fill
        text: App.genTime
        textStyle{
            base: SystemDefaults.TextStyles.SubtitleText
            color: Color.Gray
        }
    }
    
    
    attachedObjects: [
        LayoutUpdateHandler {
            id: refreshHandler
            
            onLayoutFrameChanged: {
                if (layoutFrame.y >= 0.0 * layoutFrame.height) {
                    refreshLabel.text = qsTr("Release to refresh...")
                    
                    if (layoutFrame.y == 0) {
                        refreshTriggered();
                    }
                } else {
                    refreshLabel.text = qsTr("Pull down to refresh...")
                }
            }
        }
    ]
}
