import bb.cascades 1.2



Page {
    id: root
    

    property bool active: false
    property alias loading: indicator.visible
    property alias loadingText: indicator.text
    property alias header: root.titleBar
    property alias noContentVisible: noContent.visible
    property alias noContentText: noContentLabel.text
    default property alias contentItems: rootContainer.controls
    property alias backgroundColor: rootContainer.background
    property bool destroyPage: true
    
    signal backClicked();
    
    paneProperties: NavigationPaneProperties {
        backButton: ActionItem {
            title: qsTr("Back") + Retranslate.onLocaleOrLanguageChanged
            onTriggered: {
                if (App.loading){
                    App.loading = false
                }
                activePane.pop()
            }
        }
    }
    
    actionBarAutoHideBehavior: SizeHelper.nType ? ActionBarAutoHideBehavior.HideOnScroll : ActionBarAutoHideBehavior.Default
    
    Container {
        id: rootContainer
        background: App.whiteTheme ? Color.create("#f8f8f8") : Color.create("#262626")
        layout: DockLayout{}
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        
        LoadingIndicator {
            id: indicator
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Bottom
        }
        
        
        Container {
            id: noContent
            visible: false
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
            
            Label {
                id: noContentLabel
                visible: (text != "") && !App.loading
                multiline: true
                autoSize.maxLineCount: 2
                horizontalAlignment: HorizontalAlignment.Center
                text: qsTr("No data") + Retranslate.onLocaleOrLanguageChanged
                textStyle{
                    base: SystemDefaults.TextStyles.BigText
                    color: Color.Gray
                }
                topMargin: 0
                textStyle.textAlign: TextAlign.Center
            }
        
        }
        
    }
}
