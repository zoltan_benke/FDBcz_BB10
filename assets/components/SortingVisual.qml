import bb.cascades 1.2

Container {
    
    property alias name: nameOption.value
    property alias year: yearOption.value
    property alias rating: ratingOption.value
    
    leftPadding: 20
    rightPadding: 20
    bottomPadding: 20
    topPadding: 20
    DropDown {
        title: qsTr("Sorting by") + Retranslate.onLocaleOrLanguageChanged
        selectedIndex: 0
        options: [
            Option {
                id: nameOption
                text: qsTr("Name") + Retranslate.onLocaleOrLanguageChanged
                value: ["title"]
            },
            Option {
                id: yearOption
                text: qsTr("Year") + Retranslate.onLocaleOrLanguageChanged
                value: ["year"]
            },
            Option {
                id: ratingOption
                text: qsTr("Rating count") + Retranslate.onLocaleOrLanguageChanged
                value: ["rating_count"]
            }
        ]
        bottomMargin: 5
        onSelectedOptionChanged: {
            model.sortingKeys = selectedValue
        }
    }
    DropDown {
        topMargin: 5
        title: qsTr("Sorting") + Retranslate.onLocaleOrLanguageChanged
        selectedIndex: 0
        options: [
            Option {
                text: qsTr("Ascending") + Retranslate.onLocaleOrLanguageChanged
                value: true
            },
            Option {
                text: qsTr("Descending") + Retranslate.onLocaleOrLanguageChanged
                value: false
            }
        ]
        onSelectedOptionChanged: {
            model.sortedAscending = selectedValue
        }
    }
}