import bb.cascades 1.2
import com.devpda.tools 1.2

import "../items"

TitleBar {
    id: root
    
    
    function reloadModel(){
        console.log("RELOADING MODEL")
        expandModel.clear()
        var data = [{"name": '06:00', "next": '08:00', "selected": false}, {"name": '08:00', "next": '10:00', "selected": false}, {"name": '10:00', "next": '12:00', "selected": false}, {"name": '12:00', "next": '14:00', "selected": false}, {"name": '14:00', "next": '16:00', "selected": false},
            {"name": '16:00', "next": '18:00', "selected": false}, {"name": '18:00', "next": '20:00', "selected": false}, {"name": "20:00", "next": '22:00', "selected": false}, {"name": '22:00', "next": '00:00', "selected": false}, {"name": '00:00', "next": '6:00', "selected": false}]
        var selectIndex = 0;
        var nowTime = Date.parse('01/01/2011 '.concat(Helper.getTime()))
        console.log("NOW TIME: ".concat(Helper.getTime()))
        for (var i = 0; i < data.length; i++){
            var current = data[i]
            var currentTime = Date.parse('01/01/2011 '.concat(current.name))
            var nextTime = Date.parse('01/01/2011 '.concat(current.next))
            //console.log("NOW: ".concat(nowTime).concat("\nCURRENT: ".concat(currentTime)).concat("\nNEXT: ".concat(nextTime)))
            if (nowTime === currentTime){
                selectIndex = i;
                current.name = qsTr("Now") + Retranslate.onLocaleOrLanguageChanged
                current.selected = true
            }
            expandModel.append(current)
        }
        if (!selectIndex){
            for (var a = 0; a < expandModel.size(); a++){
                var item = expandModel.data([a]);
                var currentTime = Date.parse('01/01/2011 '.concat(item.name))
                var nextTime = Date.parse('01/01/2011 '.concat(item.next))
                if (nowTime > currentTime && nowTime < nextTime){
                    selectIndex = ++a;
                    expandModel.insert(selectIndex,{"name": qsTr("Now") + Retranslate.onLocaleOrLanguageChanged, "selected": true})
                    break;
                }
            }
        }
        console.log("Actually: ".concat(actuallySelected))
        console.log("selectedIndex: ".concat(selectIndex))
        if (actuallySelected && actuallySelected != selectedIndex){
            actuallySelectedItem = expandList.selected()
            expandList.select(actuallySelected, true)
        }else{
            expandList.select([selectIndex], true)
        }
    }
    
    property alias titleText: titleLabel.text
    property alias subtitleText: subtitleLabel.text
    
    property alias imageVisible: image.visible
    property alias imageSource: image.url
    
    property alias leftButtonVisible: leftButton.visible
    property alias leftButtonImageSource: leftButton.defaultImageSource
    property alias rightButtonVisible: rightButton.visible
    property alias rightButtonImageSource: rightButton.defaultImageSource
    property alias rightButtonExpandVisible: rightButtonExpand.visible
    property alias rightButtonExpandImageSource: rightButtonExpand.defaultImageSource
    
    property bool expand: false
    property variant actuallySelected: null
    property variant actuallySelectedItem
    onExpandChanged: {
        freeForm.expandableArea.expanded = expand
    }
    
    onCreationCompleted: {
        //App.reloadModel.connect(reloadModel)
    }
    
    signal leftButtonClicked()
    signal rightButtonClicked()
    signal expandClicked()
    signal beziodClicked(variant item)
    
    attachedObjects: [
        ImagePaintDefinition {
            id: back
            repeatPattern: RepeatPattern.X
            imageSource: "asset:///images/262626.png"
        }
    ]
    
    
    kind: TitleBarKind.FreeForm
    kindProperties: FreeFormTitleBarKindProperties {
        id: freeForm
        
        
        content: Container {
            id: container
            
            
            layout: DockLayout{}
            background: back.imagePaint
            horizontalAlignment: HorizontalAlignment.Fill
            
            Container {
                layout: DockLayout{}
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Bottom
                preferredHeight: 10
                background: Color.create("#".concat(Settings.activeColor))
            }
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                verticalAlignment: VerticalAlignment.Center
                leftPadding: 15
                rightPadding: 15
                RemoteImage {
                    id: image
                    visible: false
                    preferredHeight: SizeHelper.nType ? 80 : 100
                    preferredWidth: SizeHelper.nType ? 80 : 100
                    horizontalAlignment: HorizontalAlignment.Left
                    verticalAlignment: VerticalAlignment.Center
                    scalingMethod: ScalingMethod.AspectFit
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    cache: true
                }
                ImageButton {
                    id: leftButton
                    visible: defaultImageSource != ""
                    verticalAlignment: VerticalAlignment.Center
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    onClicked: {
                        root.leftButtonClicked()
                    }
                }
                Container {
                    verticalAlignment: VerticalAlignment.Center
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    
                    Label {
                        id: titleLabel
                        textStyle.color: Color.White
                        textStyle.base: SystemDefaults.TextStyles.PrimaryText
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: -1
                        }
                        bottomMargin: 0
                    }
                    
                    Label {
                        id: subtitleLabel
                        visible: text
                        textStyle.color: Color.Gray
                        textStyle.base: SystemDefaults.TextStyles.SubtitleText
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: -1
                        }
                        topMargin: 0
                    }
                } // End of Title/Subtitle container
                ImageButton {
                    id: rightButton
                    visible: defaultImageSource != ""
                    verticalAlignment: VerticalAlignment.Center
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    onClicked: {
                        root.rightButtonClicked()
                    }
                }
                ImageButton {
                    id: rightButtonExpand
                    visible: defaultImageSource != ""
                    verticalAlignment: VerticalAlignment.Center
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    rotationZ: root.expand ? 360 : 180
                    onClicked: {
                        root.expandClicked()
                        if (expand){
                            reloadModel()
                            expandList.scrollToItem(expandList.selected(),ScrollAnimation.None)
                        }
                    }
                }
            }
        }
        expandableArea.indicatorVisibility: TitleBarExpandableAreaIndicatorVisibility.Hidden
        expandableArea.toggleArea: TitleBarExpandableAreaToggleArea.Default
        expandableArea.content: Container{
            background: App.whiteTheme ? Color.create("#f8f8f8") : Color.create("#262626")
            ListView {
                id: expandList
                preferredHeight: 100
                layout: StackListLayout {
                    orientation: LayoutOrientation.LeftToRight
                    headerMode: ListHeaderMode.None
                }
                dataModel: ArrayDataModel {
                    id: expandModel
                }
                listItemComponents: [
                    ListItemComponent {
                        ListItemBase {
                            highlight: true
                            preferredHeight: 100
                            preferredWidth: 150
                            Label {
                                horizontalAlignment: HorizontalAlignment.Center
                                verticalAlignment: VerticalAlignment.Center
                                text: ListItemData.name
                            }
                        }
                    }
                ]
                scrollIndicatorMode: ScrollIndicatorMode.None
                onTriggered: {
                    clearSelection()
                    select(indexPath)
                    actuallySelected = indexPath
                    var item = dataModel.data(indexPath)
                    if (item){
                        root.beziodClicked(item)
                    }
                }
            }
        }
    }
    scrollBehavior: TitleBarScrollBehavior.Sticky
}