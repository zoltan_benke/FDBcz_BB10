import bb.cascades 1.2


Container {
	id: root
	
    property int type
    property alias height: root.minHeight 
    onTypeChanged: {
        switch (type){
            case 1:
                root.background = Color.create("#ffee6b");
                break;
            case 2:
                root.background = Color.create("#5fb567");
                break;
            case 3:
                root.background = Color.create("#b4a7d1");
                break;
            case 4: 
                root.background = Color.create("#eabb79");
                break;
            case 5:
                root.background = Color.create("#97ce57");
                break;
            case 7:
                root.background = Color.create("#88cbea");
                break;
            case 8:
                root.background = Color.create("#efceeb");
                break;
            case 9:
                root.background = Color.create("#fffbc6");
                break;
        }
    }
    
    layout: DockLayout{}
    horizontalAlignment: HorizontalAlignment.Fill
    
    /*
    attachedObjects: [
        ImagePaintDefinition {
            id: back
            repeatPattern: RepeatPattern.X
            imageSource: "asset:///Images/program-type/".concat(type).concat(".png")
        }
    ]
    */
    Label {
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center
        textStyle.color: Color.Black
        text: {
            switch(root.type){
                case 1:
                    return qsTr("Fun") + Retranslate.onLocaleOrLanguageChanged
                case 2:
                    return qsTr("Sport") + Retranslate.onLocaleOrLanguageChanged
                case 3:
                    return qsTr("Movie") + Retranslate.onLocaleOrLanguageChanged
                case 4:
                    return qsTr("Serial") + Retranslate.onLocaleOrLanguageChanged
                case 5:
                    return qsTr("Document") + Retranslate.onLocaleOrLanguageChanged
                case 7:
                    return qsTr("Music") + Retranslate.onLocaleOrLanguageChanged
                case 8:
                    return qsTr("Children") + Retranslate.onLocaleOrLanguageChanged
                case 9:
                    return qsTr("News") + Retranslate.onLocaleOrLanguageChanged
                default:
                    return ""
            }
        }
        textStyle.fontSize: FontSize.XXSmall
    }

}
