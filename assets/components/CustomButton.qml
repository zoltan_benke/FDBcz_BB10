import bb.cascades 1.2


Container {
    id: root
    
    property bool pressed: false
    onPressedChanged: {
        var newImage
        if (root.pressed){
            newImage = App.whiteTheme ? image.imageSource.toString().replace("black","blue") : image.imageSource.toString().replace("white","blue")
        }else{
            newImage = App.whiteTheme ? image.imageSource.toString().replace("blue","black") : image.imageSource.toString().replace("blue","white")
        }
        image.imageSource = newImage
    }
    property alias imageSource: image.imageSource
    property alias title: label.text
    property alias btnEnabled: image.enabled
    
    signal clicked(bool pressed);
    
    Label {
        id: label
        horizontalAlignment: HorizontalAlignment.Center
        bottomMargin: 0
        textStyle{
            fontSize: FontSize.Small
        }
    }
    
    Button {
        topMargin: 0
        id: image
        onClicked: {
            root.clicked(root.pressed)
        }
    }
}

/*
Container {
    id: root

    property bool pressed: false
    property alias imageSource: image.imageSource
    property alias title: label.text
    
    preferredWidth: SizeHelper.maxWidth/3-15
    background: paint.imagePaint
    preferredHeight: 80
    attachedObjects: [
        ImagePaintDefinition {
            id: paint
            repeatPattern: RepeatPattern.X
            imageSource: {
                if (root.pressed){
                    return "asset:///images/button/core_titlebar_button_pressed.amd"
                }else{
                    return "asset:///images/button/core_titlebar_button_inactive.amd"
                }
            }
        }
    ]
    
    gestureHandlers: [
        TapHandler {
            onTapped: {
                var newImage
                root.pressed = !root.pressed
                if (root.pressed){
                    newImage = image.imageSource.toString().replace("white","blue")
                }else{
                    newImage = image.imageSource.toString().replace("blue","white")
                }
                image.imageSource = newImage
            }
        }
    ]
    
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        verticalAlignment: VerticalAlignment.Center
        horizontalAlignment: HorizontalAlignment.Center
        ImageView {
            id: image
            verticalAlignment: VerticalAlignment.Center
            preferredHeight: 20
            scalingMethod: ScalingMethod.AspectFit
        }
        
        Label {
            id: label
            verticalAlignment: VerticalAlignment.Center
            textStyle{
                fontSize: FontSize.XSmall
            }
        }
    }
}*/