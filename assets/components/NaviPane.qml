import bb.cascades 1.2


NavigationPane {

    onPopTransitionEnded: {
        page.active = false
        if (page.destroyPage) page.destroy()
    }
    
    
    onPushTransitionEnded: {
        page.active = true
    }
}