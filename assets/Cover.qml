import bb.cascades 1.2

Container {
    id: root
    layout: DockLayout{}
    
    background: SizeHelper.nType ? Color.create("#262626") : Color.create("#f8f8f8")
    
    
    ImageView {
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center
        scalingMethod: ScalingMethod.AspectFit
        imageSource: "asset:///images/app_icon_new.png"
        preferredWidth: SizeHelper.nType ? 200 : 250 
    }

}