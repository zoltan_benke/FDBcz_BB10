import bb.cascades 1.2
import com.devpda.tools 1.2

import "../components"
import "../actions"

ListItemBase {
    id: root
    
    
    visible: !Qt.Settings.showActive ? true : ListItemData.title_1 || ListItemData.title_2 ? true : false
    highlight: false
    touchPropagationMode: TouchPropagationMode.PassThrough
    property bool firstSelected: false
    property bool secondSelected: false
    
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        leftPadding: highlightFrameSize
        rightPadding: highlightFrameSize
        topPadding: highlightFrameSize
        bottomPadding: highlightFrameSize
        
        Container {
            verticalAlignment: VerticalAlignment.Center
            layoutProperties: StackLayoutProperties {
                spaceQuota: -1
            }
            RemoteImage {
                preferredHeight: Qt.SizeHelper.nType ? 100 : 130
                preferredWidth: Qt.SizeHelper.nType ? 100 : 130
                url: "http://img.fdb.cz/tv56/".concat(ListItemData.tv_logo).replace(".png", "@2x.png")
                scalingMethod: ScalingMethod.AspectFit
                cache: true
                bottomMargin: 0
            }
            
            Label {
                horizontalAlignment: HorizontalAlignment.Fill
                //visible: Qt.Settings.showStationName
                visible: false
                topMargin: 0
                text: ListItemData.name
                textStyle{
                    base: SystemDefaults.TextStyles.SmallText
                    color: Color.Gray
                }
                textFit.mode: LabelTextFitMode.FitToBounds
                textStyle.textAlign: TextAlign.Center
            }
        }
        
        
        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Center
            layoutProperties: StackLayoutProperties {
                spaceQuota: 1
            }
            Container {
                id: item1
                layout: DockLayout{}
                bottomMargin: 10
                minHeight: 50
                onTouch: {
                    if (event.isDown()){
                        console.log("Clicked")
                        firstSelected = true
                    }else if (event.isUp()){
                        firstSelected = false
                        root.ListItem.view.beziOdClicked(root.ListItem.indexPath, true)
                        console.log("Released")
                    }else if (event.isCancel()){
                        firstSelected = false
                    }
                }
                
                Container {
                    leftPadding: highlightFrameSize
                    rightPadding: highlightFrameSize
                    verticalAlignment: VerticalAlignment.Center
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    
                    MyLabel {
                        role: "subtitle"
                        verticalAlignment: VerticalAlignment.Center
                        opacity: 0.8
                        text: ListItemData.starttime_1
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: -1
                        }
                    }
                    
                    MyLabel {
                        role: "title"
                        verticalAlignment: VerticalAlignment.Center
                        text: (ListItemData.title_1 == "Nevysílá se") ? qsTr("Not broadcast") + Retranslate.onLocaleOrLanguageChanged : ListItemData.title_1
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        multiline: true
                        autoSize.maxLineCount: 2
                    }
                    
                    ImageView {
                        visible: ListItemData.film_1
                        opacity: 0.8
                        verticalAlignment: VerticalAlignment.Center
                        scaleX: Qt.SizeHelper.nType ? 0.4 : 0.6
                        scaleY: scaleX
                        imageSource: Qt.App.whiteTheme ? "asset:///images/next_classic_black.png" : "asset:///images/next_classic_white.png"
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: -1
                        }
                    }
                }
                
                Container {
                    layout: DockLayout {}
                    visible: firstSelected && root.ListItem.active
                    background: Color.create(highlightFrameColor)
                    maxWidth: highlightFrameSize
                    minWidth: highlightFrameSize
                    verticalAlignment: VerticalAlignment.Fill
                    horizontalAlignment: HorizontalAlignment.Left
                }
                
                Container {
                    layout: DockLayout {}
                    visible: firstSelected && root.ListItem.active
                    background: Color.create(highlightFrameColor)
                    maxWidth: highlightFrameSize
                    minWidth: highlightFrameSize
                    verticalAlignment: VerticalAlignment.Fill
                    horizontalAlignment: HorizontalAlignment.Right
                }
                
                
                Container {
                    layout: DockLayout {}
                    visible: firstSelected && root.ListItem.active
                    background: Color.create(highlightFrameColor)
                    maxHeight: highlightFrameSize
                    minHeight: highlightFrameSize
                    verticalAlignment: VerticalAlignment.Top
                    horizontalAlignment: HorizontalAlignment.Fill
                }
                
                Container {
                    layout: DockLayout {}
                    visible: firstSelected && root.ListItem.active
                    background: Color.create(highlightFrameColor)
                    maxHeight: highlightFrameSize
                    minHeight: highlightFrameSize
                    verticalAlignment: VerticalAlignment.Bottom
                    horizontalAlignment: HorizontalAlignment.Fill
                }
            } // End of Item 1
            
            Divider {
                horizontalAlignment: HorizontalAlignment.Fill
                topMargin: 10
                bottomMargin: 10
            }
            
            
            Container {
                id: item2
                layout: DockLayout{}
                topMargin: 10
                minHeight: 50
                onTouch: {
                    if (event.isDown()){
                        console.log("Clicked")
                        console.log("ITEM: ".concat(ListItemData.film_2))
                        secondSelected = true
                    }else if (event.isUp()){
                        secondSelected = false
                        root.ListItem.view.beziOdClicked(root.ListItem.indexPath, false)
                        console.log("Released")
                    }else if (event.isCancel()){
                        secondSelected = false
                        console.log("Cancel")
                    }
                }
                Container {
                    leftPadding: highlightFrameSize
                    rightPadding: highlightFrameSize
                    verticalAlignment: VerticalAlignment.Center               
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    
                    MyLabel {
                        role: "subtitle"
                        opacity: 0.8
                        verticalAlignment: VerticalAlignment.Center
                        text: ListItemData.starttime_2
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: -1
                        }
                    }
                    MyLabel {
                        verticalAlignment: VerticalAlignment.Center
                        role: "title"
                        text: (ListItemData.title_2 == "Nevysílá se") ? qsTr("Not broadcast") + Retranslate.onLocaleOrLanguageChanged : ListItemData.title_2
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        multiline: true
                        autoSize.maxLineCount: 2
                    }
                    
                    ImageView {
                        visible: ListItemData.film_2
                        opacity: 0.8
                        verticalAlignment: VerticalAlignment.Center
                        scaleX: Qt.SizeHelper.nType ? 0.4 : 0.6
                        scaleY: scaleX
                        imageSource: Qt.App.whiteTheme ? "asset:///images/next_classic_black.png" : "asset:///images/next_classic_white.png"
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: -1
                        }
                    }
                }
                
                Container {
                    layout: DockLayout {}
                    visible: secondSelected && root.ListItem.active
                    background: Color.create(highlightFrameColor)
                    maxWidth: highlightFrameSize
                    minWidth: highlightFrameSize
                    verticalAlignment: VerticalAlignment.Fill
                    horizontalAlignment: HorizontalAlignment.Left
                }
                
                Container {
                    layout: DockLayout {}
                    visible: secondSelected && root.ListItem.active
                    background: Color.create(highlightFrameColor)
                    maxWidth: highlightFrameSize
                    minWidth: highlightFrameSize
                    verticalAlignment: VerticalAlignment.Fill
                    horizontalAlignment: HorizontalAlignment.Right
                }
                
                
                Container {
                    layout: DockLayout {}
                    visible: secondSelected && root.ListItem.active
                    background: Color.create(highlightFrameColor)
                    maxHeight: highlightFrameSize
                    minHeight: highlightFrameSize
                    verticalAlignment: VerticalAlignment.Top
                    horizontalAlignment: HorizontalAlignment.Fill
                }
                
                Container {
                    layout: DockLayout {}
                    visible: secondSelected && root.ListItem.active
                    background: Color.create(highlightFrameColor)
                    maxHeight: highlightFrameSize
                    minHeight: highlightFrameSize
                    verticalAlignment: VerticalAlignment.Bottom
                    horizontalAlignment: HorizontalAlignment.Fill
                }
            } // End of Item 1
        }
    
    
    
    } // End of LeftToRight container
}