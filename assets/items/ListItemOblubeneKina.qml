import bb.cascades 1.2
import com.devpda.tools 1.2
import bb.system 1.2

import "../components"
import "../actions"

ListItemBase {
    id: root
    
    property bool checkbox: true
    
    contextActions: [
        ActionSet {
            title: ListItemData.name
            ShowMapAction {
                onClicked: {
                    root.ListItem.view.showMap(root.ListItem.indexPath)
                }
            }
            FavoriteAction {
                add: false
                onClicked: {
                    root.ListItem.view.removeFavorite(root.ListItem.indexPath)
                }
            }
            DeleteActionItem {
                title: qsTr("Remove all") + Retranslate.onLocaleOrLanguageChanged
                onTriggered: {
                    clearCinemas.show()
                }
                attachedObjects: [
                    SystemDialog {
                        id: clearCinemas
                        title: qsTr("Remove all cinemas?") + Retranslate.onLocaleOrLanguageChanged
                        body: qsTr("Are you sure you want to remove all favorite cinemas?") + Retranslate.onLocaleOrLanguageChanged
                        confirmButton{
                            label: qsTr("Yes") + Retranslate.onLocaleOrLanguageChanged
                        }
                        onFinished: {
                            if (value == SystemUiResult.ConfirmButtonSelection){
                                root.ListItem.view.removeAll()
                            }
                        }
                    }
                ]
            }
        }
    ]
    
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        leftPadding: highlightFrameSize
        rightPadding: highlightFrameSize
        topPadding: highlightFrameSize
        bottomPadding: highlightFrameSize
        
        ImageView {
            verticalAlignment: VerticalAlignment.Center
            preferredHeight: Qt.SizeHelper.nType ? 100 : 120
            preferredWidth: Qt.SizeHelper.nType ? 100 : 120
            scalingMethod: ScalingMethod.AspectFit
            imageSource: Qt.App.whiteTheme ? "asset:///images/cinema_black.png" : "asset:///images/cinema_white.png"
            layoutProperties: StackLayoutProperties {
                spaceQuota: -1
            }
        }
        
        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Center
            layoutProperties: StackLayoutProperties {
                spaceQuota: 1
            }
            
            MyLabel {
                role: "title"
                text: ListItemData.name
                multiline: true
                autoSize.maxLineCount: 2
                bottomMargin: 0
            }
            
            MyLabel {
                topMargin: 0
                visible: ListItemData.adress
                role: "subtitle"
                color: Color.Gray
                text: ListItemData.adress
            }
        }
        
        CheckBox {
            visible: checkbox
            verticalAlignment: VerticalAlignment.Center
            touchPropagationMode: TouchPropagationMode.None
            checked: Qt.Database.isFavoriteCinema(ListItemData.id)
        }
    }
}