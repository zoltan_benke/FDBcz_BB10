import bb.cascades 1.2

import "../components"
import "../actions"

ListItemBase {
    id: root
    
    minHeight: Qt.SizeHelper.nType ? 100 : 150
    maxHeight: Qt.SizeHelper.nType ? 100 : 150
    showNext: true
    
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        verticalAlignment: VerticalAlignment.Center
        
        ImageView {
            verticalAlignment: VerticalAlignment.Center
            preferredHeight: 120
            scalingMethod: ScalingMethod.AspectFit
            imageSource: ListItemData.icon
        }
        
        MyLabel {
            role: "title"
            verticalAlignment: VerticalAlignment.Center
            text: ListItemData.name
        }
    }
    
}