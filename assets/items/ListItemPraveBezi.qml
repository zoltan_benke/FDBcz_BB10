import bb.cascades 1.2
import com.devpda.tools 1.2

import "../components"
import "../actions"

ListItemBase {
    id: root
    
    visible: !Qt.Settings.showActive ? true : ListItemData.title ? true : false
    contextActions: [
        ActionSet {
            title: ListItemData.name
            subtitle: ListItemData.title
            
            actions: [
                FavoriteAction {
                    add: !ListItemData.favorite
                    onClicked: {
                        if (ListItemData.favorite){
                            root.ListItem.view.setFavorite(root.ListItem.indexPath, false)                
                        }else{
                            root.ListItem.view.setFavorite(root.ListItem.indexPath, true)
                        }
                    }
                }
            ]
        }
    ]
    
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        leftPadding: highlightFrameSize
        rightPadding: highlightFrameSize
        topPadding: highlightFrameSize
        bottomPadding: highlightFrameSize
        
        
        Container {
            verticalAlignment: VerticalAlignment.Center
            layoutProperties: StackLayoutProperties {
                spaceQuota: -1
            }
            RemoteImage {
                preferredHeight: Qt.SizeHelper.nType ? 100 : 130
                preferredWidth: Qt.SizeHelper.nType ? 100 : 130
                url: "http://img.fdb.cz/tv56/".concat(ListItemData.logo).replace(".png", "@2x.png")
                scalingMethod: ScalingMethod.AspectFit
                cache: true
                bottomMargin: 0
            }
            
            Label {
                horizontalAlignment: HorizontalAlignment.Fill
                //visible: Qt.Settings.showStationName
                visible: false
                topMargin: 0
                text: ListItemData.name
                textStyle{
                    base: SystemDefaults.TextStyles.SmallText
                    color: Color.Gray
                }
                textFit.mode: LabelTextFitMode.FitToBounds
                textStyle.textAlign: TextAlign.Center
            }
        }
        
        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Center
            layoutProperties: StackLayoutProperties {
                spaceQuota: 1
            }
            
            Container {
                objectName: "titleContainer"
                horizontalAlignment: HorizontalAlignment.Fill
                MyLabel {
                    role: "title"
                    text: ListItemData.title ? ListItemData.title : qsTr("Not broadcast") + Retranslate.onLocaleOrLanguageChanged
                    multiline: true
                    autoSize.maxLineCount: 1
                    bottomMargin: 0
                    topMargin: 0
                }
            }
            
            
            Container {
                objectName: "progressContainer"
                visible: ListItemData.title
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                horizontalAlignment: HorizontalAlignment.Fill
                MyLabel {
                    role: "subtitle"
                    horizontalAlignment: HorizontalAlignment.Left
                    text: ListItemData.time.split(" - ")[0]
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                }
                
                ProgressIndicator {
                    verticalAlignment: VerticalAlignment.Center
                    value: parseInt(ListItemData.state)
                    toValue: 100.0
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
                
                
                MyLabel {
                    role: "subtitle"
                    horizontalAlignment: HorizontalAlignment.Right
                    text: ListItemData.time.split(" - ")[1]
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                }
            } // End of Progress indicator
            
            Container {
                objectName: "programOneContainer"
                visible: ListItemData.cas1 && ListItemData.title1 && Qt.Settings.showFirstProgram
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                horizontalAlignment: HorizontalAlignment.Fill
                topMargin: 0
                bottomMargin: 0
                MyLabel {
                    role: "subtitle"
                    textStyle.color: Color.Gray
                    text: ListItemData.cas1
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                }
                
                MyLabel {
                    role: "subtitle"
                    textStyle.color: Color.Gray
                    text: ListItemData.title1
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
            } // End of First following program
            
            Container {
                objectName: "programSecondContainer"
                visible: ListItemData.cas2 && ListItemData.title2 && Qt.Settings.showSecondProgram
                //visible: false
                opacity: 0.8
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                horizontalAlignment: HorizontalAlignment.Fill
                topMargin: 0
                bottomMargin: 0
                MyLabel {
                    role: "subtitle"
                    textStyle.color: Color.Gray
                    text: ListItemData.cas2
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                }
                
                MyLabel {
                    role: "subtitle"
                    textStyle.color: Color.Gray
                    text: ListItemData.title2
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
            } // End of Second following program
        } // End of StackLayout TopBottom container
    } // End of LeftToRight container


}
