import bb.cascades 1.2
import com.devpda.tools 1.2

import "../components"
import "../actions"

ListItemBase {
    id: root
    
    preferredHeight: Qt.SizeHelper.nType ? 100 : 140
    
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        leftPadding: highlightFrameSize
        rightPadding: highlightFrameSize
        topPadding: highlightFrameSize
        bottomPadding: highlightFrameSize
        
        Container {
            layoutProperties: StackLayoutProperties {
                spaceQuota: 1
            }
            leftPadding: 15
            verticalAlignment: VerticalAlignment.Center
            MyLabel {
                role: "title"
                text: ListItemData.name
            }
        }
        
        ImageView {
            opacity: 0.8
            verticalAlignment: VerticalAlignment.Center
            scaleX: 0.6
            scaleY: scaleX
            imageSource: Qt.App.whiteTheme ? "asset:///images/next_classic_black.png" : "asset:///images/next_classic_white.png"
            layoutProperties: StackLayoutProperties {
                spaceQuota: -1
            }
        }
    }
}