import bb.cascades 1.2
import com.devpda.tools 1.2

import "../components"
import "../actions"

ListItemBase {
    id: root
    
    showNext: ListItemData.film_id
    
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        leftPadding: highlightFrameSize
        rightPadding: highlightFrameSize
        topPadding: highlightFrameSize
        bottomPadding: highlightFrameSize
        
        RemoteImage {
            verticalAlignment: VerticalAlignment.Center
            defaultImage: "asset:///images/movie-placeholder.png"
            preferredWidth: Qt.SizeHelper.nType ? 120 : 160
            preferredHeight: Qt.SizeHelper.nType ? 170 : 230
            scalingMethod: ScalingMethod.AspectFill
            url: Qt.Settings.imageUrl.concat(ListItemData.image[0]).concat("/").concat(ListItemData.image)
            layoutProperties: StackLayoutProperties {
                spaceQuota: -1
            }
            cache: true
        }
        
        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            layoutProperties: StackLayoutProperties {
                spaceQuota: 1
            }
            
            MyLabel {
                role: "title"
                text: ListItemData.title
                multiline: true
                bottomMargin: 0
                topMargin: 0
            }
            
            MyLabel {
                role: "subtitle"
                color: Color.Gray
                topMargin: 0
                bottomMargin: 0
                text: ListItemData.orig_title
            }
            
            MyLabel {
                role: "subtitle"
                color: Color.Gray
                topMargin: 0
                bottomMargin: 0
                text: ListItemData.year.concat(" | ").concat(ListItemData.length).concat(" min")
            }
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                bottomMargin: 0
                ImageView {
                    visible: (ListItemData.videt == 1)
                    preferredHeight: Qt.SizeHelper.nType ? 50 : 60
                    scalingMethod: ScalingMethod.AspectFit
                    imageSource: Qt.App.whiteTheme ? "asset:///images/chcem_vidiet_black.png" : "asset:///images/chcem_vidiet_white.png"
                }
                ImageView {
                    visible: (ListItemData.videl == 1)
                    preferredHeight: Qt.SizeHelper.nType ? 50 : 60
                    scalingMethod: ScalingMethod.AspectFit
                    imageSource: Qt.App.whiteTheme ? "asset:///images/videl_som_black.png" : "asset:///images/videl_som_white.png"
                }
           }
           Container {
               topMargin: 0
               layout: StackLayout {
                   orientation: LayoutOrientation.LeftToRight
               }
               visible: ListItemData.rating
               MyLabel {
                   role: "subtitle"
                   text: ListItemData.rating+"/10"
                   rightMargin: 0
               }
               
               MyRating {
                   leftMargin: 0
                   verticalAlignment: VerticalAlignment.Center
                   rating: ListItemData.rating
               }
           } // end of rating container
        
        } // end of description container
    } // end of root LeftToRight container

}