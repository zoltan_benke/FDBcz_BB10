import bb.cascades 1.2
import com.devpda.tools 1.2

import "../components"
import "../actions"

ListItemBase {
    id: root
    
    showNext: ListItemData.film
    
    contextActions: [
        ActionSet {
            title: ListItemData.title
            subtitle: ListItemData.name
            
            actions: [
                CalendarAction {
                    onClicked: {
                        root.ListItem.view.addNotify(root.ListItem.indexPath)
                    }
                }
            ]
        }
    ]
    
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        leftPadding: highlightFrameSize
        rightPadding: highlightFrameSize
        topPadding: highlightFrameSize
        bottomPadding: highlightFrameSize
        
        RemoteImage {
            verticalAlignment: VerticalAlignment.Center
            defaultImage: "asset:///images/movie-placeholder.png"
            preferredWidth: Qt.SizeHelper.nType ? 120 : 160
            preferredHeight: Qt.SizeHelper.nType ? 170 : 230
            scalingMethod: ScalingMethod.AspectFill
            url: Qt.Settings.imageUrl.concat(ListItemData.image[0]).concat("/").concat(ListItemData.image)
            layoutProperties: StackLayoutProperties {
                spaceQuota: -1
            }
            cache: true
        }
        
        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            layoutProperties: StackLayoutProperties {
                spaceQuota: 1
            }
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                MyLabel {
                    role: "title"
                    text: ListItemData.title
                    multiline: false
                    bottomMargin: 0
                    topMargin: 0
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
                
                FilmRating {
                    size: "normal"
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    rating: ListItemData.rating
                }
            } // End of Title/Rating Container
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    rightMargin: 5
                    MyLabel {
                        text: ListItemData.starttime
                        role: "highlight"
                        textStyle.fontWeight: FontWeight.Bold
                        color: Color.create("#".concat(Qt.Settings.activeColor))
                        topMargin: 0
                        bottomMargin: 0
                    }
                    
                    ProgramType {
                        visible: ListItemData.type != 0 && ListItemData.type != 6
                        height: Qt.SizeHelper.nType ? 15 : 25
                        type: ListItemData.type
                    }
                
                }
                
                MyLabel {
                    text: ListItemData.caption ? ListItemData.caption : qsTr("No description available") + Retranslate.onLocaleOrLanguageChanged
                    multiline: true
                    autoSize {
                        maxLineCount: 2
                    }
                    textStyle{
                        fontSize: FontSize.PointValue
                        fontSizeValue: Qt.Settings.fontSizeSubtitle+1
                        fontFamily: Qt.Settings.fontFamilySubtitle
                        color: Color.Gray
                    }
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    topMargin: 0
                    leftMargin: 2
                }
            } // end of description container
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                RemoteImage {
                    preferredHeight: Qt.SizeHelper.nType ? 70 : 90
                    preferredWidth: Qt.SizeHelper.nType ? 70 : 90
                    url: "http://img.fdb.cz/tv56/".concat(ListItemData.tv_logo).replace(".png", "@2x.png")
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    cache: true
                }
                
                MyLabel {
                    role: "highlight"
                    color: Color.create("#".concat(Qt.Settings.activeColor))
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    verticalAlignment: VerticalAlignment.Center
                    text: ListItemData.startdate
                    textStyle{
                        fontWeight: FontWeight.Bold
                    }
                }
            }
            
        } // end of info container
    } // End of main container
}
