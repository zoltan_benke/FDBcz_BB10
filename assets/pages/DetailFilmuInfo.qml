import bb.cascades 1.2
import com.devpda.items 1.2
import com.devpda.tools 1.2
import bb.system 1.2

import "../components"
import "../items"

Container {
    id: root
    
    property MovieItem movie: null
    property variant mojFilm: null
    onMojFilmChanged: {
        console.log("MOJ FILM: "+mojFilm)
        if (mojFilm.hodnoceni){
            userBtnHodnotit.title = mojFilm.hodnoceni+"/10"
            userBtnHodnotit.pressed = true
            rateDialog.value = mojFilm.hodnoceni
        }else{
            rateDialog.value = 0
        }
        if (mojFilm.videl == 1){
            userBtnVidelSom.pressed = true
        }
        if (mojFilm.videt == 1){
            userBtnChcemVidiet.pressed = true
        }
    }
    onMovieChanged: {
        tvModel.insertList(movie.tv)
        /*
        for (var item in movie.tv){
            tvModel.append(movie.tv[item])
        }
        */
    }
    
    function chcemVidiet(response){
        var json = JSON.parse(response)
        userBtnChcemVidiet.pressed = (json.state == 1)
        userContainer.enabled = true
    }
    
    function videlSom(response){
        var json = JSON.parse(response)
        userBtnVidelSom.pressed = (json.state == 1)
        userContainer.enabled = true
    }
    
    function hodnotenie(response){
    	var json = JSON.parse(response)
        userBtnHodnotit.title = (json.star == 0) ? qsTr("Rating") + Retranslate.onLocaleOrLanguageChanged : json.star+"/10"
        userBtnHodnotit.pressed = (json.star != 0)
        userContainer.enabled = true
    }
    
    onCreationCompleted: {
        App.chcemVidietDone.connect(chcemVidiet)
        App.videlSomDone.connect(videlSom)
        App.hodnotenieDone.connect(hodnotenie)
    }
    
    attachedObjects: [
        SystemToast {
            id: toast
            button.label: ""
            autoUpdateEnabled: true
        }
    ]
    
    ScrollView {
        id: scrollView
        scrollRole: ScrollRole.Main
        onOpacityChanged: {
            if (opacity == 1){
                showFade.play()
            }else{
                hideFade.play()
            }
        }
        attachedObjects: [
            FadeTransition {
                id: showFade
                target: scrollView
                toOpacity: 1.0
                fromOpacity: 0.0
                duration: 300
            },
            FadeTransition {
                id: hideFade
                target: scrollView
                toOpacity: 0.0
                fromOpacity: 1.0
                duration: 300
            }
        ]
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        scrollViewProperties.scrollMode: ScrollMode.Vertical
        scrollViewProperties.pinchToZoomEnabled: false 
        
        content: Container{
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10
                rightPadding: 10
                topPadding: 10
                Label {
                    
                    verticalAlignment: VerticalAlignment.Center
                    text: movie ? movie.title : ""
                    textStyle{
                        base: SizeHelper.nType ? SystemDefaults.TextStyles.TitleText :SystemDefaults.TextStyles.BigText
                    }
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
                
                FilmRating {
                    size: SizeHelper.nType ? "medium" : "big"
                    verticalAlignment: VerticalAlignment.Center
                    rating: movie ? movie.rating : 0
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                }
            } // End of Title and Rating Container
            
            Header {
                title: qsTr("Basic information") + Retranslate.onLocaleOrLanguageChanged
            }
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                horizontalAlignment: HorizontalAlignment.Fill
                leftPadding: 10
                rightPadding: 10
                topPadding: 10
                Container {
                    //minHeight: 350
                    //maxHeight: 350
                    //minWidth: 250
                    //maxWidth: 250
                    RemoteImage {
                        defaultImage: "asset:///images/movie-placeholder.png"
                        scalingMethod: ScalingMethod.AspectFit
                        preferredHeight: SizeHelper.nType ? 270 : 350
                        preferredWidth: SizeHelper.nType ? 170 : 250
                        url: Qt.Settings.imageUrl.concat(movie.image[0]).concat("/").concat(movie.image)
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: -1
                        }
                    }
                }
                
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    verticalAlignment: VerticalAlignment.Fill
                    leftPadding: 10
                    Label {
                        visible: movie.orig_title
                        text: qsTr("Original name:")+ Retranslate.onLocaleOrLanguageChanged
                        topMargin: 0
                        bottomMargin: 0
                        textStyle.fontWeight: FontWeight.Bold
                        textStyle.base: SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : SystemDefaults.TextStyles.BodyText
                    } 
                    Label {
                        visible: movie.orig_title
                        text: movie.orig_title
                        multiline: true
                        autoSize{
                            maxLineCount: 2
                        }
                        topMargin: 0
                        bottomMargin: 0
                        textStyle.fontWeight: FontWeight.Normal
                        textStyle.base: SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : SystemDefaults.TextStyles.BodyText
                    }
                    Label {
                        visible: movie.year
                        text: qsTr("Year:")+ Retranslate.onLocaleOrLanguageChanged
                        topMargin: 0
                        bottomMargin: 0
                        textStyle.fontWeight: FontWeight.Bold
                        textStyle.base: SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : SystemDefaults.TextStyles.BodyText
                    } 
                    Label {
                        visible: movie.year
                        text: movie.year
                        topMargin: 0
                        bottomMargin: 0
                        textStyle.fontWeight: FontWeight.Normal
                        textStyle.base: SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : SystemDefaults.TextStyles.BodyText
                    }
                    Label {
                        visible: movie.zeme
                        text: qsTr("State:")+ Retranslate.onLocaleOrLanguageChanged
                        topMargin: 0
                        bottomMargin: 0
                        textStyle.fontWeight: FontWeight.Bold
                        textStyle.base: SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : SystemDefaults.TextStyles.BodyText
                    } 
                    Label {
                        visible: movie.zeme
                        text: movie.zeme
                        topMargin: 0
                        bottomMargin: 0
                        textStyle.fontWeight: FontWeight.Normal
                        textStyle.base: SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : SystemDefaults.TextStyles.BodyText
                    }
                    Label {
                        visible: movie.zanry
                        text: qsTr("Genre:")+ Retranslate.onLocaleOrLanguageChanged
                        topMargin: 0
                        bottomMargin: 0
                        textStyle.fontWeight: FontWeight.Bold
                        textStyle.base: SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : SystemDefaults.TextStyles.BodyText
                    } 
                    Label {
                        visible: movie.zanry
                        text: movie.zanry
                        topMargin: 0
                        bottomMargin: 0
                        textStyle.fontWeight: FontWeight.Normal
                        textStyle.base: SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : SystemDefaults.TextStyles.BodyText
                    }
                } // End of base details Container
            } // End of Image and Details Container
            
            Container {
                visible: Api.isLogged()
                topMargin: 0
                horizontalAlignment: HorizontalAlignment.Fill
                Divider{bottomMargin: 0}
                attachedObjects: [
                    CustomDialog {
                        id: rateDialog
                        onSaved: {
                            console.log("rating selected: "+rating)
                            Api.hodnotit(movie.id, rating)
                        }
                        onCanceled: {
                            userContainer.enabled = true
                        }
                    }
                ]
                Container {
                    id: userContainer
                    topMargin: 0
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    horizontalAlignment: HorizontalAlignment.Center
                    CustomButton {
                        id: userBtnHodnotit
                        title: qsTr("Rating") + Retranslate.onLocaleOrLanguageChanged
                        imageSource: App.whiteTheme ? "asset:///images/button/hodnotil_som_black.png" : "asset:///images/button/hodnotil_som_white.png"
                        onClicked: {
                            userContainer.enabled = false
                            rateDialog.open()
                        }
                    }
                    CustomButton {
                        id: userBtnVidelSom
                        title: qsTr("I saw") + Retranslate.onLocaleOrLanguageChanged
                        imageSource: App.whiteTheme ? "asset:///images/button/videl_som_black.png" : "asset:///images/button/videl_som_white.png"
                        onClicked: {
                            userContainer.enabled = false
                            Api.videlSom(movie.id, pressed ? 0 : 1)
                        }
                    }
                    CustomButton {
                        id: userBtnChcemVidiet
                        title: qsTr("I want to see") + Retranslate.onLocaleOrLanguageChanged
                        imageSource: App.whiteTheme ? "asset:///images/button/chcem_vidiet_black.png" : "asset:///images/button/chcem_vidiet_white.png"
                        onClicked: {
                            userContainer.enabled = false
                            Api.chcemVidiet(movie.id, pressed ? 0 : 1)
                        }
                    }
                } // end of buttons
            }
            
            Container {
                visible: movie.pocettv
                topMargin: 0
                Header {
                    title: qsTr("Broadcast") + Retranslate.onLocaleOrLanguageChanged
                    subtitle: movie.pocettv
                }
                
                ListView {
                    id: tvList
                    scrollRole: ScrollRole.None
                    horizontalAlignment: HorizontalAlignment.Fill
                    dataModel: GroupDataModel {
                        id: tvModel
                        grouping: ItemGrouping.None
                        sortingKeys: ["startdate", "starttime"]
                    }
                    layout: StackListLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    preferredHeight: Qt.SizeHelper.nType ? 100 : 140
                    listItemComponents: [
                        ListItemComponent {
                            type: "item"
                            ListItemBase{
                                highlight: true
                                backColor: SystemDefaults.Paints.ContainerBackground
                                showDivider: false
                                preferredHeight: Qt.SizeHelper.nType ? 100 : 140
                                preferredWidth: Qt.SizeHelper.nType ? 260 : 300
                                Container {
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    horizontalAlignment: HorizontalAlignment.Fill
                                    verticalAlignment: VerticalAlignment.Fill
                                    
                                    leftPadding: highlightFrameSize
                                    rightPadding: highlightFrameSize
                                    topPadding: highlightFrameSize
                                    bottomPadding: highlightFrameSize
                                    
                                    RemoteImage {
                                        scalingMethod: ScalingMethod.AspectFit
                                        verticalAlignment: VerticalAlignment.Center
                                        preferredHeight: Qt.SizeHelper.nType ? 80 : 120
                                        preferredWidth: Qt.SizeHelper.nType ? 80 : 120
                                        url: "http://img.fdb.cz/tv56/".concat(ListItemData.image).replace(".png", "@2x.png")
                                        layoutProperties: StackLayoutProperties {
                                            spaceQuota: -1
                                        }
                                        cache: true
                                    }
                                    Container {
                                        layoutProperties: StackLayoutProperties {
                                            spaceQuota: 1
                                        }
                                        verticalAlignment: VerticalAlignment.Center
                                        MyLabel {
                                            role: "subtitle"
                                            text: ListItemData.startdate
                                            bottomMargin: 0
                                            topMargin: 0
                                            textStyle.fontWeight: FontWeight.Bold
                                        }
                                        
                                        MyLabel {
                                            role: "subtitle"
                                            text: ListItemData.starttime
                                            bottomMargin: 0
                                            topMargin: 0
                                            textStyle.fontWeight: FontWeight.Bold
                                        }
                                    }
                                    
                                } // End of LeftToRight container
                            }
                        }
                    ]
                    scrollIndicatorMode: ScrollIndicatorMode.None
                    onTriggered: {
                        var item = dataModel.data(indexPath)
                        if (item){
                            var data = {"title": movie.title, "body": movie.info, "station": item.name, "guid": item.idt}
                            Calendar.addEvent(item.alarm, movie.length, data)
                        }
                    }
                }
            
            }
            
            Header {
                title: qsTr("Detailed information") + Retranslate.onLocaleOrLanguageChanged
            }
            
            Container {
                horizontalAlignment: HorizontalAlignment.Fill
                leftPadding: 10
                rightPadding: 10
                topPadding: 10
                Label {
                    text: qsTr("Description:") + Retranslate.onLocaleOrLanguageChanged
                    topMargin: 0
                    bottomMargin: 0
                    textStyle.fontWeight: FontWeight.Bold
                    textStyle.base: SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : SystemDefaults.TextStyles.BodyText
                } 
                Label {
                    text: movie.info ? movie.info : qsTr("Description not available") + Retranslate.onLocaleOrLanguageChanged
                    multiline: true
                    topMargin: 0
                    bottomMargin: 0
                    textStyle.fontStyle: FontStyle.Italic
                    textFormat: TextFormat.Auto
                    textStyle.base: SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : SystemDefaults.TextStyles.BodyText
                }
            }
        } // End of Root Container (TopToBottom)
    } // End of ScrollView
}
