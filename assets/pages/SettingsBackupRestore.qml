import bb.cascades 1.2
import bb.cascades.pickers 1.0

import "../components"

MyPage {
    id: root
    
    property alias title: titleHeader.titleText
    property alias subtitle: titleHeader.subtitleText
    property alias image: titleHeader.imageSource
    
    header: TitleHeader {
        id: titleHeader
        imageVisible: false
    }
    loading: false
    
    
    ScrollView {
        scrollRole: ScrollRole.Main
        scrollViewProperties.pinchToZoomEnabled: false
        scrollViewProperties.scrollMode: ScrollMode.Vertical
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        Container {
            topPadding: 20
            leftPadding: 20
            rightPadding: 20
            bottomPadding: 20
            
            Label {
                horizontalAlignment: HorizontalAlignment.Fill
                topMargin: 0
                multiline: true
                text: qsTr("backup_restore_description") + Retranslate.onLocaleOrLanguageChanged
                textStyle{
                    color: Color.Gray
                
                }
            }
            
            Button {
                horizontalAlignment: HorizontalAlignment.Center
                text: qsTr("Backup") + Retranslate.onLocaleOrLanguageChanged
                onClicked: {
                    console.log(filenames.dbFile)
                    fileSaver.open()
                }
                attachedObjects: [
                    FilePicker {
                        id: fileSaver
                        mode: FilePickerMode.Saver
                        type: FileType.Other
                        defaultType: FileType.Other
                        title: qsTr("FDB.cz backup") + Retranslate.onLocaleOrLanguageChanged
                        defaultSaveFileNames: ["fdbcz.backup"]
                        onFileSelected: {
                            console.log("Saving file: ".concat(selectedFiles))
                            if (Helper.saveFile(selectedFiles)){
                                toast.body = qsTr("Settings was saved!") + Retranslate.onLocaleOrLanguageChanged
                                toast.show()
                                console.log("Saved")
                            }
                        }
                        allowOverwrite: true
                    }
                    
                ]
            }
            
            Button {
                horizontalAlignment: HorizontalAlignment.Center
                text: qsTr("Restore") + Retranslate.onLocaleOrLanguageChanged
                onClicked: {
                    fileLoader.open()
                }
                attachedObjects: [
                    FilePicker {
                        id: fileLoader
                        type: FileType.Other
                        defaultType: FileType.Other
                        onFileSelected: {
                            console.log("Restore file: ".concat(selectedFiles))
                            if (Helper.restoreFile(selectedFiles)){
                                toast.body = qsTr("Settings was restored!") + Retranslate.onLocaleOrLanguageChanged
                                toast.show()
                                Database.reopen()
                                Api.praveBezi()
                                console.log("Restored")
                            }
                        }
                        allowOverwrite: true
                    }
                ]
            }
            
        }
    }
}