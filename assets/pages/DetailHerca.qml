import bb.cascades 1.2
import com.devpda.tools 1.2
import com.devpda.items 1.2

import "../components"


MyPage {
    id: root
    
    function switchView(){
        switch (root.viewIndex){
            case 0:
                viewDelegate.sourceComponent = infoActor
                break;
            case 1:
                viewDelegate.sourceComponent = filmsActor
                break;
        }
        fadeViewInAnim.play()
    }
    
    function request(response){
        if (actor == null){
            actor = response
            viewDelegate.sourceComponent = infoActor
        }
        App.loading = false
    }
    
    property string title
    onTitleChanged: {
        titleHeader.subtitleText = title
    }
    property string actorId
    property ActorItem actor: null
    property int viewIndex: 0
    
    header: TitleHeader {
        id: titleHeader
        titleText: qsTr("Actor detail") + Retranslate.onLocaleOrLanguageChanged
    }
    loading: App.loading
    onActiveChanged: {
        if (active){
            App.loading = true
            App.detailHercaDone.connect(root.request)
            Api.detailHerca(actorId)
            segmentControl.selectedOptionChanged.connect(segmentControl.handleIndexChanged)
            fadeViewInAnim.play()
        }
    }
    
    attachedObjects: [
        FadeTransition {
            id: fadeViewInAnim
            target: viewDelegate
            duration: 300
            easingCurve: StockCurve.Linear
            fromOpacity: 0
            toOpacity: 1
        },
        ImplicitAnimationController {
            enabled: false
        },
        Timer{
            id: loadTimer
            interval: 100
            onTimeout: {
                switchView()
                stop()
            }
        },
        ComponentDefinition {
            id: infoActor
            DetailHercaInfo {
                actor: root.actor
            }
        },
        ComponentDefinition {
            id: filmsActor
            DetailHercaFilmografia {
                actor: root.actor
            }
        }
    ]
    
    Container {
        horizontalAlignment: HorizontalAlignment.Fill
        SegmentedControl {
            id: segmentControl
            opacity: Api.loading ? 0 : 1
            selectedIndex: 0
            horizontalAlignment: HorizontalAlignment.Fill
            topMargin: 0
            bottomMargin: 0
            options: [
                Option {
                    text: qsTr("Info") + Retranslate.onLocaleOrLanguageChanged
                },
                Option {
                    text: qsTr("Filmography") + Retranslate.onLocaleOrLanguageChanged
                }
            ]
            function handleIndexChanged(){
                console.log("Segmented control selection changed to: " + selectedIndex)
                root.viewIndex = selectedIndex
                switchView()
            }
        }
        ControlDelegate {
            id: viewDelegate
            delegateActive: true
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            layoutProperties: StackLayoutProperties {
                spaceQuota: 1
            }
            overlapTouchPolicy: OverlapTouchPolicy.Allow
            onError: {
                console.log("VIEW DELEGATE: ".concat(errorMessage))
            }
            onSourceComponentChanged: {
                console.debug(sourceComponent.toString())
            }
        }
    }
}
