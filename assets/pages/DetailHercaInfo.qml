import bb.cascades 1.2
import com.devpda.items 1.2
import com.devpda.tools 1.2

import "../components"

Container {
    id: root
    /*
     * id
     url
     name
     lastname
     nick
     profession
     nationality
     age
     birthday
     death
     image
     info
     films
     * 
     */
    
    property ActorItem actor: null
    
    ScrollView {
        id: scrollView
        scrollRole: ScrollRole.Main
        onOpacityChanged: {
            if (opacity == 1){
                showFade.play()
            }else{
                hideFade.play()
            }
        }
        attachedObjects: [
            FadeTransition {
                id: showFade
                target: scrollView
                toOpacity: 1.0
                fromOpacity: 0.0
                duration: 300
            },
            FadeTransition {
                id: hideFade
                target: scrollView
                toOpacity: 0.0
                fromOpacity: 1.0
                duration: 300
            }
        ]
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        scrollViewProperties.scrollMode: ScrollMode.Vertical
        scrollViewProperties.pinchToZoomEnabled: false 
        
        content: Container{
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            Header {
                title: qsTr("Basic information") + Retranslate.onLocaleOrLanguageChanged
            }
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                horizontalAlignment: HorizontalAlignment.Fill
                leftPadding: 10
                rightPadding: 10
                topPadding: 10
                Container {
                    //minHeight: 350
                    //maxHeight: 350
                    //minWidth: 250
                    //maxWidth: 250
                    RemoteImage {
                        defaultImage: "asset:///images/person-placeholder.png"
                        scalingMethod: ScalingMethod.AspectFit
                        preferredHeight: SizeHelper.nType ? 270 : 350
                        preferredWidth: SizeHelper.nType ? 170 : 250
                        url: Qt.Settings.imageUrl.concat(actor.image[0]).concat("/").concat(actor.image)
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: -1
                        }
                        cache: true
                    }
                }
                
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    verticalAlignment: VerticalAlignment.Fill
                    leftPadding: 10
                    Label {
                        visible: actor.name
                        text: qsTr("Name:") + Retranslate.onLocaleOrLanguageChanged
                        topMargin: 0
                        bottomMargin: 0
                        textStyle.fontWeight: FontWeight.Bold
                        textStyle.base: SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : undefined
                    } 
                    Label {
                        visible: actor.name
                        text: actor.name.concat(" ").concat(actor.lastname)
                        multiline: true
                        autoSize{
                            maxLineCount: 2
                        }
                        topMargin: 0
                        bottomMargin: 0
                        textStyle.fontWeight: FontWeight.Normal
                        textStyle.base: SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : undefined
                    }
                    Label {
                        visible: actor.birthday
                        text: qsTr("Birth:") + Retranslate.onLocaleOrLanguageChanged
                        topMargin: 0
                        bottomMargin: 0
                        textStyle.fontWeight: FontWeight.Bold
                        textStyle.base: SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : undefined
                    } 
                    Label {
                        visible: actor.birthday
                        text: actor.birthday.concat(actor.age ? " (".concat(actor.age).concat(")") : "")
                        topMargin: 0
                        bottomMargin: 0
                        textStyle.fontWeight: FontWeight.Normal
                        textStyle.base: SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : undefined
                    }
                    Label {
                        visible: actor.death
                        text: qsTr("Death:") + Retranslate.onLocaleOrLanguageChanged
                        topMargin: 0
                        bottomMargin: 0
                        textStyle.fontWeight: FontWeight.Bold
                        textStyle.base: SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : undefined
                    } 
                    Label {
                        visible: actor.death
                        text: "† ".concat(actor.death)
                        topMargin: 0
                        bottomMargin: 0
                        textStyle.fontWeight: FontWeight.Normal
                        textStyle.base: SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : undefined
                    }
                    Label {
                        visible: actor.nationality
                        text: qsTr("Nationality:") + Retranslate.onLocaleOrLanguageChanged
                        topMargin: 0
                        bottomMargin: 0
                        textStyle.fontWeight: FontWeight.Bold
                        textStyle.base: SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : undefined
                    } 
                    Label {
                        visible: actor.nationality
                        text: actor.nationality
                        topMargin: 0
                        bottomMargin: 0
                        textStyle.fontWeight: FontWeight.Normal
                        textStyle.base: SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : undefined
                    }
                } // End of base details Container
            } // End of Image and Details Container
            Header {
                title: qsTr("Detailed information") + Retranslate.onLocaleOrLanguageChanged
            }
            
            Container {
                horizontalAlignment: HorizontalAlignment.Fill
                leftPadding: 10
                rightPadding: 10
                topPadding: 10
                
                Label {
                    visible: actor.profession
                    text: qsTr("Profession:") + Retranslate.onLocaleOrLanguageChanged
                    topMargin: 0
                    bottomMargin: 0
                    textStyle.fontWeight: FontWeight.Bold
                    textStyle.base: SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : undefined
                } 
                Label {
                    visible: actor.profession
                    text: actor.profession
                    multiline: true
                    topMargin: 0
                    bottomMargin: 0
                    textStyle.fontWeight: FontWeight.Normal
                    textStyle.base: SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : undefined
                }
                
                Label {
                    text: qsTr("Bio:") + Retranslate.onLocaleOrLanguageChanged
                    topMargin: 0
                    bottomMargin: 0
                    textStyle.fontWeight: FontWeight.Bold
                    textStyle.base: SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : undefined
                } 
                Label {
                    text: actor.info ? actor.info : qsTr("Bio not available")  + Retranslate.onLocaleOrLanguageChanged
                    multiline: true
                    topMargin: 0
                    bottomMargin: 0
                    textStyle.fontWeight: FontWeight.Normal
                    textFormat: TextFormat.Html
                    textStyle.base: SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : undefined
                }
            }
        
        } // End of Root Container (TopToBottom)
    } // End of ScrollView
}
