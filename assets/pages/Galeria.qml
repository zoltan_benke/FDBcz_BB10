import bb.cascades 1.2
import com.devpda.tools 1.2
import com.devpda.items 1.2

import "../components"
import "../items"
import "../actions"

MyPage {
    id: root
    
    property MovieItem film: null
    header: TitleHeader {
        titleText: qsTr("Gallery") + Retranslate.onLocaleOrLanguageChanged
        subtitleText: if (film) film.title
    }
    loading: App.loading
    
    function request(response){
        console.log("request(), response = ".concat(response))
        var json = JSON.parse(response)
        for (var item in json){
            model.append(json[item])
        }
        App.loading = false
    }
    
    
    onActiveChanged: {
        if (active){
            App.loading = true
            App.galeriaDone.connect(request)
            Api.galeriaFilmu(film.id)
        }
    }
    
    actions: [
        RefreshAction {
            ActionBar.placement: ActionBarPlacement.OnBar
            onClicked: {
                App.loading = true
                model.clear()
                Api.galeriaFilmu(film.id)
            }
        }
    ]
    
    
    ListView {
        opacity: App.loading ? 0 : 1
        scrollRole: ScrollRole.Main
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        dataModel: ArrayDataModel {
            id: model 
        }
        listItemComponents: [
            ListItemComponent {
                ListItemGallery {}
            }
        ]
    }
}