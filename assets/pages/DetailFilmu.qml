import bb.cascades 1.2
import com.devpda.tools 1.2
import com.devpda.items 1.2
import bb.multimedia 1.2

import "../components"
import "../actions"

MyPage {
    id: root
    
    function switchView(){
        switch (root.viewIndex){
            case 0:
                viewDelegate.sourceComponent = infoMovie
                break;
            case 1:
                viewDelegate.sourceComponent = actorMovie
                break;
            case 2:
                viewDelegate.sourceComponent = commentMovie
                break;
            case 3:
                viewDelegate.sourceComponent = episodesMovie
                break;
        }
        fadeViewInAnim.play()
    }
    
    function request(response){
        if (movie == null){
            movie = response
            if (movie.pocetdilu){
                segmentControl.add(episodesOption.createObject())
            }
            switchView()
            Api.mojFilm(filmId)
        }
    }
    
    function mojFilmDone(response){
        var json = JSON.parse(response)
        mojFilm = json
        App.loading = false
    }
    
    property alias title: titleHeader.subtitleText
    property string filmId    
    property MovieItem movie: null
    property int viewIndex: 0
    property variant mojFilm: null
    
    header: TitleHeader {
        id: titleHeader
        titleText: qsTr("Movie detail") + Retranslate.onLocaleOrLanguageChanged
    }
    loading: App.loading
    
    onCreationCompleted: {
        App.mojFilmDone.connect(mojFilmDone)
        App.detailFilmuDone.connect(request)
    }
    
    onActiveChanged: {
        if (active){
            App.loading = true
            activePane.peekEnabled = false 
            Api.detailFilmu(filmId)
            segmentControl.selectedOptionChanged.connect(segmentControl.handleIndexChanged)
            fadeViewInAnim.play()
        }
    }
    
    actions: [
        PlayAction {
            enabled: (movie != null) && movie.video.length
            ActionBar.placement: ActionBarPlacement.OnBar
            onClicked: {
                invoker.target = "sys.mediaplayer.previewer"
                invoker.uri = movie.video[0].stream
                invoker.data = Helper.encodeObject({"contentTitle": movie.title})
                invoker.invoke()
            }
            attachedObjects: [
                Invoker {
                    id: invoker
                    onInvocationFailed: {
                        console.log("onInvocationFailed")
                    }
                }
            ]
        },
        GalleryAction {
            ActionBar.placement: ActionBarPlacement.OnBar
            enabled: (movie != null) && movie.fotogalerie
            onClicked: {
                var page = galerryPage.createObject()
                activePane.push(page)
            }
        },
        BrowserAction {
            enabled: (movie != null)
            onClicked: {
                Helper.openInBrowser(movie.id)
            }
        }
    ]
    /*
     ForeignWindowControl {
     id: videoPlayer
     visible: (mediaPlayer.play() != MediaError.None)
     preferredHeight: SizeHelper.maxHeight
     preferredWidth: SizeHelper.maxWidth
     updatedProperties: WindowProperty.Size | 
     WindowProperty.Position | 
     WindowProperty.Visible        
     }
     */
    
    attachedObjects: [
        FadeTransition {
            id: fadeViewInAnim
            target: viewDelegate
            duration: 300
            easingCurve: StockCurve.Linear
            fromOpacity: 0
            toOpacity: 1
        },
        ImplicitAnimationController {
            enabled: false
        },
        Timer{
            id: loadTimer
            interval: 100
            onTimeout: {
                switchView()
                stop()
            }
        },
        ComponentDefinition {
            id: infoMovie
            DetailFilmuInfo {
                movie: root.movie
                mojFilm: root.mojFilm
            }
        },
        ComponentDefinition {
            id: actorMovie
            DetailFilmuObsadenie {
                movie: root.movie
            }
        },
        ComponentDefinition {
            id: commentMovie
            DetailFilmuKomentare{
                movie: root.movie
            }
        },
        ComponentDefinition {
            id: galerryPage
            Galeria {
                film: root.movie
            }
        },
        ComponentDefinition {
            id: episodesMovie
            DetailFilmuEpisodes {
                movie: root.movie
            }
        },
        ComponentDefinition {
            id: episodesOption
            Option {
                text: qsTr("Episodes") + Retranslate.onLocaleOrLanguageChanged
            }
        }
    ]
    
    Container {
        horizontalAlignment: HorizontalAlignment.Fill
        SegmentedControl {
            id: segmentControl
            opacity: Api.loading ? 0 : 1
            selectedIndex: 0
            horizontalAlignment: HorizontalAlignment.Fill
            topMargin: 0
            bottomMargin: 0
            options: [
                Option {
                    text: qsTr("Info") + Retranslate.onLocaleOrLanguageChanged
                },
                Option {
                    text: qsTr("Cast") + Retranslate.onLocaleOrLanguageChanged
                },
                Option {
                    text: qsTr("Comments") + Retranslate.onLocaleOrLanguageChanged
                }
            ]
            function handleIndexChanged(){
                console.log("Segmented control selection changed to: " + selectedIndex)
                root.viewIndex = selectedIndex
                switchView()
            }
        }
        ControlDelegate {
            id: viewDelegate
            delegateActive: true
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            layoutProperties: StackLayoutProperties {
                spaceQuota: 1
            }
            overlapTouchPolicy: OverlapTouchPolicy.Allow
            onError: {
                console.log("VIEW DELEGATE: ".concat(errorMessage))
            }
            onSourceComponentChanged: {
                console.debug(sourceComponent.toString())
            }
        }
    }
}
