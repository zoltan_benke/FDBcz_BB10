import bb.cascades 1.2
import com.devpda.items 1.2
import com.devpda.tools 1.2


import "../components"
import "../items"

Container {
    id: root
    
    property MovieItem movie: null
    onMovieChanged: {
        for (var item in movie.movie_peoples){
            //console.debug("NAME: ".concat(movie.movie_peoples[item].name))
            actorsModel.insert(movie.movie_peoples[item])
        }
    }
    
    
    attachedObjects: [
        ComponentDefinition {
            id: detailHerca
            source: "DetailHerca.qml"
        },
        NavigationPane {
            id: navigationPane
        }
    ]
    
    
    ListView {
        id: actorsList
        scrollRole: ScrollRole.Main
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        dataModel: GroupDataModel {
            id: actorsModel
            grouping: ItemGrouping.ByFullValue
            sortingKeys: ["type", "name", "lastname"]
            sortedAscending: false
        }
        listItemComponents: [
            ListItemComponent {
                type: "item"
                ListItemActors{}
            }
        ]
        layout: StackListLayout {
            headerMode: ListHeaderMode.Sticky
        
        }
        onTriggered: {
            var item = dataModel.data(indexPath)
            if (item){
                var page = detailHerca.createObject()
                page.actorId = item.id
                activePane.push(page)
            }
        }
    }
}
