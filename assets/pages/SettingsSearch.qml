import bb.cascades 1.2
import com.devpda.tools 1.2

import "../components"
import "../items"

MyPage {
    id: root
    
    
    property alias title: titleHeader.titleText
    property alias subtitle: titleHeader.subtitleText
    property alias image: titleHeader.imageSource
    
    header: TitleHeader {
        id: titleHeader
        imageVisible: false
    }
    loading: false
    
    
    ScrollView {
        scrollRole: ScrollRole.Main
        scrollViewProperties.pinchToZoomEnabled: false
        scrollViewProperties.scrollMode: ScrollMode.Vertical
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        Container {
            
            Header {
                title: qsTr("Searching movie/person") + Retranslate.onLocaleOrLanguageChanged
            }
            
            Container {
                topPadding: 20
                leftPadding: 20
                rightPadding: 20
                bottomPadding: 20
                
                Label {
                    horizontalAlignment: HorizontalAlignment.Fill
                    topMargin: 0
                    multiline: true
                    text: qsTr("Limit of results which will be displayed in searching. Default limit is 10.") + Retranslate.onLocaleOrLanguageChanged
                    textStyle{
                        color: Color.Gray
                    
                    }
                }
                
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Label {
                        multiline: true
                        text: qsTr("Current limit") + Retranslate.onLocaleOrLanguageChanged
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                    }
                    
                    Label {
                        id: searchLimitLabel
                        text: Settings.searchLimit
                        textStyle{
                            color: Color.create("#".concat(Settings.activeColor))
                        }
                    }
                } // actuall limit container
                
                
                Slider {
                    id: slider
                    fromValue: 10
                    toValue: 50
                    value: Settings.searchLimit
                    onImmediateValueChanged: {
                        searchLimitLabel.text = Math.floor(immediateValue)
                    }
                    onValueChanged: {
                        Settings.searchLimit = value
                    }
                }
            } // End of Search movie/person limit
            
            Header {
                title: qsTr("Searching tv program") + Retranslate.onLocaleOrLanguageChanged
            }
            
            Container {
                topPadding: 20
                leftPadding: 20
                rightPadding: 20
                bottomPadding: 20
                
                Label {
                    horizontalAlignment: HorizontalAlignment.Fill
                    topMargin: 0
                    multiline: true
                    text: qsTr("Limit of results which will be displayed in searching for program. Default limit is 10.") + Retranslate.onLocaleOrLanguageChanged
                    textStyle{
                        color: Color.Gray
                    
                    }
                }
                
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Label {
                        multiline: true
                        text: qsTr("Current limit") + Retranslate.onLocaleOrLanguageChanged
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                    }
                    
                    Label {
                        id: searchLimitProgramLabel
                        text: Settings.searchLimitProgram
                        textStyle{
                            color: Color.create("#".concat(Settings.activeColor))
                        }
                    }
                } // actuall limit container
                
                
                Slider {
                    fromValue: 10
                    toValue: 50
                    value: Settings.searchLimitProgram
                    onImmediateValueChanged: {
                        searchLimitProgramLabel.text = Math.floor(immediateValue)
                    }
                    onValueChanged: {
                        Settings.searchLimitProgram = value
                    }
                }
            }
            
        }
    }

}