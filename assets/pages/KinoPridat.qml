import bb.cascades 1.2
import com.devpda.tools 1.2

import "../components"
import "../items"
import "../actions"

MyPage {
    id: root
    
    signal kinoChanged()
    
    function dropdownKraje()
    {
        var list = []
        list.push(qsTr("Karlovarsky") + Retranslate.onLocaleOrLanguageChanged)
        list.push(qsTr("Ustecky") + Retranslate.onLocaleOrLanguageChanged)
        list.push(qsTr("Liberecky") + Retranslate.onLocaleOrLanguageChanged)
        list.push(qsTr("Kralovehradecky") + Retranslate.onLocaleOrLanguageChanged)
        list.push(qsTr("Pardubicky") + Retranslate.onLocaleOrLanguageChanged)
        list.push(qsTr("Olomoucky") + Retranslate.onLocaleOrLanguageChanged)
        list.push(qsTr("Moravskoslezsky") + Retranslate.onLocaleOrLanguageChanged)
        list.push(qsTr("Zlinsky") + Retranslate.onLocaleOrLanguageChanged)
        list.push(qsTr("Jihomoravsky") + Retranslate.onLocaleOrLanguageChanged)
        list.push(qsTr("Vysocina") + Retranslate.onLocaleOrLanguageChanged)
        list.push(qsTr("Jihocesky") + Retranslate.onLocaleOrLanguageChanged)
        list.push(qsTr("Plzensky") + Retranslate.onLocaleOrLanguageChanged)
        list.push(qsTr("Hlavni mesto Praha") + Retranslate.onLocaleOrLanguageChanged)
        list.push(qsTr("Stredocesky") + Retranslate.onLocaleOrLanguageChanged)
        for (var item in list){
            var ob = option.createObject()
            ob.text = list[item]
            dropKraje.add(ob)
        }
    }
    
    
    function dropdownMesta(response)
    {
        var json = JSON.parse(response)
        for (var item in json.data){
            var ob = option.createObject()
            ob.value = json.data[item].url
            ob.text = json.data[item].name
            dropMesta.add(ob)
        }
        App.loading = false
    }
    
    
    function request(response){
        var json = JSON.parse(response)
        model.insertList(json.data)
        App.loading = false
    }
    
    
    function load(data){
        App.loading = true
        model.clear()
        if (mesta){
            Api.kinoMesta(root.kraje[dropKraje.selectedIndex])
        }else{
            Api.kinoZoznamKin(data)
        }
    }
    
    
    property variant kraje: ["karlovarsky", "ustecky", "liberecky", "kralovehradecky", "pardubicky", 
    "olomoucky", "moravskoslezsky", "zlinsky", "jihomoravsky", "vysocina", "jihocesky", "plzensky", "praha", "stredocesky"]
    property bool mesta: true
    property int lastPosition
    header: TitleHeader {
        titleText: qsTr("Add cinema") + Retranslate.onLocaleOrLanguageChanged
    }
    loading: App.loading
    onCreationCompleted: {
        App.kinoMestaDone.connect(request)
        App.kinoZoznamDone.connect(request)
        dropdownKraje()
        if (Settings.language === "sk")
            root.removeAllActions()
    }
    onActiveChanged: {
        if (active){
            if (Settings.language === "sk"){
                mesta = false
                Api.kinoZoznamKin("slovensko")
            }
        }
    }
    
    attachedObjects: [
        ComponentDefinition {
            id: option
            Option {}
        }
    ]
    
    actions: [
        BackAction {
            enabled: !mesta && (dropKraje.selectedIndex != 12) 
            ActionBar.placement: ActionBarPlacement.OnBar
            title: qsTr("Cities") + Retranslate.onLocaleOrLanguageChanged
            icon: "asset:///images/actions/list.png"
            onClicked: {
                App.loading = true
                mesta = true
                load()
            }
        }
    ]
    
    
    Container {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        DropDown {
            id: dropKraje
            visible: Settings.language === "cs"
            enabled: !App.loading
            title: qsTr("Region") + Retranslate.onLocaleOrLanguageChanged
            horizontalAlignment: HorizontalAlignment.Fill
            onSelectedOptionChanged: {
                console.log("Kraj: ".concat(root.kraje[selectedIndex]))
                if (selectedIndex == 12){
                    root.mesta = false
                    load(root.kraje[selectedIndex])
                }else{
                    root.mesta = true
                    load(root.kraje[selectedIndex])
                }
            }
        } // end of kraje dropdown
        
        
        ListView {
            id: listView
            scrollRole: ScrollRole.Main
            opacity: App.loading ? 0 : 1
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            function itemType(data, indexPath){
                if (Settings.language === "cs"){
                    return root.mesta ? "mesta" : "kina"
                }
                else{
                    return "kina"
                }
            }
            
            dataModel: GroupDataModel {
                id: model
                grouping: ItemGrouping.None
                sortingKeys: ["name"]
            }
            listItemComponents: [
                ListItemComponent {
                    type: "mesta"  
                    ListItemKinoMesta {}
                },
                ListItemComponent {
                    type: "kina"
                    ListItemOblubeneKina{}
                }
            ]
            onTriggered: {
                var item = dataModel.data(indexPath)
                if (item){
                    if (mesta){
                        mesta = false
                        load(item.url)
                    }else{
                        if (Database.isFavoriteCinema(item.id)){
                            Database.removeCinema(item.id)
                        }else{
                            Database.addCinema(item.id, JSON.stringify(item))
                        }
                        dataModel.updateItem(indexPath,item)
                        root.kinoChanged()
                    }
                }
            }
        }
    } 
}