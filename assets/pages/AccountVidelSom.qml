import bb.cascades 1.2

import "../components"
import "../items"
import "../actions"

MyPage {
    
    function request(response){
        model.clear()
        var json = JSON.parse(response)
        if (json.pocet == 0){
            noContentVisible = true
        }else{
            model.insertList(json.film)
        }
        App.loading = false
    }
    
    property alias title: titleHeader.titleText
    
    header: TitleHeader {
        id: titleHeader
    }
    loading: App.loading
    
    onCreationCompleted: {
        App.mojeVidelSomDone.connect(request)
    }
    
    onActiveChanged: {
        if (active){
            App.loading = true
            Api.mojeVidelSom("", 0, 50)
        }
    }
    
    actions: [
        RefreshAction {
            ActionBar.placement: ActionBarPlacement.OnBar
            onClicked: {
                App.loading = true
                Api.mojeVidelSom("", 0, 50)
            }
        }
    ]
    
    attachedObjects: [
        ComponentDefinition {
            id: movieDetail
            source: "DetailFilmu.qml"
        }
    ]
    
    ListView {
        scrollRole: ScrollRole.Main
        leadingVisual: SortingVisual {}
        opacity: App.loading ? 0 : 1
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        dataModel: GroupDataModel {
            id: model
            sortingKeys: ["title"]
            grouping: ItemGrouping.None
        }
        layout: StackListLayout {
        
        }
        listItemComponents: [
            ListItemComponent {
                type: "item"
                ListItemVidelSom{}
            }
        ]
        onTriggered: {
            var item = dataModel.data(indexPath)
            if (item){
                var page = movieDetail.createObject()
                page.filmId = item.film_id
                page.title = item.title
                activePane.push(page)
            }
        }
    } // end of listview

}