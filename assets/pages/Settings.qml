import bb.cascades 1.2
import com.devpda.tools 1.2

import "../components"
import "../items"

MyPage {
    
    header: TitleHeader {
        id: titleHeader
        titleText: settingsAction.title
    }
    loading: false

    onActiveChanged: {
        if (active){
            console.log("onActiveChanged, Settings.qml, true")
            settingsAction.enabled = false
        }else{
            console.log("onActiveChanged, Settings.qml, false")
            settingsAction.enabled = true
        }
    }
    
    
    attachedObjects: [
        ComponentDefinition {
            id: general
            source: "SettingsGeneral.qml"
        },
        ComponentDefinition {
            id: appearance
            source: "SettingsAppearance.qml"
        },
        ComponentDefinition {
            id: calendar
            source: "SettingsCalendar.qml"
        },
        ComponentDefinition {
            id: search
            source: "SettingsSearch.qml"
        },
        ComponentDefinition {
            id: cache
            source: "SettingsCache.qml"
        },
        ComponentDefinition {
            id: backup
            source: "SettingsBackupRestore.qml"
        }
    ]
    
    
    ListView {
        id: listVIew
        onCreationCompleted: {
            dataModel.append([{"name": qsTr("General") + Retranslate.onLocaleOrLanguageChanged, "subtitle": qsTr("Station logo, filtering, images quality etc...") + Retranslate.onLocaleOrLanguageChanged, "icon": App.whiteTheme ? "asset:///images/settings/settings_black.png" : "asset:///images/settings/settings_white.png"}, 
                    {"name": qsTr("Appearance") + Retranslate.onLocaleOrLanguageChanged, "subtitle": qsTr("Application theme, highlight colors etc..") + Retranslate.onLocaleOrLanguageChanged, "icon": App.whiteTheme ? "asset:///images/settings/appearance_black.png" : "asset:///images/settings/appearance_white.png"},
                    {"name": qsTr("Calendar") + Retranslate.onLocaleOrLanguageChanged, "subtitle": qsTr("Calendar, notifications etc...") + Retranslate.onLocaleOrLanguageChanged, "icon": App.whiteTheme ? "asset:///images/settings/calendar_black.png" : "asset:///images/settings/calendar_white.png"},
                    {"name": qsTr("Searching") + Retranslate.onLocaleOrLanguageChanged, "subtitle": qsTr("Searching limit etc...") + Retranslate.onLocaleOrLanguageChanged, "icon": App.whiteTheme ? "asset:///images/settings/search_black.png" : "asset:///images/settings/search_white.png"},
                    {"name": qsTr("Cache") + Retranslate.onLocaleOrLanguageChanged, "subtitle": qsTr("Cache limit, cached images etc...") + Retranslate.onLocaleOrLanguageChanged, "icon": App.whiteTheme ? "asset:///images/settings/offline_black.png" : "asset:///images/settings/offline_white.png"},
                    {"name": qsTr("Backup/Restore") + Retranslate.onLocaleOrLanguageChanged, "subtitle": qsTr("Backup/Restore favorite stations, cinemas etc...") + Retranslate.onLocaleOrLanguageChanged, "icon": App.whiteTheme ? "asset:///images/settings/backup-restore_black.png" : "asset:///images/settings/backup-restore_white.png"}
                    ])
        }
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        dataModel: ArrayDataModel {
            id: model
        }
        layout: StackListLayout {
        }
        listItemComponents: [
            ListItemComponent {
                ListItemBase {
                    id: rootItem
                    preferredHeight: Qt.SizeHelper.nType ? 100 : 120
                    backColor: SystemDefaults.Paints.ContainerBackground
                    Container {
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        horizontalAlignment: HorizontalAlignment.Fill
                        verticalAlignment: VerticalAlignment.Fill
                        
                        ImageView {
                            verticalAlignment: VerticalAlignment.Center
                            imageSource: ListItemData.icon
                            preferredHeight: Qt.SizeHelper.nType ? 70 : 100
                            preferredWidth: Qt.SizeHelper.nType ? 70 : 100
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: -1
                            }
                        }
                        Container {
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                            verticalAlignment: VerticalAlignment.Center
                            Label{
                                text: ListItemData.name
                                textStyle.base: SystemDefaults.TextStyles.PrimaryText
                                bottomMargin: 0
                            }
                            
                            Label {
                                topMargin: 0
                                text: ListItemData.subtitle
                                textStyle.base: SystemDefaults.TextStyles.SubtitleText
                            }
                        }
                    } // End of Container
                } // End of ListItemBase
            } // ListItemComponent
        ]
        onTriggered: {
            console.debug("INDEX: ".concat(indexPath))
            var data = dataModel.data(indexPath)
            var page;
            switch (parseInt(indexPath)){
                case 0: // General settings
                    page = general.createObject()
                    break;
                case 1:
                    page = appearance.createObject()
                    break;
                case 2:
                    page = calendar.createObject()
                    break;
                case 3:
                    page = search.createObject()
                    break;
                case 4:
                    page = cache.createObject()
                    break;
                case 5:
                    page = backup.createObject()
                    break;
                default:
                    console.log("Unknown index")
                    return;
            }
            page.title = titleHeader.titleText
            page.subtitle = data.name
            page.image = data.icon
            activePane.push(page)
        }
    }
}