import bb.cascades 1.2

import "../components"

MyPage {
    id: root
    
    property alias title: titleHeader.titleText
    property alias subtitle: titleHeader.subtitleText
    property alias image: titleHeader.imageSource
    
    header: TitleHeader {
        id: titleHeader
        imageVisible: false
    }
    loading: false
    
    
    ScrollView {
        scrollRole: ScrollRole.Main
        scrollViewProperties.pinchToZoomEnabled: false
        scrollViewProperties.scrollMode: ScrollMode.Vertical
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        Container {
            
            
            Header {
                title: qsTr("Application language") + Retranslate.onLocaleOrLanguageChanged
            }
            
            
            Container {
                leftPadding: 10
                rightPadding: 10
                topPadding: 5
                bottomPadding: 5
                DropDown {
                    id: themeDropDown
                    title: qsTr("Current language") + Retranslate.onLocaleOrLanguageChanged
                    selectedIndex: (Settings.language == "cs") ? 0 : 1
                    Option {
                        text: qsTr("Czech") + Retranslate.onLocaleOrLanguageChanged
                        value: "cs"
                    }
                    
                    Option {
                        text: qsTr("Slovak") + Retranslate.onLocaleOrLanguageChanged
                        value: "sk"
                    }
                    onSelectedIndexChanged: {
                        if (Settings.language != selectedValue){
                            Settings.language = selectedValue
                            App.changeLanguage(selectedValue)
                        }
                    }
                }
            }
            
            
            Header {
                title: qsTr("Tv") + Retranslate.onLocaleOrLanguageChanged
            }
            
            /*Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                topPadding: 20
                leftPadding: 20
                rightPadding: 20
                bottomPadding: 20
                
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    Label {
                        horizontalAlignment: HorizontalAlignment.Left
                        text: qsTr("Logo stanice", "Station logo") + Retranslate.onLocaleOrLanguageChanged
                        bottomMargin: 0
                    }
                    Label {
                        topMargin: 0
                        text: qsTr("Zobrazí/Skryje logo stanice", "Show/Hide station logo") + Retranslate.onLocaleOrLanguageChanged
                        textStyle{
                            base: SystemDefaults.TextStyles.SubtitleText
                            color: Color.Gray
                        }
                    }
                }
                ToggleButton {
                    horizontalAlignment: horizontalAlignment.Right
                    checked: Settings.showLogo
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    onCheckedChanged: {
                        Settings.showLogo = checked
                    }
                }
            } // end of Station logo */
            /*
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                topPadding: 20
                leftPadding: 20
                rightPadding: 20
                bottomPadding: 20
                
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    Label {
                        horizontalAlignment: HorizontalAlignment.Left
                        text: qsTr("Názov stanice", "Station name") + Retranslate.onLocaleOrLanguageChanged
                        bottomMargin: 0
                    }
                    Label {
                        topMargin: 0
                        text: qsTr("Zobrazí/Skryje názov stanice", "Show/Hide station name") + Retranslate.onLocaleOrLanguageChanged
                        textStyle{
                            base: SystemDefaults.TextStyles.SubtitleText
                            color: Color.Gray
                        }
                    }
                }
                ToggleButton {
                    horizontalAlignment: horizontalAlignment.Right
                    checked: Settings.showStationName
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    onCheckedChanged: {
                        Settings.showStationName = checked
                    }
                }
            }*/
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                topPadding: 20
                leftPadding: 20
                rightPadding: 20
                bottomPadding: 20
                
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    Label {
                        horizontalAlignment: HorizontalAlignment.Left
                        text: qsTr("Show only active stations")  + Retranslate.onLocaleOrLanguageChanged
                        bottomMargin: 0
                    }
                    Label {
                        multiline: true
                        topMargin: 0
                        text: qsTr("Hides all stations, which currently does not broadcast.") + Retranslate.onLocaleOrLanguageChanged
                        textStyle{
                            base: SystemDefaults.TextStyles.SubtitleText
                            color: Color.Gray
                        }
                    }
                }
                ToggleButton {
                    horizontalAlignment: horizontalAlignment.Right
                    verticalAlignment: VerticalAlignment.Center
                    checked: Settings.showActive
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    onCheckedChanged: {
                        Settings.showActive = checked
                    }
                }
            }
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                topPadding: 20
                leftPadding: 20
                rightPadding: 20
                bottomPadding: 20
                
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    Label {
                        horizontalAlignment: HorizontalAlignment.Left
                        text: qsTr("Show first program")  + Retranslate.onLocaleOrLanguageChanged
                        bottomMargin: 0
                    }
                    Label {
                        topMargin: 0
                        text: qsTr("Show/Hide first following program") + Retranslate.onLocaleOrLanguageChanged
                        textStyle{
                            base: SystemDefaults.TextStyles.SubtitleText
                            color: Color.Gray
                        }
                    }
                }
                ToggleButton {
                    horizontalAlignment: horizontalAlignment.Right
                    verticalAlignment: VerticalAlignment.Center
                    checked: Settings.showFirstProgram
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    onCheckedChanged: {
                        Settings.showFirstProgram = checked
                    }
                }
            } // End of First program container
            
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                topPadding: 20
                leftPadding: 20
                rightPadding: 20
                bottomPadding: 20
                
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    Label {
                        horizontalAlignment: HorizontalAlignment.Left
                        text: qsTr("Show second program")  + Retranslate.onLocaleOrLanguageChanged
                        bottomMargin: 0
                    }
                    Label {
                        topMargin: 0
                        text: qsTr("Show/Hide second following program") + Retranslate.onLocaleOrLanguageChanged
                        textStyle{
                            base: SystemDefaults.TextStyles.SubtitleText
                            color: Color.Gray
                        }
                    }
                }
                ToggleButton {
                    horizontalAlignment: horizontalAlignment.Right
                    verticalAlignment: VerticalAlignment.Center
                    checked: Settings.showSecondProgram
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    onCheckedChanged: {
                        Settings.showSecondProgram = checked
                    }
                }
            } // End of Second program container
            
            Header {
                title: qsTr("Tv Tips") + Retranslate.onLocaleOrLanguageChanged
            }
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                topPadding: 20
                leftPadding: 20
                rightPadding: 20
                bottomPadding: 20
                
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    Label {
                        horizontalAlignment: HorizontalAlignment.Left
                        text: qsTr("Filter")  + Retranslate.onLocaleOrLanguageChanged
                        bottomMargin: 0
                    }
                    Label {
                        multiline: true
                        topMargin: 0
                        text: qsTr("Filter tv tips according to favorites stations") + Retranslate.onLocaleOrLanguageChanged
                        textStyle{
                            base: SystemDefaults.TextStyles.SubtitleText
                            color: Color.Gray
                        }
                    }
                }
                ToggleButton {
                    horizontalAlignment: horizontalAlignment.Right
                    verticalAlignment: VerticalAlignment.Center
                    checked: Settings.filterTipy
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    onCheckedChanged: {
                        Settings.filterTipy = checked
                    }
                }
            } // End of Filtering tv tips
            
            Header {
                title: qsTr("Default view") + Retranslate.onLocaleOrLanguageChanged
            }
            
            Container {
                leftPadding: 10
                rightPadding: 10
                topPadding: 5
                Label {
                    multiline: true
                    textStyle{
                        base: SystemDefaults.TextStyles.SubtitleText
                        color: Color.Gray
                    }
                    text: qsTr("If you have set favorite stations you can choose which view will be displayed as default after start application") + Retranslate.onLocaleOrLanguageChanged
                }
                
                DropDown {
                    enabled: Database.favoriteCount
                    selectedIndex: Settings.startView
                    title: qsTr("Current view") + Retranslate.onLocaleOrLanguageChanged
                    options: [
                        Option {
                            text: qsTr("Others") + Retranslate.onLocaleOrLanguageChanged
                            value: 0
                        },
                        Option {
                            text: qsTr("Favorite") + Retranslate.onLocaleOrLanguageChanged
                            value: 1
                        }
                    ]
                    onSelectedOptionChanged: {
                        console.log("onSelectedValueChanged, selectedValue = ".concat(selectedValue))
                        Settings.startView = selectedValue
                    }
                }
            }
            
            Header {
                title: qsTr("Images quality") + Retranslate.onLocaleOrLanguageChanged
            }
            
            Container {
                leftPadding: 10
                rightPadding: 10
                topPadding: 5
                DropDown {
                    title: qsTr("Current quality") + Retranslate.onLocaleOrLanguageChanged
                    
                    selectedIndex: Settings.imageQuality
                    options: [
                        Option {
                            text: qsTr("Low") + Retranslate.onLocaleOrLanguageChanged
                            //description: qsTr("Nízka kvalita obrázkov. Nízka spotreba internetových dát") + Retranslate.onLocaleOrLanguageChanged
                            value: 0
                            onSelectedChanged: {
                                if (selected)
                                    Settings.imageQuality = value
                            }
                        },
                        Option {
                            text: qsTr("Medium") + Retranslate.onLocaleOrLanguageChanged
                            //description: qsTr("Stredná kvalita obrázkov. Stredná spotreba internetových dát") + Retranslate.onLocaleOrLanguageChanged
                            value: 1
                            onSelectedChanged: {
                                if (selected)
                                    Settings.imageQuality = value
                            }
                        },
                        Option {
                            text: qsTr("High") + Retranslate.onLocaleOrLanguageChanged
                            //description: qsTr("Vysoká kvalita obrázkov. Vysoká spotreba internetových dát") + Retranslate.onLocaleOrLanguageChanged
                            value: 2
                            onSelectedChanged: {
                                if (selected)
                                    Settings.imageQuality = value
                            }
                        }
                    ]
                }
            } // End of DropDown container
        } // End of Root container
    } // End of Scroll View
}