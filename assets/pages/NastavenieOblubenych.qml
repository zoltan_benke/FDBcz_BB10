import bb.cascades 1.2

import "../components"
import "../items"
import "../actions"

MyPage {
    id: root
    
    
    function request(response){
        console.log(response)
        var json = JSON.parse(response)
        var list = Database.favoriteList
        for (var item in json.data){
            var program = json.data[item]
            if (list.indexOf(program.id) != -1){
                program.favorite = true
            }else{
                program.favorite = false
            }
            model.insert(program)
        }
        App.loading = false
    }
    
    
    function addItem(item){
        Database.addFavorite(item.id)
        var data = baseModel.data(baseModel.find([item.name]))
        baseModel.remove(data)
        data.favorite = true
        favoriteModel.insert(data)
    }
    
    
    function removeItem(item){
        Database.removeFavorite(item.id)
        var data = favoriteModel.data(favoriteModel.find([item.name]))
        favoriteModel.remove(data)
        data.favorite = false
        baseModel.insert(data)
    }
    
    property GroupDataModel baseModel
    property GroupDataModel favoriteModel
    
    header: TitleHeader {
        titleText: qsTr("Favorite stations") + Retranslate.onLocaleOrLanguageChanged
        subtitleText: qsTr("Settings") + Retranslate.onLocaleOrLanguageChanged
    }
    loading: App.loading
    onCreationCompleted: {
        App.programDone.connect(request)
        App.loading = true
    }
    onActiveChanged: {
        if (active){
            Api.program()
        }
    }
    actions: [
        MultiSelectActionItem {
            title: qsTr("Select more") + Retranslate.onLocaleOrLanguageChanged
            multiSelectHandler: listView.multiSelectHandler
        }/*,
        SelectAction {
            onClicked: {
                listView.selectAll()
            }
        },
        SelectAction {
            selectAll: false
            onClicked: {
                listView.clearSelection()
            }
        }*/
    ]
    
    
    
    ListView {
        id: listView
        scrollRole: ScrollRole.Main
        onTriggered: {
            var item = dataModel.data(indexPath)
            if (item){
                if (item.favorite){
                    item.favorite = false
                    root.removeItem(item)
                }else{
                    item.favorite = true
                    root.addItem(item)
                }
                dataModel.updateItem(indexPath,item)
            }
        }
        
        multiSelectHandler{
            actions: [
                FavoriteAction {
                    add: true
                    onClicked: {
                        var list = listView.selectionList()
                        listView.clearSelection()
                        for (var i = 0; i < list.length; ++i){
                            var program = listView.dataModel.data(list[i])
                            program.favorite = true
                            root.addItem(program)
                            listView.dataModel.updateItem(list[i],program)
                        }
                    }
                },
                FavoriteAction {
                    add: false
                    onClicked: {
                        var list = listView.selectionList()
                        listView.clearSelection()
                        for (var i = 0; i < list.length; ++i){
                            var program = listView.dataModel.data(list[i])
                            program.favorite = false
                            root.removeItem(program)
                            listView.dataModel.updateItem(list[i],program)
                        }
                    }
                }
            ]
        }
        
        contextActions: [
            ActionSet {
                MultiSelectActionItem {
                    multiSelectHandler: listView.multiSelectHandler
                }
            }
        ]
        
        onSelectionChanged: {
            if (selectionList().length > 1) {
                multiSelectHandler.status = selectionList().length + " "+ qsTr("selected stations") + Retranslate.onLocaleOrLanguageChanged;
            } else if (selectionList().length == 1) {
                multiSelectHandler.status = qsTr("1 selected station") + Retranslate.onLocaleOrLanguageChanged;
            } else {
                multiSelectHandler.status = qsTr("no selected") + Retranslate.onLocaleOrLanguageChanged;
            }
        }
        
        opacity: App.loading ? 0 : 1
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        dataModel: GroupDataModel {
            id: model
            grouping: ItemGrouping.None
            sortingKeys: ["name"]
        }
        listItemComponents: [
            ListItemComponent {
                type: "item"
                ListItemProgram {}
            }
        ]
        layout: StackListLayout {
            headerMode: ListHeaderMode.Sticky
        }
        bufferedScrollingEnabled: true
    }
    
}