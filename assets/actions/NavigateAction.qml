import bb.cascades 1.2

ActionItem {
    id: root

    property alias text: root.title
    property alias icon: root.imageSource
    signal clicked()
    
    title: qsTr("Navigate") + Retranslate.onLocaleOrLanguageChanged
    imageSource: "asset:///images/actions/navigovat.png"
    onTriggered: {
        root.clicked()
    }
}