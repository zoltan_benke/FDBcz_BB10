import bb.cascades 1.2

ActionItem {
    id: root
    enabled: false
    property alias text: root.title
    property alias icon: root.imageSource
    signal clicked()
    
    title: qsTr("Play") + Retranslate.onLocaleOrLanguageChanged
    imageSource: "asset:///images/actions/play-video.png"
    onTriggered: {
        root.clicked()
    }
}