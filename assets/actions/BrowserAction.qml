import bb.cascades 1.2

ActionItem {
    id: root
    
    property alias text: root.title
    property alias icon: root.imageSource
    property string film: ""
    signal clicked()
    
    title: qsTr("Open in browser") + Retranslate.onLocaleOrLanguageChanged
    imageSource: "asset:///images/actions/open_browser.png"
    onTriggered: {
        root.clicked()
    }
}