import bb.cascades 1.2

ActionItem {
    id: root
    
    property alias text: root.title
    property alias icon: root.imageSource
    
    
    signal clicked()
    
    title: qsTr("Refresh") + Retranslate.onLocaleOrLanguageChanged
    imageSource: "asset:///images/actions/refresh.png"
    onTriggered: {
        root.clicked()
    }
}