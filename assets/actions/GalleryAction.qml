import bb.cascades 1.2

ActionItem {
    id: root
    enabled: false
    property alias text: root.title
    property alias icon: root.imageSource
    signal clicked()
    
    title: qsTr("Gallery") + Retranslate.onLocaleOrLanguageChanged
    imageSource: "asset:///images/actions/gallery.png"
    onTriggered: {
        root.clicked()
    }
}