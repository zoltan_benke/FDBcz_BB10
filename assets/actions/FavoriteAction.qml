import bb.cascades 1.2

ActionItem {
    id: root
    
    property alias text: root.title
    property alias icon: root.imageSource
    property bool add: true
    onAddChanged: {
        if (add){
            remove = false
        }else{
            remove = true
        }
    }
    property bool remove: false
    signal clicked()
    
    
    title: add ? qsTr("Add to favorite") + Retranslate.onLocaleOrLanguageChanged : qsTr("Remove from favorite") + Retranslate.onLocaleOrLanguageChanged
    imageSource: add ? "asset:///images/actions/add_to_favorite.png" : "asset:///images/actions/remove_from_favorite.png"
    onTriggered: {
        root.clicked()
    }
}