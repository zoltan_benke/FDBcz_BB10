import bb.cascades 1.2

import "../components"
import "../items"
import "../actions"
import "../pages"

NaviPane {
    id: navigationPane
    
    onPopTransitionEnded: {
        listView.clearSelection()
    }
    
    MyPage {
        id: root
        
        
        function load(){
            model.clear()
            var list = Database.cinemasList
            for (var item in list){
                var kino = JSON.parse(list[item])
                model.insert(kino)
            }
            App.loading = false
        }
        
        
        function remove(item){
            Database.removeCinema(item.id)
            model.remove(item)
        }
        
        header: TitleHeader {
            titleText: activeTab.title
            subtitleText: activeTab.description
        }
        loading: App.loading
        
        actions: [
            AddAction {
                ActionBar.placement: ActionBarPlacement.OnBar
                onClicked: {
                    var page = addCinema.createObject()
                    activePane.push(page)
                }
            },
            RefreshAction {
                onClicked: {
                    root.load()
                }
            }
        ]
        
        attachedObjects: [
            ComponentDefinition {
                id: addCinema
                KinoPridat {
                    onKinoChanged: {
                        root.load()
                    }
                }
            },
            ComponentDefinition {
                id: kinoProgram
                source: "../pages/KinoProgram.qml"
            },
            ComponentDefinition {
                id: map
                source: "../pages/Mapa.qml"
            }
        ]
        
        onActiveChanged: {
            if (active){
                App.loading = true
                load()
            }
        }
        
        
        ListView {
            id: listView
            scrollRole: ScrollRole.Main
            opacity: App.loading ? 0 : 1
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            function removeFavorite(indexPath){
                var item = dataModel.data(indexPath)
                if (item){
                    var check = Database.removeCinema(item.id)
                    if (check){
                        dataModel.removeAt(indexPath)
                    }
                }
            }
            
            function removeAll(){
                var check = Database.clearCinemas()
                if (check){
                    model.clear()
                }
            }
            
            function showMap(indexPath){
                var item = dataModel.data(indexPath)
                if (item){
                    var m = map.createObject()
                    m.title = item.name
                    m.subtitle = item.adress
                    var location = {}
                    location.lat = item.lat
                    location.lon = item.lon
                    m.location = location
                    activePane.push(m)
                }
            }
            /*
            multiSelectAction: MultiSelectActionItem {
            
            }
            
            multiSelectHandler{
                actions: [
                    FavoriteAction {
                        add: false
                        onClicked: {
                            var list = listView.selectionList()
                            listView.clearSelection()
                            for (var i = 0; i < list.length; i++){
                                var kino = model.data(list[i])
                                if (kino){
                                    root.remove(kino)
                                }
                            }
                        }
                    }
                ]
            }
            onSelectionChanged: {
                if (selectionList().length > 1) {
                    multiSelectHandler.status = selectionList().length + " "+ qsTr("vybrané kiná") + Retranslate.onLocaleOrLanguageChanged;
                } else if (selectionList().length == 1) {
                    multiSelectHandler.status = qsTr("1 vybrané kino") + Retranslate.onLocaleOrLanguageChanged;
                } else {
                    multiSelectHandler.status = qsTr("nevybrané") + Retranslate.onLocaleOrLanguageChanged;
                }
            }*/
            
            dataModel: GroupDataModel {
                id: model
                grouping: ItemGrouping.None
                sortingKeys: ["name"]
            }
            listItemComponents: [
                ListItemComponent {
                    type: "item"
                    ListItemOblubeneKina{checkbox: false}
                }
            ]
            bufferedScrollingEnabled: true
            onTriggered: {
                var item = dataModel.data(indexPath)
                if (item){
                    console.log(JSON.stringify(item))
                    var page = kinoProgram.createObject()
                    page.title = item.name
                    page.subtitle = item.adress
                    page.kinoId = item.id
                    page.kino = item
                    activePane.push(page)
                }
            }
        }
    
    }
}