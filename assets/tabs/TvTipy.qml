import bb.cascades 1.2
import bb.data 1.0

import "../components"
import "../items"
import "../actions"

NaviPane {
    id: navigationPane
    
    onPopTransitionEnded: {
        listView.clearSelection()
    }
    
    MyPage {
        id: root
        
        
        function request(response){
            noContentVisible = false
            var json = JSON.parse(response)
            if (Settings.filterTipy){
                var idList = Database.favoriteList
                console.log("function request(), list count = ".concat(idList.length))
                for (var item in json.data){
                    var tip = json.data[item]
                    if (idList.indexOf(tip.id) != -1){
                        model.insert(tip)
                    }
                }
                if (!model.size()){
                    noContentText = qsTr("No tips for your favorite stations") + Retranslate.onLocaleOrLanguageChanged  
                }
            }else{
                model.insertList(json.data)
                if (!model.size()){
                    noContentText = qsTr("No data") + Retranslate.onLocaleOrLanguageChanged
                }
            }
            if (!model.size()){
                console.log("TU SOM")
                noContentVisible = true
            }
            App.loading = false
        }
        
        function updateModelSize(){
            modelSize = model.size()
        }
        
        
        property int modelSize: model.size()
        header: TitleHeader {
            titleText: activeTab.title
            subtitleText: qsTr("Filtration: %1").arg(Settings.filterTipy ? qsTr("active") : qsTr("inactive"))  + Retranslate.onLocaleOrLanguageChanged
        }
        loading: App.loading
        noContentVisible: !App.loading && !modelSize
        onActiveChanged: {
            if (active){
                console.log("onCreationCompleted, Tv Tipy")
                model.itemAdded.connect(updateModelSize)
                model.itemRemoved.connect(updateModelSize)
                App.tvTipyDone.connect(request)
                App.loading = true
                Api.tipy()
            }
        }
        
        
        actions: [
            FavoriteFilterAction {
              	ActionBar.placement: ActionBarPlacement.OnBar
              	onClicked: {
                   Settings.filterTipy = !Settings.filterTipy
                   App.loading = true
                   model.clear()
                   Api.tipy(tipSelection.selectedIndex)
               }  
            },
            RefreshAction {
                ActionBar.placement: ActionBarPlacement.OnBar
                onClicked: {
                    App.loading = true
                    model.clear()
                    Api.tipy(tipSelection.selectedIndex)
                }
            }
        ]
        
        Container {
            SegmentedControl {
                id: tipSelection
                horizontalAlignment: HorizontalAlignment.Fill
                options: [
                    Option {
                        text: qsTr("Today") + Retranslate.onLocaleOrLanguageChanged
                    },
                    Option {
                        text: qsTr("Tomorrow") + Retranslate.onLocaleOrLanguageChanged
                    },
                    Option {
                        text: qsTr("Day after tomorrow") + Retranslate.onLocaleOrLanguageChanged
                    }
                ]
                onSelectedOptionChanged: {
                    App.loading = true
                    model.clear() 
                    Api.tipy(selectedIndex)
                }
            }
            ListView {
                id: listView
                
                function addNotify(indexPath){
                    var item = model.data(indexPath)
                    // {"title": Nazov filmu, "body": Popis filmu, "station": Nazov stanice, "guid": Unikatne ID}
                    var data = {"title": item.film.title, "body": item.film.zanry, "station": item.name, "guid": item.idz}
                    Calendar.addEvent(item.alarm, item.min, data)
                }
                
                scrollRole: ScrollRole.Main
                opacity: App.loading ? 0 : 1
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                dataModel: GroupDataModel {
                    id: model
                    grouping: (tipSelection.selectedIndex === 2) ? ItemGrouping.ByFullValue : ItemGrouping.None
                    sortingKeys: ["startdate", "time"]
                    sortedAscending: true
                }
                listItemComponents: [
                    ListItemComponent {
                        type: "item"
                        ListItemTvTipy {}
                    }
                ]
                layout: StackListLayout {
                    headerMode: ListHeaderMode.Sticky
                }
                bufferedScrollingEnabled: false
                onTriggered: {
                    var item = dataModel.data(indexPath)
                    if (item){
                        var page = detailFilmu.createObject()
                        page.title = item.film.title
                        page.filmId = item.film.id
                        activePane.push(page);
                    }
                }
                
                attachedObjects: [
                    ComponentDefinition {
                        id: detailFilmu
                        source: "../pages/DetailFilmu.qml"
                    }
                ]
            }
        }
    }
}
