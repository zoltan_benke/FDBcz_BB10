import bb.cascades 1.2

import "../components"
import "../items"
import "../actions"

NaviPane {
    id: navigationPane
    
    onPopTransitionEnded: {
        listView.clearSelection()
    }

    MyPage {
        id: root
        
        function baseItem(item){
            baseModel.insert(item)
        }
        
        function favoriteItem(item){
            favoriteModel.insert(item)
        }
        
        function favoriteChanged(list, count){
            if (listView.dataModel == favoriteModel && !count){
                listView.dataModel = baseModel
            }
        }
        
        function checkModel(){
            if (!Database.favoriteCount && listView.dataModel == favoriteModel)
                listView.dataModel = baseModel
        }
        
        
        function clear(){
            baseModel.clear()
            favoriteModel.clear()
        }
        
        
        header: TitleHeader {
            id: titleHeader
            titleText: activeTab.title
            subtitleText: !Database.favoriteCount ? "" : (listView.dataModel == baseModel) ? qsTr("Others") + Retranslate.onLocaleOrLanguageChanged : qsTr("Favorite") + Retranslate.onLocaleOrLanguageChanged
            rightButtonVisible: Database.favoriteCount
            rightButtonImageSource: (listView.dataModel == baseModel) ? "asset:///images/others_stations.png" : "asset:///images/favorite_stations.png"
            onRightButtonClicked: {
                if (listView.dataModel == baseModel){
                    listView.dataModel = favoriteModel
                }else{
                    listView.dataModel = baseModel
                }
            }
            rightButtonExpandVisible: true
            rightButtonExpandImageSource: "asset:///images/expand.png"
            onExpandClicked: {
                expand = !expand
                window.peekEnabled = !expand
            }
            onBeziodClicked: {
                App.loading = true
                if (item.selected){
                    Api.praveBezi()
                }else{
                    Api.beziOd(item.name.split(":")[0])
                }
            }
        }
        loading: App.loading
        onActiveChanged: {
            if (active){
                App.clearModel.connect(clear)
                App.baseItemDone.connect(baseItem)
                App.favoriteItemDone.connect(favoriteItem)
                Database.favoriteChanged.connect(favoriteChanged)
                App.loading = true
                Api.praveBezi()
            }
        }
        actions: [
            SearchAction {
                ActionBar.placement: ActionBarPlacement.OnBar
              	onClicked: {
                   var page = searchProgram.createObject()
                   activePane.push(page)
               }  
            },
            RefreshAction {
                ActionBar.placement: ActionBarPlacement.OnBar
                onClicked: {
                    App.loading = true
                    Api.praveBezi()
                }
            },
            ActionItem {
                title: qsTr("Favorite stations") + Retranslate.onLocaleOrLanguageChanged
                onTriggered: {
                    var page = nastavenieOblubenych.createObject()
                    page.baseModel = baseModel
                    page.favoriteModel = favoriteModel
                    activePane.push(page)
                }
            }
        ]
        
        
        attachedObjects: [
            ComponentDefinition {
                id: vysielanie
                source: "../pages/Vysielanie.qml"
            },
            ComponentDefinition {
                id: nastavenieOblubenych
                source: "../pages/NastavenieOblubenych.qml"
            },
            ComponentDefinition {
                id: movieDetail
                source: "../pages/DetailFilmu.qml"
            },
            ComponentDefinition {
                id: searchProgram
                source: "../pages/HladatProgram.qml"
            }
        ]
        
        
        ListView {
            id: listView
            scrollRole: ScrollRole.Main
            onCreationCompleted: {
                if (Database.favoriteCount && Settings.startView == 1){
                    dataModel = favoriteModel
                }else{
                    dataModel = baseModel
                }
            }
            function setFavorite(indexPath, favorite){
                var item = dataModel.data(indexPath)
                if (item){
                    item.favorite = favorite
                    if (favorite){
                        Database.addFavorite(item.id)
                    }else{
                        Database.removeFavorite(item.id)
                    }
                    if (dataModel == baseModel){
                        favoriteModel.insert(item)
                        baseModel.removeAt(indexPath)
                    }else{
                        baseModel.insert(item)
                        favoriteModel.removeAt(indexPath)
                    }
                }
                root.checkModel()
            }
            
            //multiSelectAction: MultiSelectActionItem {}
            
            multiSelectHandler{
                actions: [
                    FavoriteAction {
                        enabled: (listView.dataModel == baseModel)
                        add: true
                        onClicked: {
                            App.addToFavorite(listView.selectionList());
                            //var list = listView.selectionList()
                            listView.clearSelection()
                            /*
                             for (var i = 0; i < list.length; i++){
                             var program = listView.dataModel.data(list[i])
                             
                             baseModel.remove(program)
                             program.favorite = true
                             favoriteModel.insert(program)
                             Database.addFavorite(program.id) 
                             }*/
                        }
                    },
                    FavoriteAction {
                        enabled: (listView.dataModel == favoriteModel)
                        add: false
                        onClicked: {
                            var list = listView.selectionList()
                            listView.clearSelection()
                            for (var i = 0; i < list.length; i++){
                                var program = listView.dataModel.data(list[i])
                                
                                favoriteModel.remove(program)
                                program.favorite = false
                                baseModel.insert(program)
                                Database.removeFavorite(program.id)
                            }
                            root.checkModel()
                        }
                    }
                ]
            }
            
            onSelectionChanged: {
                if (selectionList().length > 1) {
                    multiSelectHandler.status = selectionList().length + " "+ qsTr("selected stations") + Retranslate.onLocaleOrLanguageChanged;
                } else if (selectionList().length == 1) {
                    multiSelectHandler.status = qsTr("1 selected station") + Retranslate.onLocaleOrLanguageChanged;
                } else {
                    multiSelectHandler.status = qsTr("no selected") + Retranslate.onLocaleOrLanguageChanged;
                }
            }
            
            attachedObjects: [
                GroupDataModel {
                    id: baseModel
                    objectName: "baseModel"
                    grouping: ItemGrouping.None
                    sortingKeys: ["name"]
                },
                GroupDataModel {
                    id: favoriteModel
                    objectName: "favoriteModel"
                    grouping: ItemGrouping.None
                    sortingKeys: ["name"]
                }
            ]
            
            function itemType(data, indexPath) {
                return ((data.state != undefined) ? 'item' : 'beziod');
            }
            
            opacity: App.loading ? 0 : 1
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            dataModel: baseModel
            listItemComponents: [
                ListItemComponent {
                    type: "item"
                    ListItemPraveBezi {}
                },
                ListItemComponent {
                    type: "beziod"
                    ListItemBeziOd{}
                }
            ]
            layout: StackListLayout {
                headerMode: ListHeaderMode.Sticky
            }
            
            function beziOdClicked(indexPath, first){
                var item = dataModel.data(indexPath)
                var page;
                if (item){
                    if (first){
                        if (item.film_1 != 0){
                            page = movieDetail.createObject()
                            page.title = item.title_1
                            page.filmId = item.film_1
                        }
                    }else{
                        if (item.film_2 != 0){
                            page = movieDetail.createObject()
                            page.title = item.title_2
                            page.filmId = item.film_2
                        }
                    }
                    if (page){
                        activePane.push(page)
                    }
                }
            }
            
            onTriggered: {
                select(indexPath)
                var data  = dataModel.data(indexPath)
                if (data){
                    if (data.state != undefined){
                        if (data.title){
                            var page = vysielanie.createObject()
                            page.stanica = data
                            activePane.push(page)
                        }else{
                            listView.clearSelection()
                            toast.body = qsTr("Station does not broadcast") + Retranslate.onLocaleOrLanguageChanged
                            toast.show()
                        }
                    }
                }
            }
            bufferedScrollingEnabled: true
        }
    }
}